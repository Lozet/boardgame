#ifndef BOARDGAME_UCT_WORKER_H
#define BOARDGAME_UCT_WORKER_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"
#include <mutex>
#include <string>

class MainWindow;

class UctWorker
{
public:
	UctWorker();

	/** Thread function. */
	void do_work(MainWindow* caller, const GameState*const rootstate, const int itermax, const double uctk, const int verbosity = 0);

	std::string uct_search(const GameState*const rootstate, const int itermax, const double uctk, const int verbosity);

	void get_move(std::string* move) const;

	void stop_work();

	bool has_stopped() const;

private:
	// Synchronizes access to member data.
	mutable std::mutex mutex_;

	// Data used by both GUI thread and worker thread.
	bool shall_stop_;
	bool has_stopped_;
	std::string move_;
};

#endif //BOARDGAME_UCT_WORKER_H
