// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "node.h"
#include "random.h"
#include <cmath>
#include <cstdio>
#include <cassert>

Node::Node(const std::string& move, Node*const parent, const GameState*const state)
: move_(move),
  parent_(parent),
  wins_(0.0),
  visits_(0),
  playerJustMoved_(state->get_player_just_moved())
{
	state->get_moves(untried_moves_);
}

Node::~Node()
{
	if (!childs_.empty()) {
		for (std::vector<Node*>::iterator it = childs_.begin(); it != childs_.end(); ++it) {
			delete *it;
		}
		childs_.clear();
	}

	untried_moves_.clear();
}

double Node::get_wins() const
{
	return wins_;
}

int Node::get_visits() const
{
	return visits_;
}

Node* Node::get_parent()
{
	return parent_;
}

int Node::get_player() const
{
	return playerJustMoved_;
}

Node* Node::select_child(const double uctk)
{
	/* Use the UCB1 formula to select a child node. Often a constant UCTK is applied so we have
	lambda c: c.wins/c.visits + UCTK * sqrt(2*log(self.visits)/c.visits to vary the amount of
	exploration versus exploitation. */
	// s = sorted(self.childNodes, key = lambda c: c.wins/c.visits + sqrt(2*log(self.visits)/c.visits))[-1]
	// return s
	Node* best_node = 0;
	double best_value = -1.0; // minimal value should be 0.0
	double value;

	for (std::vector<Node*>::iterator it = childs_.begin(); it != childs_.end(); ++it) {
		Node*const c = *it;
		value = c->get_wins() / static_cast<double>(c->get_visits()) +
			uctk * sqrt(2.0 * log(static_cast<double>(visits_)) / static_cast<double>(c->get_visits()));
		if (value > best_value) {
			best_value = value;
			best_node = c;
		}
	}

	assert(best_node != 0);
	return best_node;
}

Node* Node::select_child(const std::string& move)
{
	for (std::vector<Node*>::iterator it = childs_.begin(); it != childs_.end(); ++it) {
		if ((*it)->get_move() == move) {
			return (*it);
		}
	}

	assert("Move not found" == 0);
	return 0;
}

Node* Node::add_child(const std::string& move, const GameState*const state)
{
	Node* n = new Node(move, this, state);
	untried_moves_.remove(move);
	childs_.push_back(n);
	return n;
}

void Node::update(const double result)
{
	++visits_;
	wins_ += result;
}

std::string Node::get_move() const
{
	return move_;
}

std::string Node::get_most_visited_move() const
{
	int most_visists = -1;
	std::string most_visited = "";

	for (std::vector<Node*>::const_iterator it = childs_.begin(); it != childs_.end(); ++it) {
		const int visits = (*it)->get_visits();
		if (visits > most_visists) {
			most_visists = visits;
			most_visited = (*it)->get_move();
		}
	}

	assert(most_visited != "");
	return most_visited;
}

std::string Node::get_random_move() const
{
	std::list<std::string>::const_iterator it = untried_moves_.begin();
	if (untried_moves_.size() > 1) {
		const int r = generate_number(untried_moves_.size());
		for (int i = 0; i < r; ++i) {
			++it;
		}
	}
	return (*it);
}

void Node::display_tree(const int verbose_level) const
{
	if (verbose_level > 0) {
		printf("Node level %d, Move %s by player %d\n", verbose_level, move_.c_str(), playerJustMoved_);
		for (std::vector<Node*>::const_iterator it = childs_.begin(); it != childs_.end(); ++it) {
			const double wins = (*it)->get_wins();
			const int visits = (*it)->get_visits();
			printf("[M:%s W/V: %.1lf/%d = %.2lf%%]\n", (*it)->get_move().c_str(), wins, visits,
					100.0 * wins / static_cast<double>(visits));
		}
		putchar('\n');
		if (verbose_level > 1 && childs_.size() > 0) {
			for (std::vector<Node*>::const_iterator it = childs_.begin(); it != childs_.end(); ++it) {
				(*it)->display_tree(verbose_level - 1);
			}
		}
	}
}
