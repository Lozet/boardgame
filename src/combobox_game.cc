/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "combobox_game.h"
#include <cassert>

ComboBoxGame::ComboBoxGame()
{
	// create a model
	refTreeModel_ = Gtk::ListStore::create(columns_);
	set_model(refTreeModel_);

	// add a label column
	Gtk::CellRendererText *const pRenderer = Gtk::manage(new Gtk::CellRendererText);
	pack_start(*pRenderer, false);

	// set the label and sensitive properties
	add_attribute(pRenderer->property_sensitive(), columns_.col_sensitive);
	add_attribute(pRenderer->property_markup(), columns_.col_name);

	set_id_column(0);
}

ComboBoxGame::~ComboBoxGame()
{
}

void ComboBoxGame::append_category(const GameCategory& category)
{
	append_header(category.id, category.name);

	for (auto it = category.games.cbegin(); it != category.games.cend(); ++it) {
		append_game((*it)->id, (*it)->name);
	}
}

void ComboBoxGame::append(const Glib::ustring& id, const Glib::ustring& label, const bool sensitive)
{
	Gtk::TreeModel::Row row = *(refTreeModel_->append());
	row[columns_.col_id] = id;
	row[columns_.col_name] = label;
	row[columns_.col_sensitive] = sensitive;
}
