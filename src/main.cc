/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "application.h"
#include "exception.h"
#include "messagedialog.h"
#include <glibmm/i18n.h>
#include <iostream>

void print_fatal_error(const Glib::ustring& error)
{
	Glib::ustring msg = Glib::ustring::compose("boardgame stopped because an error occurred.\nPlease report this error to %1\nInclude the following information:\n%2", PACKAGE_BUGREPORT, error);

	std::cerr << msg << std::endl;
	message_error(_("Error Boardgame"), msg);
}

int main(int argc, char **argv)
{
	int status = EXIT_SUCCESS;

	try
	{
#ifdef ENABLE_NLS
		setlocale(LC_MESSAGES, "");
		bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
		bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
		textdomain(GETTEXT_PACKAGE);
#endif

		Glib::RefPtr<BoardgameApplication> app = BoardgameApplication::create();
		status = app->run(argc, argv);
	}
	catch (const BoardgameErr& ex)
	{
		print_fatal_error(ex.get_message());
		status = EXIT_FAILURE;
	}
	catch (const Glib::Error& ex)
	{
		print_fatal_error(ex.what());
		status = EXIT_FAILURE;
	}

	return status;
}
