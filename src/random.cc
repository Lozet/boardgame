/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "random.h"
#include <random>

std::mt19937 random_number_generator;

void init_random()
{
	random_number_generator.seed(std::random_device()());
}

int generate_number(const int min, const int max)
{
	std::uniform_int_distribution<std::mt19937::result_type> dist(min, max);
	return dist(random_number_generator);
}

int generate_number(const int limit)
{
	std::uniform_int_distribution<std::mt19937::result_type> dist(0, limit - 1);
	return dist(random_number_generator);
}
