// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/nim.h"
#include "states/nim_state.h"
#include <gtkmm/window.h>
#include <cmath>
#include <cassert>

const double Nim::HMARGIN = 0.05;
const double Nim::TOKEN_SIZE = 0.95;

Nim::Nim(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: Game(difficulty, nb_players),
  moving_heap_(-1),
  first_moving_token_(-1)
{
	state_ = new NimState;
}

Nim::~Nim()
{
}

void Nim::reset()
{
	Game::reset();

	moving_heap_ = -1;
	first_moving_token_ = -1;

	delete state_;
	state_ = new NimState;
}

int Nim::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 3000;

		case DIFFICULTY_EASY:
			return 9000;

		case DIFFICULTY_NORMAL:
			return 30000;

		case DIFFICULTY_HARD:
			return 100000;

		case DIFFICULTY_VERY_HARD:
			return 300000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

void Nim::set_displayed_move(const std::string& move)
{
	Game::set_displayed_move(move);

	std::string str = move.c_str();
	std::string::size_type sz;

	moving_heap_ = std::stoi(str, &sz);
	str.erase(0, sz + 1);

	const int number = std::stoi(str);
	first_moving_token_ = static_cast<NimState*>(state_)->get_board()[moving_heap_] - number;
}

bool Nim::update_animation()
{
	displayed_pourcent_ += (1.0 / 12.0);
	if (displayed_pourcent_ >= 1.0) {
		moving_heap_ = -1;
		first_moving_token_ = -1;
		// play the move
		play_displayed_move();
		// stop the timer
		return false;
	} else {
		return true;
	}
}

bool Nim::handle_click(GdkEventButton* release_event)
{
	//  human turn and left click and not already playing
	if (!is_human_turn() || release_event->button != 1 || displayed_move_ != "") {
		return false;
	}

	// get the gdk window
	Glib::RefPtr<Gdk::Window> win = Glib::wrap(release_event->window, true);

	// check the y coord of the click event
	const int height = win->get_height();
	const int width = win->get_width();
	const double heap_spacing = height / (NimState::NB_HEAPS);
	const double hmargin = HMARGIN * width;
	const double token_spacing = (width - hmargin * 2.0) / NimState::MAX_TOKENS_PER_HEAP;
	const double token_size = std::min(TOKEN_SIZE * token_spacing, heap_spacing - 2.0);

	const int min_pos_y = (heap_spacing - token_size) / 2.0;
	const int max_pos_y = (heap_spacing + token_size) / 2.0;
	const int pos_y = ((int) release_event->y) % ((int) heap_spacing);
	if (pos_y < min_pos_y || pos_y > max_pos_y) {
		return false;
	}

	const int release_x = release_event->x - hmargin;
	const int pos_x = release_x % ((int) token_spacing);
	if (pos_x < 0.0 || pos_x > token_size) {
		return false;
	}

	const int heap = ((int) release_event->y) / heap_spacing;
	const int token = release_x / ((int) token_spacing);

	const int* board = static_cast<NimState*>(state_)->get_board();
	const int number = board[heap] - token;
	if (number <= 0 || number > 3) {
		return false;
	}

	set_displayed_move(std::to_string(heap) + ":" + std::to_string(number));
	return true;
}

void Nim::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	if (!state_->has_move_left()) {
		return;
	}

	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);

	const double hmargin = HMARGIN * width;
	const double token_spacing = (width - hmargin * 2.0) / NimState::MAX_TOKENS_PER_HEAP;
	const double heap_spacing = height / (NimState::NB_HEAPS);
	const double token_size = std::min(TOKEN_SIZE * token_spacing, heap_spacing - 2.0);
	const double vmargin = (heap_spacing - token_size) / 2.0;
	const int* heaps = static_cast<NimState*>(state_)->get_board();
	const bool number_displayed = (displayed_move_ == "" && is_human_turn());
	double alpha;

	Cairo::RefPtr<Cairo::Surface> target = cr->get_target();
	Cairo::RefPtr<Cairo::Surface> token = create_token(target, token_size);
	Cairo::RefPtr<Cairo::Surface> numbers[3];
	if (number_displayed) {
		numbers[0] = create_number(target, 1, token_size);
		numbers[1] = create_number(target, 2, token_size);
		numbers[2] = create_number(target, 3, token_size);
	}

	for (int h = 0; h < NimState::NB_HEAPS; ++h) {
		y = vmargin + h * heap_spacing;
		for (int t = 0; t < heaps[h]; ++t) {
			x = hmargin + t * token_spacing;
			if (h == moving_heap_ && t >= first_moving_token_) {
				alpha = 1.0 - displayed_pourcent_;
			} else {
				alpha = 1.0;
			}

			// draw the token
			cr->set_source(token, x, y);
			cr->paint_with_alpha(alpha);

			if (number_displayed && heaps[h] - t <= 3) {
				// draw the number
				cr->set_source(numbers[heaps[h] - t - 1], x, y);
				cr->paint_with_alpha(alpha);
			}
		}
	}
}

Cairo::RefPtr<Cairo::Surface> Nim::create_token(const Cairo::RefPtr<Cairo::Surface>& target, const double size)
{
	Cairo::RefPtr<Cairo::Surface> token = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, size, size);
	Cairo::RefPtr<Cairo::Context> token_cxt = Cairo::Context::create(token);

	const double OUTER_RADIUS = (size * 0.93) / 2.0;
	const double INNER_RADIUS = (size * 0.90) / 2.0;
	const double POS_CENTER = size / 2.0;

	token_cxt->set_line_width(size / 20.0);

	// draw a circle
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(
		POS_CENTER, POS_CENTER, 0.0,
		POS_CENTER, POS_CENTER, INNER_RADIUS
	);
	gradient->add_color_stop_rgb(0.0, 0.0, 0.0, 0.9);
	gradient->add_color_stop_rgb(1.0, 0.0, 0.0, 0.7);
	token_cxt->set_source(gradient);
	token_cxt->arc(POS_CENTER, POS_CENTER, INNER_RADIUS, 0.0, 2.0 * M_PI);
	token_cxt->fill();

	// the border
	token_cxt->set_source_rgb(0.0, 0.0, 0.0);
	token_cxt->arc(POS_CENTER, POS_CENTER, OUTER_RADIUS, 0.0, 2.0 * M_PI);
	token_cxt->stroke();

	return token;
}

Cairo::RefPtr<Cairo::Surface> Nim::create_number(const Cairo::RefPtr<Cairo::Surface>& target, const int number, const double size)
{
	Cairo::RefPtr<Cairo::Surface> surface = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, size, size);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(surface);

	context->set_source_rgb(0.0, 0.0, 0.0);
	context->select_font_face("Sans", Cairo::FontSlant::FONT_SLANT_NORMAL, Cairo::FontWeight::FONT_WEIGHT_BOLD);
	context->set_font_size(size / 2.5);

	const std::string text = std::to_string(number);

	Cairo::TextExtents extends;
	context->get_text_extents(text, extends);
	context->move_to((size / 2.0) - extends.x_bearing - (extends.width / 2.0),
	                 (size / 2.0) - extends.y_bearing - (extends.height / 2.0));
	context->show_text(text);

	return surface;
}
