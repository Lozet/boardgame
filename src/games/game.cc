// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "game.h"
#include "random.h"
#include <glibmm/i18n.h>
#include <cassert>

Game::Game(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: difficulty_(difficulty),
  displayed_pourcent_(0.0)
{
	if (nb_players == GAME_1_PLAYER) { // human vs computer
		const int r = generate_number(0, 1);
		players_[r] = PLAYER_TYPE_HUMAN;
		players_[(r + 1) % NB_PLAYER] = PLAYER_TYPE_COMPUTER;
	} else if (nb_players == GAME_2_PLAYERS) { // human vs human
		players_[0] = PLAYER_TYPE_HUMAN;
		players_[1] = PLAYER_TYPE_HUMAN;
	} else { // computer vs computer
		players_[0] = PLAYER_TYPE_COMPUTER;
		players_[1] = PLAYER_TYPE_COMPUTER;
	}
}

Game::~Game()
{
	if (state_) {
		delete state_;
	}
}

void Game::reset()
{
	// swap begining player
	if (players_[0] != players_[1]) {
		const PlayerType temp = players_[0];
		players_[0] = players_[1];
		players_[1] = temp;
	}

	displayed_pourcent_ = 0.0;
	displayed_move_ = "";

	history_.clear();
	signal_game_reset_.emit();
}

std::string Game::get_status() const
{
	std::string status;

	if (state_->has_move_left()) {
		const int id = state_->get_current_player();

		if (is_computer_turn()) {
			status = Glib::ustring::compose(_("Player %1 is thinking..."), id);
		} else {
			status = Glib::ustring::compose(_("Player %1 turn"), id);
		}
	} else {
		status = _("Game finished: ");

		const double result = state_->get_result(1);
		if (result == 1.0) {
			status += _("Player 1 wins");
		} else if (result == 0.5) {
			status += _("Draw");
		} else if (result == 0.0) {
			status += _("Player 2 wins");
		} else {
			status += _("Error: Unknow result");
		}
	}

	return status;
}

const GameState* Game::get_state() const
{
	return state_;
}

bool Game::update_animation()
{
	// no animation, so just play the move
	play_displayed_move();
	// and stop the timer
	return false;
}

void Game::set_displayed_move(const std::string& move)
{
	displayed_move_ = move;
	displayed_pourcent_ = 0.0;
}

void Game::play_displayed_move()
{
	// add the move to history
	history_.push_back(displayed_move_);
	signal_new_move_.emit(state_->get_current_player(), displayed_move_);

	// play the move
	state_->do_move(displayed_move_);

	// reset the animation
	displayed_pourcent_ = 0.0;
	displayed_move_ = "";
}

bool Game::is_human_turn() const
{
	return state_->has_move_left() && (players_[state_->get_current_player() - 1] == PLAYER_TYPE_HUMAN);
}

bool Game::is_computer_turn() const
{
	return state_->has_move_left() && (players_[state_->get_current_player() - 1] == PLAYER_TYPE_COMPUTER);
}

void Game::export_history(FILE* fp) const
{
	fprintf(fp, _("Difficulty: %d\n"), difficulty_);

	for (int i = 0; i < NB_PLAYER; ++i) {
		fprintf(fp, _("Player %d: "), i + 1);
		if (players_[i] == PLAYER_TYPE_HUMAN) {
			fputs(_("Human\n"), fp);
		} else {
			fputs(_("Computer\n"), fp);
		}
	}

	fputs(_("\nmoves:\n"), fp);
	for (int i = 0; i < history_.size(); ++i) {
		fprintf(fp, "%s\n", history_[i].c_str());
	}
}

Game::type_signal_new_move Game::signal_new_move()
{
	return signal_new_move_;
}

Game::type_signal_game_reset Game::signal_game_reset()
{
	return signal_game_reset_;
}
