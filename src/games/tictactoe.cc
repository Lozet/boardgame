// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/tictactoe.h"
#include "states/tictactoe_state.h"
#include <gtkmm/window.h>
#include <cmath>
#include <cassert>

TicTacToe::TicTacToe(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: Game(difficulty, nb_players)
{
	state_ = new TicTacToeState;
}

TicTacToe::~TicTacToe()
{
}

void TicTacToe::reset()
{
	Game::reset();

	delete state_;
	state_ = new TicTacToeState;
}

int TicTacToe::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 100;

		case DIFFICULTY_EASY:
			return 200;

		case DIFFICULTY_NORMAL:
			return 600;

		case DIFFICULTY_HARD:
			return 2000;

		case DIFFICULTY_VERY_HARD:
			return 5000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

bool TicTacToe::update_animation()
{
	displayed_pourcent_ += (1.0 / 15.0);
	if (displayed_pourcent_ >= 1.0) {
		// play the move
		play_displayed_move();
		// stop the timer
		return false;
	} else {
		return true;
	}
}

bool TicTacToe::handle_click(GdkEventButton* release_event)
{
	bool ok = false;

	//  human turn and left click and not already playing
	if (is_human_turn() && release_event->button == 1 && displayed_move_ == "")
	{
		// get the clicked square
		Glib::RefPtr<Gdk::Window> win = Glib::wrap(release_event->window, true);
		const double square_size = std::min(win->get_width(), win->get_height()) / 3.0;
		const int xsquare = release_event->x / square_size;
		const int ysquare = release_event->y / square_size;

		if (xsquare >= 0 && xsquare < 3 && ysquare >= 0 && ysquare < 3) {
			const int human_move = ysquare * 3 + xsquare;

			// check if this move is playable
			if (static_cast<TicTacToeState*>(state_)->try_move(human_move)) {
				// set the current move animation
				set_displayed_move(std::to_string(human_move));
				ok = true;
			}
		}
	}

	return ok;
}

double TicTacToe::get_board_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);
	return std::min(width, height);
}

Cairo::RefPtr<Cairo::Surface> TicTacToe::create_circle(const Cairo::RefPtr<Cairo::Surface>& target, const double size)
{
	Cairo::RefPtr<Cairo::Surface> circle = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, size, size);
	Cairo::RefPtr<Cairo::Context> circle_cxt = Cairo::Context::create(circle);

	circle_cxt->set_line_width(size / 25.0);
	circle_cxt->arc(size / 2.0, size / 2.0, size / 2.5, 0.0, 2.0 * M_PI);
	circle_cxt->stroke();

	return circle;
}

Cairo::RefPtr<Cairo::Surface> TicTacToe::create_cross(const Cairo::RefPtr<Cairo::Surface>& target, const double size)
{
	const double MARGIN = size / 10.0;
	Cairo::RefPtr<Cairo::Surface> cross = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, size, size);
	Cairo::RefPtr<Cairo::Context> cross_cxt = Cairo::Context::create(cross);

	cross_cxt->set_line_width(size / 25.0);
	cross_cxt->move_to(MARGIN, MARGIN);
	cross_cxt->line_to(size - MARGIN, size - MARGIN);
	cross_cxt->stroke();
	cross_cxt->move_to(MARGIN, size - MARGIN);
	cross_cxt->line_to(size - MARGIN, MARGIN);
	cross_cxt->stroke();

	return cross;
}

void TicTacToe::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	const double board_size = get_board_size(cr);
	const double square_size = board_size / 3.0;

	// draw the grid
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->set_line_width(board_size / 90.0);

	cr->move_to(square_size, 0.0);
	cr->line_to(square_size, board_size);
	cr->stroke();

	cr->move_to(square_size * 2.0, 0.0);
	cr->line_to(square_size * 2.0, board_size);
	cr->stroke();

	cr->move_to(0.0, square_size);
	cr->line_to(board_size, square_size);
	cr->stroke();

	cr->move_to(0.0, square_size * 2.0);
	cr->line_to(board_size, square_size * 2.0);
	cr->stroke();

	// draw the played squares
	Cairo::RefPtr<Cairo::Surface> target = cr->get_target();
	Cairo::RefPtr<Cairo::Surface> cross;
	Cairo::RefPtr<Cairo::Surface> circle;

	const int* board = static_cast<TicTacToeState*>(state_)->get_board();

	for (int x = 0; x < 3; ++x) {
		for (int y = 0; y < 3; ++y) {
			const int player = board[x + y * 3];
			if (player == 1)
			{
				if (!cross) {
					cross = create_cross(target, square_size);
				}
				cr->set_source(cross, x * square_size, y * square_size);
				cr->paint();
			}
			else if (player == 2)
			{
				if (!circle) {
					circle = create_circle(target, square_size);
				}
				cr->set_source(circle, x * square_size, y * square_size);
				cr->paint();
			}
		}
	}

	// draw the animation

	if (displayed_move_ != "") {
		const double MARGIN = square_size / 10.0;
		const int m = atol(displayed_move_.c_str());
		const int x = m % 3;
		const int y = m / 3;

		cr->set_source_rgb(0.0, 0.0, 0.0);
		cr->set_line_width(square_size / 25.0);

		if (state_->get_current_player() == 1) {
			double sx, sy, mx, my, progress;

			sx = x * square_size + MARGIN;
			mx = ((x+1) * square_size - MARGIN) - sx;

			sy = y * square_size + MARGIN;
			my = ((y+1) * square_size - MARGIN) - sy;

			if (displayed_pourcent_ < 0.45) {
				progress = displayed_pourcent_ / 0.45;
				mx *= progress;
				my *= progress;
			}

			cr->move_to(sx, sy);
			cr->line_to(sx + mx, sy + my);
			cr->stroke();

			if (displayed_pourcent_ > 0.55) {
				sx = (x+1) * square_size - MARGIN;
				mx = (x * square_size + MARGIN) - sx;

				sy = y * square_size + MARGIN;
				my = ((y+1) * square_size - MARGIN) - sy;

				progress = (displayed_pourcent_ - 0.55) / 0.45;
				mx *= progress;
				my *= progress;

				cr->move_to(sx, sy);
				cr->line_to(sx + mx, sy + my);
				cr->stroke();
			}
		} else {
			const double pos_x = x * square_size + square_size / 2.0;
			const double pos_y = y * square_size + square_size / 2.0;
			cr->arc(pos_x, pos_y, square_size / 2.5, 2.0 * M_PI * -displayed_pourcent_, 0.0);
			cr->stroke();
		}
	} else if (!state_->has_move_left()) {
		// draw the result
		const double half_square = square_size / 2.0;

		cr->set_source_rgb(0.0, 0.0, 0.5);
		cr->set_line_width(square_size / 25.0);

		if (board[0]) {
			if (board[0] == board[1] && board[0] == board[2]) {
				cr->move_to(half_square, half_square);
				cr->line_to(half_square + (square_size * 2.0), half_square);
				cr->stroke();
				return;
			} else if (board[0] == board[3] && board[0] == board[6]) {
				cr->move_to(half_square, half_square);
				cr->line_to(half_square, half_square + (square_size * 2.0));
				cr->stroke();
				return;
			} else if (board[0] == board[4] && board[0] == board[8]) {
				cr->move_to(half_square, half_square);
				cr->line_to(half_square + (square_size * 2.0), half_square + (square_size * 2.0));
				cr->stroke();
				return;
			}
		}

		if (board[4]) {
			if (board[4] == board[3] && board[4] == board[5]) {
				cr->move_to(half_square, half_square + square_size);
				cr->line_to(half_square + (square_size * 2.0), half_square + square_size);
				cr->stroke();
				return;
			} else if (board[4] == board[1] && board[4] == board[7]) {
				cr->move_to(half_square + square_size, half_square);
				cr->line_to(half_square + square_size, half_square + (square_size * 2.0));
				cr->stroke();
				return;
			} else if (board[4] == board[2] && board[4] == board[6]) {
				cr->move_to(half_square + (square_size * 2.0), half_square);
				cr->line_to(half_square, half_square + (square_size * 2.0));
				cr->stroke();
				return;
			}
		}

		if (board[8]) {
			if (board[8] == board[6] && board[8] == board[7]) {
				cr->move_to(half_square, half_square + (square_size * 2.0));
				cr->line_to(half_square + (square_size * 2.0), half_square + (square_size * 2.0));
				cr->stroke();
				return;
			} else if (board[8] == board[2] && board[8] == board[5]) {
				cr->move_to(half_square + (square_size * 2.0), half_square);
				cr->line_to(half_square + (square_size * 2.0), half_square + (square_size * 2.0));
				cr->stroke();
				return;
			}
		}
	}
}
