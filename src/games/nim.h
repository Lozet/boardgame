#ifndef BOARDGAME_NIM_H
#define BOARDGAME_NIM_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"

class Nim : public Game
{
public:
	Nim(const GameDifficulty difficulty, const GameNumberPlayers nb_players = GAME_1_PLAYER);

	~Nim();

	void reset();

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	bool update_animation();

	void set_displayed_move(const std::string& move);

private:
	int moving_heap_;
	int first_moving_token_;

	Cairo::RefPtr<Cairo::Surface> create_token(const Cairo::RefPtr<Cairo::Surface>& target, const double size);
	Cairo::RefPtr<Cairo::Surface> create_number(const Cairo::RefPtr<Cairo::Surface>& target, const int number, const double size);

	/** horizontal margin in pourcent of the width. */
	static const double HMARGIN;
	/** token size in pourcent of the token spacing. */
	static const double TOKEN_SIZE;
};

#endif //BOARDGAME_NIM_H
