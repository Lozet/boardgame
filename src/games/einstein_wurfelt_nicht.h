#ifndef BOARDGAME_EINSTEINWURFELTNICHT_H
#define BOARDGAME_EINSTEINWURFELTNICHT_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"

class EinSteinWurfeltNicht : public Game
{
public:
	EinSteinWurfeltNicht(const GameDifficulty difficulty, const GameNumberPlayers nb_players = GAME_1_PLAYER);

	~EinSteinWurfeltNicht();

	void reset();

	std::string get_status() const;

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	double get_uctk() const;

	bool update_animation();

	void set_displayed_move(const std::string& move);

private:
	static const double SQUARE_SIDE;

	int selected_piece_;

	double pos_x_;
	double pos_y_;
	double mx_;
	double my_;

	void draw_board(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	void draw_piece(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y);

	void draw_number(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y, const int number);

	void draw_dice(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	void draw_point(const ::Cairo::RefPtr< ::Cairo::Context>& cr, const double x, const double y);

	void draw_axis(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	void draw_axis_name(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y, const char name);
};

#endif //BOARDGAME_EINSTEINWURFELTNICHT_H
