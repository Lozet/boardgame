// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/reversi.h"
#include "states/reversi_state.h"
#include <gdkmm/window.h>
#include <glibmm/i18n.h>
#include <cmath>
#include <cassert>

Reversi::Reversi(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: Game(difficulty, nb_players),
  square_size_(-1)
{
	state_ = new ReversiState;
}

Reversi::~Reversi()
{
}

void Reversi::reset()
{
	Game::reset();
	moving_pieces_.clear();

	delete state_;
	state_ = new ReversiState;
}

int Reversi::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 200;

		case DIFFICULTY_EASY:
			return 1000;

		case DIFFICULTY_NORMAL:
			return 5000;

		case DIFFICULTY_HARD:
			return 30000;

		case DIFFICULTY_VERY_HARD:
			return 70000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

bool Reversi::update_animation()
{
	displayed_pourcent_ += (1.0 / 14.0);
	if (displayed_pourcent_ >= 1.0) {
		// play the move
		play_displayed_move();
		moving_pieces_.clear();
		// stop the timer
		return false;
	} else {
		return true;
	}
}

void Reversi::set_displayed_move(const std::string& move)
{
	Game::set_displayed_move(move);
	// get the list of captured pieces
	static_cast<ReversiState*>(state_)->get_pieces_taken(move, moving_pieces_);
}

std::string Reversi::get_status() const
{
	std::string s = Game::get_status();

	if (!state_->has_move_left()) {
		int scores[2];
		static_cast<ReversiState*>(state_)->get_scores(scores);
		s += " / " + Glib::ustring::compose(_("Score: %1 - %2"), scores[0], scores[1]);
	}

	return s;
}

bool Reversi::handle_click(GdkEventButton* release_event)
{
	if (!is_human_turn() || release_event->button != 1 || displayed_move_ != "") {
		return false;
	}

	const int x = release_event->x / square_size_;
	if (x < 0 || x >= ReversiState::SIDE_SIZE) {
		return false;
	}

	const int y = release_event->y / square_size_;
	if (y < 0 || y >= ReversiState::SIDE_SIZE) {
		return false;
	}

	const int square = x + y * ReversiState::SIDE_SIZE;
	if (static_cast<ReversiState*>(state_)->try_move(square)) {
		// set the current move animation
		set_displayed_move(std::to_string(square));
		return true;
	} else {
		return false;
	}
}

void Reversi::update_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);
	square_size_ = (int) (std::min(width, height) / ReversiState::SIDE_SIZE);
}

void Reversi::get_coord(const int square, int& x, int& y) const
{
	x = static_cast<ReversiState*>(state_)->get_column(square) * square_size_;
	y = static_cast<ReversiState*>(state_)->get_row(square) * square_size_;
}

void Reversi::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	update_square_size(cr);
	const int board_size = square_size_ * ReversiState::SIDE_SIZE;

	// draw the board
	cr->set_source_rgb(0.00, 0.62, 0.04);
	cr->rectangle(0.0, 0.0, board_size, board_size);
	cr->fill();

	// draw the grid
	cr->set_source_rgb(0.53, 0.75, 0.56);
	cr->set_line_width(board_size / 160.0);
	for (int i = 1; i < ReversiState::SIDE_SIZE; ++i) {
		cr->move_to(i * square_size_, 0.0);
		cr->line_to(i * square_size_, board_size);
		cr->stroke();

		cr->move_to(0.0, i * square_size_);
		cr->line_to(board_size, i * square_size_);
		cr->stroke();
	}

	Cairo::RefPtr<Cairo::Surface> target = cr->get_target();
	Cairo::RefPtr<Cairo::Surface> player1;
	Cairo::RefPtr<Cairo::Surface> player2;

	// draw the static disks
	const int* game_board = static_cast<ReversiState*>(state_)->get_board();
	int x, y;

	for (int s = 0; s < ReversiState::NB_SQUARES; ++s)
	{
		if (game_board[s] && (moving_pieces_.empty() || !is_moving(s)))
		{
			get_coord(s, x, y);

			if (game_board[s] == 1)
			{
				if (!player1) {
					player1 = create_player1_piece(target);
				}
				cr->set_source(player1, x, y);
			}
			else
			{
				if (!player2) {
					player2 = create_player2_piece(target);
				}
				cr->set_source(player2, x, y);
			}

			cr->paint();
		}
	}

	// draw the moving disks
	if (displayed_move_ != "")
	{
		const int move = atol(displayed_move_.c_str());
		get_coord(move, x, y);
		if (state_->get_current_player() == 1)
		{
			if (!player1) {
				player1 = create_player1_piece(target);
			}
			cr->set_source(player1, x, y);
		}
		else
		{
			if (!player2) {
				player2 = create_player2_piece(target);
			}
			cr->set_source(player2, x, y);
		}
		cr->paint();

		Cairo::RefPtr<Cairo::Surface> move_gfx = create_moving_piece(target);
		for (int i = 0; i < moving_pieces_.size(); ++i) {
			get_coord(moving_pieces_[i], x, y);
			cr->set_source(move_gfx, x, y);
			cr->paint();
		}
	}
}

bool Reversi::is_moving(const int square) const
{
	for (int i = 0; i < moving_pieces_.size(); ++i) {
		if (moving_pieces_[i] == square) {
			return true;
		}
	}
	return false;
}

Cairo::RefPtr<Cairo::Surface> Reversi::create_moving_piece(const Cairo::RefPtr<Cairo::Surface>& target)
{
	// create the surface and its context
	Cairo::RefPtr<Cairo::Surface> surface = Cairo::Surface::create(
		target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_
	);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(surface);

	const double half = square_size_ / 2.0;

	// create a dark and a light gradient
	Cairo::RefPtr<Cairo::RadialGradient> gradient_dark = Cairo::RadialGradient::create(
		half, half, 0.0,
		half, half, square_size_ / 2.75
	);
	gradient_dark->add_color_stop_rgb(0.0, 0.15, 0.15, 0.15);
	gradient_dark->add_color_stop_rgb(1.0, 0.0, 0.0, 0.0);

	Cairo::RefPtr<Cairo::RadialGradient> gradient_light = Cairo::RadialGradient::create(
		half, half, 0.0,
		half, half, square_size_ / 2.75
	);
	gradient_light->add_color_stop_rgb(0.0, 1.0, 1.0, 1.0);
	gradient_light->add_color_stop_rgb(1.0, 0.75, 0.75, 0.75);

	// get the disk dimensions
	double scaling_disk;
	double disk_edge_pourcent;
	if (displayed_pourcent_ < 0.5) {
		scaling_disk = 2.0 * (0.5 - displayed_pourcent_);
		disk_edge_pourcent = 2.0 * displayed_pourcent_;
	} else {
		scaling_disk = 2.0 * (displayed_pourcent_ - 0.5);
		disk_edge_pourcent = 2.0 * (1.0 - displayed_pourcent_);
	}

	static const double DISK_EDGE_RATIO = 0.12;
	const double HALF_DISK_EDGE = (square_size_ * DISK_EDGE_RATIO * disk_edge_pourcent) / 2.0;
	const double DISK_RADIUS = square_size_ / 2.70;
	const int current_player = state_->get_current_player();

	// draw the top edge
	if (current_player == 1) {
		set_light_color(context);
	} else {
		set_dark_color(context);
	}
	context->rectangle(half - DISK_RADIUS, half - HALF_DISK_EDGE, 2.0 * DISK_RADIUS, HALF_DISK_EDGE);
	context->fill();

	// draw the bottom edge
	if (current_player == 1) {
		set_dark_color(context);
	} else {
		set_light_color(context);
	}
	context->rectangle(half - DISK_RADIUS, half, 2.0 * DISK_RADIUS, HALF_DISK_EDGE);
	context->fill();

	// draw the round parts
	if (scaling_disk > 0.0)
	{
		context->save();
		context->scale(1.0, scaling_disk);

		if (displayed_pourcent_ < 0.5)
		{
			// the bottom of the disk
			if (current_player == 1) {
				set_dark_color(context);
			} else {
				set_light_color(context);
			}
			context->arc(half, (half + HALF_DISK_EDGE) / scaling_disk, DISK_RADIUS, 0.0, 2.0 * M_PI);
			context->fill();

			// the middle of the edge
			if (current_player == 1) {
				set_light_color(context);
			} else {
				set_dark_color(context);
			}
			context->arc(half, half / scaling_disk, DISK_RADIUS, 0.0, M_PI);
			context->fill();

			// the top of the disk
			if (current_player == 1) {
				context->set_source(gradient_light);
			} else {
				context->set_source(gradient_dark);
			}
			context->arc(half, (half - HALF_DISK_EDGE) / scaling_disk, DISK_RADIUS, 0.0, 2.0 * M_PI);
			context->fill();
		}
		else
		{
			if (current_player == 1) {
				set_light_color(context);
			} else {
				set_dark_color(context);
			}
			context->arc(half, (half - HALF_DISK_EDGE) / scaling_disk, DISK_RADIUS, 0.0, 2.0 * M_PI);
			context->fill();

			if (current_player == 1) {
				set_dark_color(context);
			} else {
				set_light_color(context);
			}
			context->arc(half, half / scaling_disk, DISK_RADIUS, M_PI, 2.0 * M_PI);
			context->fill();

			if (current_player == 1) {
				context->set_source(gradient_dark);
			} else {
				context->set_source(gradient_light);
			}
			context->arc(half, (half + HALF_DISK_EDGE) / scaling_disk, DISK_RADIUS, 0.0, 2.0 * M_PI);
			context->fill();
		}

		context->restore();
	}

	return surface;
}

Cairo::RefPtr<Cairo::Surface> Reversi::create_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double start_red, const double start_green, const double start_blue, const double end_red, const double end_green, const double end_blue)
{
	Cairo::RefPtr<Cairo::Surface> piece = Cairo::Surface::create(
		target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_
	);
	Cairo::RefPtr<Cairo::Context> piece_cxt = Cairo::Context::create(piece);

	const double half = square_size_ / 2.0;
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(
		half, half, 0.0,
		half, half, square_size_ / 2.75
	);
	gradient->add_color_stop_rgb(0.0, start_red, start_green, start_blue);
	gradient->add_color_stop_rgb(1.0, end_red, end_green, end_blue);

	piece_cxt->set_source(gradient);
	piece_cxt->arc(half, half, square_size_ / 2.70, 0.0, 2.0 * M_PI);
	piece_cxt->fill();

	return piece;
}
