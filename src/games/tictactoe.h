#ifndef BOARDGAME_TICTACTOE_H
#define BOARDGAME_TICTACTOE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"

class TicTacToe : public Game
{
public:
	TicTacToe(const GameDifficulty difficulty, const GameNumberPlayers nb_players = GAME_1_PLAYER);

	~TicTacToe();

	void reset();

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	bool update_animation();

private:
	double get_board_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
	Cairo::RefPtr<Cairo::Surface> create_circle(const Cairo::RefPtr<Cairo::Surface>& target, const double size);
	Cairo::RefPtr<Cairo::Surface> create_cross(const Cairo::RefPtr<Cairo::Surface>& target, const double size);
};

#endif //BOARDGAME_TICTACTOE_H
