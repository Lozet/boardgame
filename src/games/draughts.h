#ifndef BOARDGAME_DRAUGHTS_H
#define BOARDGAME_DRAUGHTS_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"
#include "color.h"

class Draughts : public Game
{
public:
	enum Variant {
		BRAZILIAN_VARIANT,
		CANADIAN_VARIANT,
		ENGLISH_VARIANT,
		INTERNATIONAL_VARIANT
	};

	Draughts(const GameDifficulty difficulty, const GameNumberPlayers nb_players, const Variant variant);

	~Draughts();

	virtual void reset();

	std::string get_status() const;

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	bool update_animation();

	void set_displayed_move(const std::string& move);

private:
	Variant variant_;

	int selected_piece_;

	int current_pos_;

	std::string current_move_;

	/* geometry information */
	int square_by_side_;
	int square_size_;

	int total_distance_;
	int current_phase_;

	double anim_speed_;

	double pos_x_;
	double pos_y_;
	double mx_;
	double my_;

	/** Is the board reversed for player 2? */
	bool reversed_;

	static const double ANIMATION_DURATION;

	/** The pieces size relative to the square size. */
	static const double PIECE_SIZE;
	/** The points radius relative to the square size. */
	static const double POINT_SIZE;
	/** The crosses size relative to the square size. */
	static const double CROSS_SIZE;
	/** The lines width relative to the square size. */
	static const double LINE_WIDTH;


	void create_new_game();

	bool try_move(const std::string& move);

	void change_animation_phase();

	void get_coord(const int square, double& x, double& y) const;

	void display_moves(const ::Cairo::RefPtr< ::Cairo::Context>& cr, const int square);

	void update_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	void draw_board(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	void draw_pieces(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	Cairo::RefPtr<Cairo::Surface> create_pion(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color);

	Cairo::RefPtr<Cairo::Surface> create_dame(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color);

	Cairo::RefPtr<Cairo::Surface> create_destination(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color);

	Cairo::RefPtr<Cairo::Surface> create_point(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color);

	Cairo::RefPtr<Cairo::Surface> create_cross(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color);
};

#endif //BOARDGAME_DRAUGHTS_H
