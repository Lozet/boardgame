// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/draughts.h"
#include "states/english_draughts_state.h"
#include "states/international_draughts_state.h"
#include "color.h"
#include <gtkmm/window.h>
#include <glibmm/i18n.h>
#include <glibmm/ustring.h>
#include <cmath>
#include <cassert>

const double Draughts::ANIMATION_DURATION = 8.0;
const double Draughts::PIECE_SIZE = 0.4;
const double Draughts::POINT_SIZE = 0.06;
const double Draughts::CROSS_SIZE = 0.1;
const double Draughts::LINE_WIDTH = 1.0 / 30.0;

Draughts::Draughts(const GameDifficulty difficulty, const GameNumberPlayers nb_players, const Variant variant)
: Game(difficulty, nb_players),
  variant_(variant),
  selected_piece_(-1),
  current_pos_(-1),
  anim_speed_(0.0)
{
	create_new_game();

	square_by_side_ = static_cast<DraughtsState*>(state_)->get_board_size();
	square_size_ = -1;

	reversed_ = (players_[1] == PLAYER_TYPE_HUMAN && players_[0] == PLAYER_TYPE_COMPUTER);
}

Draughts::~Draughts()
{
}

void Draughts::create_new_game()
{
	switch (variant_) {
		case BRAZILIAN_VARIANT:
			state_ = new BrazilianDraughtsState;
			break;
		case CANADIAN_VARIANT:
			state_ = new CanadianDraughtsState;
			break;
		case ENGLISH_VARIANT:
			state_ = new EnglishDraughtsState;
			break;
		case INTERNATIONAL_VARIANT:
			state_ = new FrenchDraughtsState;
			break;
	}
}

void Draughts::reset()
{
	Game::reset();

	reversed_ = (players_[1] == PLAYER_TYPE_HUMAN && players_[0] == PLAYER_TYPE_COMPUTER);

	selected_piece_ = -1;
	current_move_ = "";

	delete state_;
	create_new_game();
}

int Draughts::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 500;

		case DIFFICULTY_EASY:
			return 1500;

		case DIFFICULTY_NORMAL:
			return 5000;

		case DIFFICULTY_HARD:
			return 15000;

		case DIFFICULTY_VERY_HARD:
			return 30000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

bool Draughts::update_animation()
{
	displayed_pourcent_ += anim_speed_;

	if (displayed_pourcent_ >= 1.0) {
		// play the move
		play_displayed_move();
		// reset the game selection state
		selected_piece_ = -1;
		current_move_ = "";
		current_pos_ = -1;
		// stop the timer
		return false;
	} else if (total_distance_ > 1 && displayed_pourcent_ > current_phase_ * (2.0 / total_distance_)) {
		// change direction
		change_animation_phase();
		return true;
	} else {
		pos_x_ += mx_;
		pos_y_ += my_;
		return true;
	}
}

void Draughts::change_animation_phase()
{
	++current_phase_;

	std::string str = displayed_move_.c_str();
	std::string::size_type sz;

	// remove previous movements
	for (int i = 1; i < current_phase_; ++i) {
		std::stoi(str, &sz);
		str.erase(0, sz + 1);
	}

	int source = std::stoi(str, &sz) - 1;
	//const char action = str[sz];
	str.erase(0, sz + 1);

	int destination = std::stoi(str, &sz) - 1;
	str.erase(0, sz);

	double src_x, src_y;
	double dst_x, dst_y;
	get_coord(source, src_x, src_y);
	get_coord(destination, dst_x, dst_y);

	pos_x_ = src_x;
	pos_y_ = src_y;

	// set the moving piece movement
	if (total_distance_ == 1) {
		// for a move the distance is 1
		mx_ = (dst_x - src_x) / ANIMATION_DURATION;
		my_ = (dst_y - src_y) / ANIMATION_DURATION;
	} else {
		// for a jump the distance is 2
		mx_ = ((dst_x - src_x) / 2.0) / ANIMATION_DURATION;
		my_ = ((dst_y - src_y) / 2.0) / ANIMATION_DURATION;
	}
}

void Draughts::set_displayed_move(const std::string& move)
{
	Game::set_displayed_move(move);

	// set the animation speed from the number of crossed squares
	total_distance_ = std::count(move.begin(), move.end(), '-');
	if (total_distance_ == 0) {
		total_distance_ = std::count(move.begin(), move.end(), 'x') * 2;
	}
	anim_speed_ = 1.0 / (total_distance_ * ANIMATION_DURATION);

	// set the moving piece position and movement
	selected_piece_ = std::stoi(move) - 1;

	current_phase_ = 0;
	change_animation_phase();
}

std::string Draughts::get_status() const
{
	std::string s = Game::get_status();

	const int draw_counter = static_cast<DraughtsState*>(state_)->get_draw_counter();
	const int king_moves = static_cast<DraughtsState*>(state_)->get_king_moves();

	if (draw_counter != 0) {
		s += " / " + Glib::ustring::compose(ngettext(_("Draw in %1 turn"), _("Draw in %1 turns"), draw_counter), draw_counter);
	} else if (king_moves != 0) {
		s += " / " + Glib::ustring::compose(_("Number of king moves: %1"), king_moves);
	}

	return s;
}

bool Draughts::handle_click(GdkEventButton* release_event)
{
	bool ok = false;

	//  human turn and left click and not already playing
	if (is_human_turn() && release_event->button == 1 && displayed_move_ == "")
	{
		assert(square_size_ != -1);

		int x = release_event->x / square_size_;
		if (x < 0 || x > square_by_side_ - 1) {
			return false;
		}
		int y = release_event->y / square_size_;
		if (y < 0 || y > square_by_side_ - 1) {
			return false;
		}

		// check that the click wasn't on a non playable square
		if ((x % 2) == (y % 2)) {
			return false;
		}

		if (reversed_) {
			x = square_by_side_ - 1 - x;
			y = square_by_side_ - 1 - y;
		}

		const int square = (y * (square_by_side_ / 2)) + (x / 2);
		const int current_player = state_->get_current_player();
		const DraughtsState::Square* board = static_cast<DraughtsState*>(state_)->get_board();
		Glib::RefPtr<Gdk::Window> win = Glib::wrap(release_event->window, true);

		if (selected_piece_ == square) {
			// unselect a square
			selected_piece_ = -1;
			current_move_ = "";
			win->invalidate(false);
		}
		else if ((selected_piece_ == -1 || board[selected_piece_].player == current_player)
				&& board[square].player == current_player && board[square].type != DraughtsState::SQUARE_EMPTY) {
			selected_piece_ = square;
			current_pos_ = square;
			current_move_ = std::to_string(selected_piece_ + 1);
			win->invalidate(false);
		}
		else if (selected_piece_ != -1) {
			// check if the clicked square is a valid destination
			std::list<std::string> moves;
			static_cast<DraughtsState*>(state_)->get_moves_from(moves, selected_piece_);

			if (!moves.empty())
			{
				std::string move = current_move_ + "-" + std::to_string(square + 1);
				std::string jump = current_move_ + "x" + std::to_string(square + 1);
				bool found = false;

				for (auto it = moves.cbegin(); !found && it != moves.cend(); ++it) {
					if ((*it) == move) {
						// valid move
						ok = try_move(move);
						found =  true;
					} else if ((*it) == jump) {
						// jump valid
						ok = try_move(jump);
						found =  true;
					} else if ((*it).find(jump) == 0) { // partial jump
						// partial jump
						current_move_ = jump;
						// update the selected piece position
						current_pos_ = square;
						win->invalidate(false);
						found =  true;
					}
				}
			}
		}
	}

	return ok;
}

bool Draughts::try_move(const std::string& move)
{
	bool ok = static_cast<DraughtsState*>(state_)->try_move(move);

	if (ok) {
		// set the current move animation
		set_displayed_move(move);
	} else { // if the move isn't valid
		current_move_ = std::to_string(selected_piece_ + 1);
	}

	return ok;
}

void Draughts::get_coord(const int square, double& x, double& y) const
{
	assert(square_size_ != -1);
	const int USED_SQUARE_BY_SIDE = square_by_side_ / 2;

	y = (square / USED_SQUARE_BY_SIDE) * square_size_;

	x = (square % USED_SQUARE_BY_SIDE) * (square_size_ * 2.0);
	if ((square / USED_SQUARE_BY_SIDE) % 2 == 0) {
		x += square_size_;
	}

	if (reversed_) {
		const double delta = square_size_ * (square_by_side_ - 1);
		x = delta - x;
		y = delta - y;
	}
	//printf("square=%d, square_size=%d, x=%lf, y=%lf\n", square, square_size_, x, y);
}

void Draughts::update_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);

	square_size_ = (int) (std::min(width, height) / square_by_side_);
}

void Draughts::draw_board(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	const int board_size = square_size_ * square_by_side_;
	Color color;

	// create the squares (2x2)
	Cairo::RefPtr<Cairo::Surface> squares = Cairo::Surface::create(
		cr->get_target(), Cairo::CONTENT_COLOR_ALPHA,
		square_size_ * 2, square_size_ * 2
	);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(squares);

	static_cast<DraughtsState*>(state_)->get_light_square_color(color);
	context->set_source_rgb(color.r, color.g, color.b);
	context->rectangle(0.0, 0.0, square_size_, square_size_);
	context->rectangle(square_size_, square_size_, square_size_, square_size_);
	context->fill();

	static_cast<DraughtsState*>(state_)->get_dark_square_color(color);
	context->set_source_rgb(color.r, color.g, color.b);
	context->rectangle(square_size_, 0.0, square_size_, square_size_);
	context->rectangle(0.0, square_size_, square_size_, square_size_);
	context->fill();

	// fill the board with squares
	cr->set_source(squares, 0.0, 0.0);
	Cairo::RefPtr<Cairo::SurfacePattern> pattern = Cairo::RefPtr<Cairo::SurfacePattern>::cast_static(cr->get_source());
	pattern->set_extend(Cairo::EXTEND_REPEAT);
	cr->rectangle(0.0, 0.0, board_size, board_size);
	cr->fill();

	// add a gradient
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(
		board_size / 2.0, board_size / 2.0, 0.0,
		board_size / 2.0, board_size / 2.0, board_size / 2.0
	);
	gradient->add_color_stop_rgba(0.0, 0.2, 0.1, 0.1, 0.025);
	gradient->add_color_stop_rgba(1.0, 0.2, 0.1, 0.1, 0.1);
	cr->set_source(gradient);
	cr->rectangle(0.0, 0.0, board_size, board_size);
	cr->fill();
}

Cairo::RefPtr<Cairo::Surface> Draughts::create_pion(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color)
{
	Cairo::RefPtr<Cairo::Surface> piece = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(piece);

	context->set_line_width(LINE_WIDTH * square_size_);

	context->set_source_rgb(color.r, color.g, color.b);
	context->arc(square_size_ / 2.0, square_size_ / 2.0, PIECE_SIZE * square_size_, 0.0, 2.0 * M_PI);
	context->fill_preserve();

	context->set_source_rgb(color.r / 4.0, color.g / 5.0, color.b / 5.0);
	context->stroke();

	return piece;
}

Cairo::RefPtr<Cairo::Surface> Draughts::create_dame(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color)
{
	Cairo::RefPtr<Cairo::Surface> piece = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(piece);

	context->set_line_width(LINE_WIDTH * square_size_);

	context->set_source_rgb(color.r, color.g, color.b);
	context->arc(square_size_ / 2.0, 28.0 * square_size_ / 50.0, PIECE_SIZE * square_size_, 0.0, 2.0 * M_PI);
	context->fill_preserve();

	context->set_source_rgb(color.r / 4.5, color.g / 4.5, color.b / 4.5);
	context->stroke();

	context->set_source_rgb(color.r, color.g, color.b);
	context->arc(square_size_ / 2.0, 22.0 * square_size_ / 50.0, PIECE_SIZE * square_size_, 0.0, 2.0 * M_PI);
	context->fill_preserve();

	context->set_source_rgb(color.r / 4.5, color.g / 4.5, color.b / 4.5);
	context->stroke();

	return piece;
}

void Draughts::draw_pieces(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	DraughtsState* game = static_cast<DraughtsState*>(state_);
	Cairo::RefPtr<Cairo::Surface> target = cr->get_target();
	const DraughtsState::Square* board = game->get_board();
	const int nb_squares = square_by_side_ * square_by_side_ / 2;
	Cairo::RefPtr<Cairo::Surface> pion1, pion2, dame1, dame2;
	Color color;
	double x, y;

	for (int i = 0; i < nb_squares; ++i) {
		if ((board[i].type != DraughtsState::SQUARE_EMPTY) && (selected_piece_ != i)) {
			const DraughtsState::Square& s = board[i];
			get_coord(i, x, y);

			if (s.player == 1)
			{
				if (s.type == DraughtsState::SQUARE_PION)
				{
					if (!pion1) {
						game->get_player1_color(color);
						pion1 = create_pion(target, color);
					}
					cr->set_source(pion1, x, y);
				}
				else
				{
					if (!dame1) {
						game->get_player1_color(color);
						dame1 = create_dame(target, color);
					}
					cr->set_source(dame1, x, y);
				}
			}
			else // if (s.player == 2)
			{
				if (s.type == DraughtsState::SQUARE_PION)
				{
					if (!pion2) {
						game->get_player2_color(color);
						pion2 = create_pion(target, color);
					}
					cr->set_source(pion2, x, y);
				}
				else
				{
					if (!dame2) {
						game->get_player2_color(color);
						dame2 = create_dame(target, color);
					}
					cr->set_source(dame2, x, y);
				}
			}

			cr->paint();
		}
	}


	if (displayed_move_ != "") {
		// display the moving piece

		if (board[selected_piece_].type == DraughtsState::SQUARE_PION) {
			if (state_->get_current_player() == 1) {
				if (!pion1) {
					game->get_player1_color(color);
					pion1 = create_pion(target, color);
				}
				cr->set_source(pion1, pos_x_, pos_y_);
			} else {
				if (!pion2) {
					game->get_player2_color(color);
					pion2 = create_pion(target, color);
				}
				cr->set_source(pion2, pos_x_, pos_y_);
			}
		} else {
			if (state_->get_current_player() == 1) {
				if (!dame1) {
					game->get_player1_color(color);
					dame1 = create_dame(target, color);
				}
				cr->set_source(dame1, pos_x_, pos_y_);
			} else {
				if (!dame2) {
					game->get_player2_color(color);
					dame2 = create_dame(target, color);
				}
				cr->set_source(dame2, pos_x_, pos_y_);
			}
		}

		cr->paint();

	} else if (selected_piece_ != -1) {
		// display the selected piece

		// set the color
		if (board[selected_piece_].player == 1) {
			game->get_player1_highlight_color(color);
		} else if (board[selected_piece_].player == 2) {
			game->get_player2_highlight_color(color);
		}

		// create the highlighted piece
		Cairo::RefPtr<Cairo::Surface> highlight;
		if (board[selected_piece_].type == DraughtsState::SQUARE_PION) {
			highlight = create_pion(target, color);
		} else {
			highlight = create_dame(target, color);
		}

		get_coord(current_pos_, x, y);

		cr->set_source(highlight, x, y);
		cr->paint();

		// display available move
		display_moves(cr, selected_piece_);
	}
}

void Draughts::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	update_square_size(cr);

	// draw the board
	draw_board(cr);

	// draw the pieces
	draw_pieces(cr);
}

void Draughts::display_moves(const ::Cairo::RefPtr< ::Cairo::Context>& cr, const int square)
{
	DraughtsState* game = static_cast<DraughtsState*>(state_);
	std::list<std::string> moves;

	// get the moves to display
	game->get_moves_from(moves, square);
	if (moves.size() < 1) {
		return;
	}

	std::string::size_type sz;
	Color color;
	double x, y;

	// set the color
	if (state_->get_current_player() == 1) {
		game->get_player1_highlight_color(color);
	} else {
		game->get_player2_highlight_color(color);
	}

	// create destination gfx
	Cairo::RefPtr<Cairo::Surface> dest_surface = create_destination(cr->get_target(), color);
	// these surfaces are only created if a capture is possible
	Cairo::RefPtr<Cairo::Surface> point_surface, cross_surface;

	for (auto it = moves.begin(); it != moves.end(); ++it) {
		std::string& str = *it;

		// eat the first number and the next character
		std::stoi(str, &sz);
		str.erase(0, sz);

		const char action = str[0];
		if (action == '-')
		{
			str.erase(0, 1);
			const int destination = std::stoi(str, &sz) - 1;

			get_coord(destination, x, y);
			cr->set_source(dest_surface, x, y);
			cr->paint();
		}
		else if (action == 'x')
		{
			if (!point_surface) {
				point_surface = create_point(cr->get_target(), color);
				cross_surface = create_cross(cr->get_target(), color);
			}

			int source, target;
			int destination = square;

			do {
				// remove the 'x' operator
				assert(str.at(0) == 'x');
				str.erase(0, 1);

				// set the new source and destinaion
				source = destination;
				destination = std::stoi(str, &sz) - 1;
				str.erase(0, sz);

				// draw a point on destination
				get_coord(destination, x, y);
				if (str.empty()) {
					cr->set_source(dest_surface, x, y);
				} else {
					cr->set_source(point_surface, x, y);
				}
				cr->paint();

				// get the jump target
				target = game->get_jump_target(source, destination);

				// draw a crosss on target
				get_coord(target, x, y);
				cr->set_source(cross_surface, x, y);
				cr->paint();

			} while (!str.empty());
		}
	}
}

Cairo::RefPtr<Cairo::Surface> Draughts::create_destination(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color)
{
	Cairo::RefPtr<Cairo::Surface> surface = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(surface);

	const double center = square_size_ / 2.0;
	const double radius = POINT_SIZE * square_size_;

	context->set_source_rgb(color.r, color.g, color.b);
	context->arc(center, center, radius, 0.0, 2.0 * M_PI);
	context->fill();
	context->arc(center, center, radius * 3.0, 0.0, 2.0 * M_PI);
	context->stroke();

	return surface;
}

Cairo::RefPtr<Cairo::Surface> Draughts::create_point(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color)
{
	Cairo::RefPtr<Cairo::Surface> surface = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(surface);

	context->set_source_rgb(color.r, color.g, color.b);
	context->arc(square_size_ / 2.0, square_size_ / 2.0, POINT_SIZE * square_size_, 0.0, 2.0 * M_PI);
	context->fill();

	return surface;
}

Cairo::RefPtr<Cairo::Surface> Draughts::create_cross(const Cairo::RefPtr<Cairo::Surface>& target, const Color& color)
{
	Cairo::RefPtr<Cairo::Surface> surface = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size_, square_size_);
	Cairo::RefPtr<Cairo::Context> context = Cairo::Context::create(surface);

	const double center = square_size_ / 2.0;
	const double margin = CROSS_SIZE * square_size_;

	context->set_line_width(LINE_WIDTH * square_size_);
	context->set_source_rgb(color.r, color.g, color.b);

	context->move_to(center - margin, center - margin);
	context->line_to(center + margin, center + margin);
	context->stroke();
	context->move_to(center + margin, center - margin);
	context->line_to(center - margin, center + margin);
	context->stroke();

	return surface;
}
