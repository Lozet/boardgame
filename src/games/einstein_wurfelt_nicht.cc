// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/einstein_wurfelt_nicht.h"
#include "states/einstein_wurfelt_nicht_state.h"
#include <gtkmm/window.h>
#include <glibmm/i18n.h>
#include <cmath>
#include <cassert>

//TODO During setup, each player can arrange the cubes as he or she sees fit within the triangular area of their own color.

const double EinSteinWurfeltNicht::SQUARE_SIDE = 1.0 / 5.0;

EinSteinWurfeltNicht::EinSteinWurfeltNicht(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: Game(difficulty, nb_players),
  selected_piece_(-1)
{
	state_ = new EinSteinWurfeltNichtState;
}

EinSteinWurfeltNicht::~EinSteinWurfeltNicht()
{
}

void EinSteinWurfeltNicht::reset()
{
	Game::reset();

	selected_piece_ = -1;

	delete state_;
	state_ = new EinSteinWurfeltNichtState;
}

std::string EinSteinWurfeltNicht::get_status() const
{
	if (state_->is_random_move()) {
		return Game::get_status();
	} else {
		const int dice = static_cast<EinSteinWurfeltNichtState*>(state_)->get_dice();
		return Game::get_status() + " / " + Glib::ustring::compose(_("dice = %1"), dice);
	}
}

int EinSteinWurfeltNicht::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 500;

		case DIFFICULTY_EASY:
			return 2000;

		case DIFFICULTY_NORMAL:
			return 5000;

		case DIFFICULTY_HARD:
			return 20000;

		case DIFFICULTY_VERY_HARD:
			return 100000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

double EinSteinWurfeltNicht::get_uctk() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_EASY:
			return 1.0;

		default:
		case DIFFICULTY_NORMAL:
			return 0.4;

		case DIFFICULTY_HARD:
			return 0.8;
	}
}

bool EinSteinWurfeltNicht::update_animation()
{
	if (state_->is_random_move())
	{
		play_displayed_move();
		return false;
	}
	else
	{
		displayed_pourcent_ += 1.0 / 9.0;
		if (displayed_pourcent_ >= 1.0) {
			// play the move
			play_displayed_move();
			selected_piece_ = -1;
			// stop the timer
			return false;
		} else {
			pos_x_ += mx_;
			pos_y_ += my_;
			return true;
		}
	}
}

void EinSteinWurfeltNicht::set_displayed_move(const std::string& move)
{
	Game::set_displayed_move(move);

	if (!state_->is_random_move()) {
		const int src_x = 4 - (displayed_move_.at(0) - 'a');
		const int src_y = (displayed_move_.at(1) - '0') - 1;
		const int dst_x = 4 - (displayed_move_.at(3) - 'a');
		const int dst_y = (displayed_move_.at(4) - '0') - 1;
		pos_x_ = src_x;
		pos_y_ = src_y;
		mx_ = (dst_x - src_x) / 9.0;
		my_ = (dst_y - src_y) / 9.0;
		selected_piece_ = src_x + src_y * EinSteinWurfeltNichtState::NB_COLUMNS;
	}
}

bool EinSteinWurfeltNicht::handle_click(GdkEventButton* release_event)
{
	EinSteinWurfeltNichtState* ewns = static_cast<EinSteinWurfeltNichtState*>(state_);
	bool ok = false;

	//  human turn and left click and not already playing
	if (is_human_turn() && release_event->button == 1 && displayed_move_ == "")
	{
		Glib::RefPtr<Gdk::Window> win = Glib::wrap(release_event->window, true);
		const double size = std::min(win->get_width() / 6.0, win->get_height() / 5.0);
		const double axis_width = (1.0 / 24.0) * (size * 6.0);
		const double square_size = size - (axis_width * 2.0 / 5.0);

		const int x = (release_event->x - axis_width) / square_size;
		const int y = (release_event->y - axis_width) / square_size;
		if (y < 0 || y > 4 || x < 0 || x > 4) {
			return false;
		}

		const EinSteinWurfeltNichtState::Square* board = ewns->get_board();
		const int current_player = state_->get_current_player();
		const int square = (y * 5) + x;

		if (selected_piece_ == square) {
			// unselect a square
			selected_piece_ = -1;
			win->invalidate(false);
		}
		else if (selected_piece_ == -1 && board[square].player == current_player && ewns->is_playable(board[square].number)) {
			selected_piece_ = square;
			win->invalidate(false);
		}
		else if (selected_piece_ != -1) {
			// check if the clicked square is a valid destination
			ok = ewns->try_move(selected_piece_ % 5, selected_piece_ / 5, square % 5, square / 5);
			if (ok) {
				// set the current move animation
				const std::string m = (char) (4 - selected_piece_ % 5 + 'a') +
					std::to_string(selected_piece_ / 5 + 1) + '-' +
					(char) (4 - square % 5 + 'a') + std::to_string(square / 5 + 1);
				set_displayed_move(m);
			}
		}
	}

	return ok;
}

void EinSteinWurfeltNicht::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);

	const double size = std::min(width / 6.0, height / 5.0);
	width = size * 6.0;
	height = size * 5.0;
	const double axis_width = 1.0 / 24.0 * width;

	cr->save();
	cr->scale(5.0 * size, height);
	draw_axis(cr);
	cr->restore();

	cr->save();
	cr->translate(axis_width, axis_width);
	cr->scale((5.0 * size) - (axis_width * 2.0), height - (axis_width * 2.0));
	draw_board(cr);
	cr->restore();

	cr->save();
	cr->translate(5.0 * size, 2.0 * size);
	cr->scale(size, size);
	draw_dice(cr);
	cr->restore();
}

void EinSteinWurfeltNicht::draw_axis(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	for (int i = 0; i < 5; ++i) {
		draw_axis_name(cr, i * SQUARE_SIDE + (SQUARE_SIDE / 2.0), 0.02, 'E'- i);
		draw_axis_name(cr, i * SQUARE_SIDE + (SQUARE_SIDE / 2.0), 1.0 - 0.02, 'E'- i);

		draw_axis_name(cr, 0.023, i * SQUARE_SIDE + (SQUARE_SIDE / 2.0), i + '1');
		draw_axis_name(cr, 1.0 - 0.023, i * SQUARE_SIDE + (SQUARE_SIDE / 2.0), i + '1');
	}
}

void EinSteinWurfeltNicht::draw_axis_name(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y, const char name)
{
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->select_font_face("Sans", Cairo::FontSlant::FONT_SLANT_NORMAL, Cairo::FontWeight::FONT_WEIGHT_NORMAL);
	cr->set_font_size(0.03);

	const std::string text(1, name);

	Cairo::TextExtents extends;
	cr->get_text_extents(text, extends);

	cr->move_to(x - (extends.width / 2.0), y + (extends.height / 2.0));
	cr->show_text(text);
}

void EinSteinWurfeltNicht::draw_dice(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	// draw the structure
	cr->move_to(0.427505185, 0.06397772);
	cr->curve_to(0.349322483, 0.069963235, 0.299028899, 0.08710435, 0.226424021, 0.148869741);
	cr->curve_to(0.154349214, 0.230687104, 0.075793821, 0.317100499, 0.047904221, 0.447089758);
	cr->curve_to(0.001702038, 0.731578066, 0.197358897, 0.841359423, 0.272994067, 0.881746264);
	cr->curve_to(0.345437221, 0.907618405, 0.415090979, 0.944647158, 0.490322561, 0.959363169);
	cr->curve_to(0.555003396, 0.969427293, 0.619684714, 0.962757946, 0.684365549, 0.947721382);
	cr->curve_to(0.849129067, 0.867385565, 0.907912008, 0.667124612, 0.944383172, 0.522641894);
	cr->curve_to(0.962410443, 0.432942923, 0.961033608, 0.3275069, 0.917216749, 0.272451359);
	cr->curve_to(0.863889493, 0.199565707, 0.757571465, 0.123891577, 0.623363838, 0.08726313);
	cr->curve_to(0.548017841, 0.054399978, 0.483450938, 0.055610647, 0.427505185, 0.06397772);
	cr->close_path();

	double x = 0.276923;
	double y = 0.507812;
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(x, y, 0.0, x, y, 0.703749);
	gradient->add_color_stop_rgb(0.0, 0.941176471, 0.498039216, 0.490196078);
	gradient->add_color_stop_rgb(1.0, 0.384313725, 0.0, 0.0);
	cr->set_source(gradient);
	cr->fill_preserve();

	cr->set_source_rgb(0.588235294, 0.0, 0.0);
	cr->set_line_width(0.012069031);
	cr->stroke();

	// draw the top face
	x = 0.228950;
	y = 0.203126;
	Cairo::RefPtr<Cairo::RadialGradient> gradient2 = Cairo::RadialGradient::create(x, y, 0.0, x, y, 1.809091);
	gradient2->add_color_stop_rgb(0.0, 0.941176471, 0.498039216, 0.490196078);
	gradient2->add_color_stop_rgb(1.0, 0.384313725, 0.0, 0.0);
	cr->set_source(gradient2);

	cr->save();
	cr->translate(0.480978468, 0.342868808);
	cr->scale(0.35482133, 0.2547436);

	cr->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	cr->set_fill_rule(Cairo::FILL_RULE_EVEN_ODD);

	cr->fill();
	cr->restore();

	// draw the left face
	x = 4.327909e-2;
	y = 0.496591;
	Cairo::RefPtr<Cairo::RadialGradient> gradient3 = Cairo::RadialGradient::create(x, y, 0.0, x, y, 2.403461);
	gradient3->add_color_stop_rgb(0.0, 0.941176471, 0.498039216, 0.490196078);
	gradient3->add_color_stop_rgb(1.0, 0.384313725, 0.0, 0.0);
	cr->set_source(gradient3);

	cr->save();
	cr->translate(0.37, 0.76);
	cr->rotate_degrees(25.0);
	cr->scale(0.29, 0.145);

	cr->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	cr->set_fill_rule(Cairo::FILL_RULE_EVEN_ODD);

	cr->fill();
	cr->restore();

	// draw the right face
	x = 0.231891;
	y = -2.546936;
	Cairo::RefPtr<Cairo::RadialGradient> gradient4 = Cairo::RadialGradient::create(x, y, 0.0, x, y, 6.407288);
	gradient4->add_color_stop_rgb(0.0, 0.941176471, 0.498039216, 0.490196078);
	gradient4->add_color_stop_rgb(1.0, 0.384313725, 0.0, 0.0);
	cr->set_source(gradient4);

	cr->save();
	cr->translate(0.825, 0.65);
	cr->rotate_degrees(110.0);
	cr->scale(0.25, 0.08);

	cr->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	cr->set_fill_rule(Cairo::FILL_RULE_EVEN_ODD);

	cr->fill();
	cr->restore();

	// draw the points
	const int dice = static_cast<EinSteinWurfeltNichtState*>(state_)->get_dice();
	if (dice == 6) {
		// left points
		draw_point(cr, 0.468043005, 0.160202392);
		draw_point(cr, 0.372869255, 0.293535892);
		draw_point(cr, 0.271280505, 0.430043642);

		// right points
		draw_point(cr, 0.695383005, 0.247795767);
		draw_point(cr, 0.604614255, 0.385599392);
		draw_point(cr, 0.496679255, 0.522107329);
	} else {
		if (dice % 2) {
			// odd value have a central point
			draw_point(cr, 0.474456755, 0.334805517);
		}
		if (dice >= 2) {
			// left bottom point (2, 3, 4, 5, 6)
			draw_point(cr, 0.262053005, 0.400167517);
			// right top point (2, 3, 4, 5, 6)
			draw_point(cr, 0.686860505, 0.269443517);
		}
		if (dice >= 4) {
			// left top point (4, 5, 6)
			draw_point(cr, 0.392546755, 0.183594642);
			// right bottom point (4, 5, 6)
			draw_point(cr, 0.556034255, 0.488830179);
		}
	}
}

void EinSteinWurfeltNicht::draw_point(const ::Cairo::RefPtr< ::Cairo::Context>& cr, const double x, const double y)
{
	cr->save();
	cr->translate(x, y);
	cr->scale(0.096770331, 0.07257675);

	const double gx = 0.20;
	const double gy = -0.25;
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(gx, gy, 0.0, gx, gy, 1.5);
	gradient->add_color_stop_rgb(0.0, 1.0, 1.0, 1.0);
	gradient->add_color_stop_rgb(1.0, 0.396078431, 0.396078431, 0.4);
	cr->set_source(gradient);

	cr->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	cr->set_fill_rule(Cairo::FILL_RULE_EVEN_ODD);

	cr->fill();
	cr->restore();
}

void EinSteinWurfeltNicht::draw_board(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	cr->set_line_width(0.007);

	// draw the grid
	for (int x = 0; x <= EinSteinWurfeltNichtState::NB_ROWS; ++x) {
		cr->move_to(x * SQUARE_SIDE, 0.0);
		cr->line_to(x * SQUARE_SIDE, 1.0);
		cr->stroke();
	}
	for (int y = 0; y <= EinSteinWurfeltNichtState::NB_COLUMNS; ++y) {
		cr->move_to(0.0, y * SQUARE_SIDE);
		cr->line_to(1.0, y * SQUARE_SIDE);
		cr->stroke();
	}

	const EinSteinWurfeltNichtState::Square* board = static_cast<EinSteinWurfeltNichtState*>(state_)->get_board();
	double x, y;

	for (int i = 0; i < 25; ++i) {
		if (selected_piece_ != i && board[i].player != 0) {
			const EinSteinWurfeltNichtState::Square& s = board[i];
			x = i % 5;
			y = i / 5;

			if (s.player == 1) {
				cr->set_source_rgb(0.8, 0.0, 0.0);
			} else if (s.player == 2) {
				cr->set_source_rgb(0.0, 0.0, 0.8);
			}

			draw_piece(cr, x, y);
			draw_number(cr, x, y, s.number);
		}
	}

	if (!state_->is_random_move() && displayed_move_ != "") {
		// draw the moving piece
		if (state_->get_current_player() == 1) {
			cr->set_source_rgb(0.99, 0.0, 0.0);
		} else {
			cr->set_source_rgb(0.0, 0.0, 0.99);
		}
		draw_piece(cr, pos_x_, pos_y_);
		draw_number(cr, pos_x_, pos_y_, board[selected_piece_].number);

	} else if (selected_piece_ != -1) {
		
		x = selected_piece_ % 5;
		y = selected_piece_ / 5;

		if (board[selected_piece_].player == 1) {
			// draw the selected piece
			cr->set_source_rgb(0.99, 0.0, 0.0);
			draw_piece(cr, x, y);
			draw_number(cr, x, y, board[selected_piece_].number);

			// draw the available moves
			cr->set_source_rgba(1.0, 0.5, 0.5, 0.5);
			if (x < 4) {
				draw_piece(cr, x + 1, y);
			}
			if (y < 4) {
				draw_piece(cr, x, y + 1);
				if (x < 4) {
					draw_piece(cr, x + 1, y + 1);
				}
			}
		} else {
			// draw the selected piece
			cr->set_source_rgb(0.0, 0.0, 0.99);
			draw_piece(cr, x, y);
			draw_number(cr, x, y, board[selected_piece_].number);

			// draw the available moves
			cr->set_source_rgba(0.5, 0.5, 1.0, 0.5);
			if (y > 0) {
				draw_piece(cr, x, y - 1);
				if (x > 0) {
					draw_piece(cr, x - 1, y - 1);
				}
			}

			if (x > 0) {
				draw_piece(cr, x - 1, y);
			}
		}
	}
}

void EinSteinWurfeltNicht::draw_piece(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y)
{
	static const double LINE_MARGIN = 0.0034;

	cr->rectangle(x * SQUARE_SIDE + LINE_MARGIN, 
				y * SQUARE_SIDE + LINE_MARGIN, 
				SQUARE_SIDE - LINE_MARGIN * 2, 
				SQUARE_SIDE - LINE_MARGIN * 2);
	cr->fill();
}

void EinSteinWurfeltNicht::draw_number(const Cairo::RefPtr<Cairo::Context>& cr, const double x, const double y, const int number)
{
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->select_font_face("Sans", Cairo::FontSlant::FONT_SLANT_NORMAL, Cairo::FontWeight::FONT_WEIGHT_BOLD);
	cr->set_font_size(0.1);

	const std::string text = std::to_string(number);

	Cairo::TextExtents extends;
	cr->get_text_extents(text, extends);

	cr->move_to((x * SQUARE_SIDE) + (SQUARE_SIDE / 2.0) - (extends.width / 2.0), 
				(y * SQUARE_SIDE) + (SQUARE_SIDE / 2.0) + (extends.height / 2.0));
	cr->show_text(text);
}
