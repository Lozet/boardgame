#ifndef BOARDGAME_REVERSI_H
#define BOARDGAME_REVERSI_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"
#include <vector>

class Reversi : public Game
{
public:
	Reversi(const GameDifficulty difficulty, const GameNumberPlayers nb_players = GAME_1_PLAYER);

	~Reversi();

	void reset();

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	bool update_animation();

	void set_displayed_move(const std::string& move);

	std::string get_status() const;

private:
	int square_size_;

	std::vector<int> moving_pieces_;

	void update_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool is_moving(const int square) const;

	void get_coord(const int square, int& x, int& y) const;

	Cairo::RefPtr<Cairo::Surface> create_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double start_red, const double start_green, const double start_blue, const double end_red, const double end_green, const double end_blue);

	Cairo::RefPtr<Cairo::Surface> create_moving_piece(const Cairo::RefPtr<Cairo::Surface>& target);

	inline Cairo::RefPtr<Cairo::Surface> create_player1_piece(const Cairo::RefPtr<Cairo::Surface>& target) {
		return create_piece(target, 0.15, 0.15, 0.15, 0.0, 0.0, 0.0);
	};

	inline Cairo::RefPtr<Cairo::Surface> create_player2_piece(const Cairo::RefPtr<Cairo::Surface>& target) {
		return create_piece(target, 1.0, 1.0, 1.0, 0.75, 0.75, 0.75);
	};

	inline void set_light_color(const ::Cairo::RefPtr< ::Cairo::Context>& context) const {
		context->set_source_rgb(0.82, 0.82, 0.82);
	}

	inline void set_dark_color(const ::Cairo::RefPtr< ::Cairo::Context>& context) const {
		context->set_source_rgb(0.05, 0.05, 0.05);
	}
};

#endif // BOARDGAME_REVERSI_H
