#ifndef BOARDGAME_GAME_H
#define BOARDGAME_GAME_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"
#include "options.h"
#include <cairomm/context.h>
#include <gdkmm/event.h>
#include <vector>
#include <sigc++/sigc++.h>

class Game
{
public:
	/** Create a new game
		@param difficulty The game difficulty
		@param nb_players The number of human players.
	*/
	Game(const GameDifficulty difficulty, const GameNumberPlayers nb_players);

	virtual ~Game();

	virtual void reset();

	virtual std::string get_status() const;

	virtual void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr) = 0;

	virtual bool handle_click(GdkEventButton* release_event) = 0;

	virtual int get_nb_iter() const = 0;

	virtual double get_uctk() const { return 1.0; };

	virtual void set_displayed_move(const std::string& move);

	virtual bool update_animation();

	const GameState* get_state() const;

	bool is_human_turn() const;

	bool is_computer_turn() const;

	void export_history(FILE* fp) const;

	// signal accessors
	typedef sigc::signal<void, const int, const std::string& > type_signal_new_move;
	type_signal_new_move signal_new_move();

	typedef sigc::signal<void> type_signal_game_reset;
	type_signal_game_reset signal_game_reset();


	static const int NB_PLAYER = 2;

	enum PlayerType
	{
		PLAYER_TYPE_HUMAN,
		PLAYER_TYPE_COMPUTER
	};

protected:
	GameDifficulty difficulty_;

	GameState* state_;

	/** Human or computer. */
	PlayerType players_[NB_PLAYER];

	std::string displayed_move_;

	double displayed_pourcent_;

	std::vector<std::string> history_;

	type_signal_new_move signal_new_move_;
	type_signal_game_reset signal_game_reset_;


	virtual void play_displayed_move();
};

#endif //BOARDGAME_GAME_H
