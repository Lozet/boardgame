// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#define _USE_MATH_DEFINES
#include "games/connectfour.h"
#include "states/connectfour_state.h"
#include <gdkmm/window.h>
#include <cmath>
#include <cassert>

const double ConnectFour::RADIUS = 0.06;

ConnectFour::ConnectFour(const GameDifficulty difficulty, const GameNumberPlayers nb_players)
: Game(difficulty, nb_players),
  anim_speed_(0.0)
{
	state_ = new ConnectFourState;
}

ConnectFour::~ConnectFour()
{
}

void ConnectFour::reset()
{
	Game::reset();

	delete state_;
	state_ = new ConnectFourState;
}

int ConnectFour::get_nb_iter() const
{
	switch (difficulty_)
	{
		case DIFFICULTY_VERY_EASY:
			return 200;

		case DIFFICULTY_EASY:
			return 1000;

		case DIFFICULTY_NORMAL:
			return 5000;

		case DIFFICULTY_HARD:
			return 30000;

		case DIFFICULTY_VERY_HARD:
			return 70000;
	}
	assert("Difficulty not defined" == 0);
	return 0;
}

bool ConnectFour::update_animation()
{
	displayed_pourcent_ += anim_speed_;
	if (displayed_pourcent_ >= 1.0) {
		// play the move
		play_displayed_move();
		// stop the timer
		return false;
	} else {
		return true;
	}
}

void ConnectFour::set_displayed_move(const std::string& move)
{
	Game::set_displayed_move(move);
	set_anim_speed(std::stoi(move));
}

void ConnectFour::play_displayed_move()
{
	const int column = std::stoi(displayed_move_);

	// current player play his move
	Game::play_displayed_move();

	// if the game is finished by a win
	if (!state_->has_move_left() && state_->get_result(1) != 0.5) {
		// hightlight the connection
		static_cast<ConnectFourState*>(state_)->hightlight_winning_pieces(column);
	}
}

void ConnectFour::set_anim_speed(const int column)
{
	const int row = static_cast<ConnectFourState*>(state_)->get_free_row(column);
	anim_speed_ = 1.0 / ((row + 1) * 4.0);
}

bool ConnectFour::handle_click(GdkEventButton* release_event)
{
	bool ok = false;

	//  human turn and left click and not already playing
	if (is_human_turn() && release_event->button == 1 && release_event->y > 0 && displayed_move_ == "")
	{
		const double square_size = get_square_size(release_event->window);
		const int column = release_event->x / square_size;

		if (static_cast<ConnectFourState*>(state_)->try_move(column)) {
			// set the current move animation
			Game::set_displayed_move(std::to_string(column));
			set_anim_speed(column);
			ok = true;
		}
	}

	return ok;
}

void ConnectFour::draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	const double square_size = get_square_size(cr);
	Cairo::RefPtr<Cairo::Surface> target = cr->get_target();

	// draw pieces
	Cairo::RefPtr<Cairo::Surface> player1;
	Cairo::RefPtr<Cairo::Surface> player2;
	Cairo::RefPtr<Cairo::Surface> player1_win;
	Cairo::RefPtr<Cairo::Surface> player2_win;

	const int* game_board = static_cast<ConnectFourState*>(state_)->get_board();
	for (int x = 0; x < ConnectFourState::NB_COLUMNS; ++x) {
		for (int y = 0; y < ConnectFourState::NB_ROWS; ++y) {
			const int p = game_board[x * ConnectFourState::NB_ROWS + y];

			if (p == 1)
			{
				if (!player1) {
					player1 = create_player1_piece(target, square_size);
				}
				cr->set_source(player1, x * square_size, y * square_size);
				cr->paint();
			}
			else if (p == 2)
			{
				if (!player2) {
					player2 = create_player2_piece(target, square_size);
				}
				cr->set_source(player2, x * square_size, y * square_size);
				cr->paint();
			}
			if (p >= ConnectFourState::HIGHTLIGHT_PIECE)
			{
				if (p - ConnectFourState::HIGHTLIGHT_PIECE == 1)
				{
					if (!player1_win) {
						player1_win = create_player1_win_piece(target, square_size);
					}
					cr->set_source(player1_win, x * square_size, y * square_size);
					cr->paint();
				}
				else
				{
					if (!player2_win) {
						player2_win = create_player2_win_piece(target, square_size);
					}
					cr->set_source(player2_win, x * square_size, y * square_size);
					cr->paint();
				}
			}
		}
	}

	// draw the moving piece
	if (displayed_move_ != "") {
		const int column = std::stoi(displayed_move_);
		const int row = static_cast<ConnectFourState*>(state_)->get_free_row(column);
		const double x = column * square_size;
		const double y = (row * square_size) * displayed_pourcent_;
		if (state_->get_current_player() == 1) {
			if (!player1) {
				player1 = create_player1_piece(target, square_size);
			}
			cr->set_source(player1, x, y);
			cr->paint();
		} else {
			if (!player2) {
				player2 = create_player2_piece(target, square_size);
			}
			cr->set_source(player2, x, y);
			cr->paint();
		}
	}

	// draw the board
	const double board_width = square_size * ConnectFourState::NB_COLUMNS;
	const double board_height = square_size * ConnectFourState::NB_ROWS;

	Cairo::RefPtr<Cairo::Surface> board = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, board_width, board_height);
	Cairo::RefPtr<Cairo::Context> board_cxt = Cairo::Context::create(board);

	// the blue gradient used for the board
	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(
		board_width / 2.0, board_height / 2.0, 0.0,
		board_width / 2.0, board_height / 2.0, board_width * 1.4
	);
	gradient->add_color_stop_rgb(0.0, 0.0, 0.0, 0.7);
	gradient->add_color_stop_rgb(1.0, 0.0, 0.0, 0.1);
	board_cxt->set_source(gradient);

	// fill the whole board with the gradient
	board_cxt->rectangle(0.0, 0.0, board_width, board_height);
	board_cxt->fill();

	// draw the hole
	Cairo::RefPtr<Cairo::Surface> hole = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, square_size, square_size);
	Cairo::RefPtr<Cairo::Context> hole_cxt = Cairo::Context::create(hole);
	hole_cxt->set_source_rgba(0.0, 0.0, 0.0, 1.0);
	hole_cxt->arc(square_size / 2.0, square_size / 2.0, square_size / 2.75, 0.0, 2.0 * M_PI);
	hole_cxt->fill();

	// make the holes in the board
	board_cxt->set_operator(Cairo::OPERATOR_DEST_OUT);
	for (int x = 0; x < ConnectFourState::NB_COLUMNS; ++x) {
		for (int y = 0; y < ConnectFourState::NB_ROWS; ++y) {
			board_cxt->set_source(hole, x * square_size, y * square_size);
			board_cxt->rectangle(x * square_size, y * square_size, square_size, square_size);
			board_cxt->fill();
		}
	}

	// blit the board on the drawing area
	cr->set_source(board, 0.0, 0.0);
	cr->rectangle(0.0, 0.0, board_width, board_height);
	cr->fill();
}

double ConnectFour::get_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	double x, y, width, height;
	cr->get_clip_extents(x, y, width, height);

	const double horiz = width / ConnectFourState::NB_COLUMNS;
	const double vert = height / ConnectFourState::NB_ROWS;
	return std::min(horiz, vert);
}

double ConnectFour::get_square_size(GdkWindow *window)
{
	const double horiz = gdk_window_get_width(window) / ConnectFourState::NB_COLUMNS;
	const double vert = gdk_window_get_height(window) / ConnectFourState::NB_ROWS;
	return std::min(horiz, vert);
}

Cairo::RefPtr<Cairo::Surface> ConnectFour::create_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double start_red, const double start_green, const double start_blue, const double end_red, const double end_green, const double end_blue, const double size)
{
	const double half = size / 2.0;
	Cairo::RefPtr<Cairo::Surface> piece = Cairo::Surface::create(target, Cairo::CONTENT_COLOR_ALPHA, size, size);
	Cairo::RefPtr<Cairo::Context> piece_cxt = Cairo::Context::create(piece);

	Cairo::RefPtr<Cairo::RadialGradient> gradient = Cairo::RadialGradient::create(half, half, 0.0, half, half, size / 2.75);
	gradient->add_color_stop_rgb(0.0, start_red, start_green, start_blue);
	gradient->add_color_stop_rgb(1.0, end_red, end_green, end_blue);

	piece_cxt->set_source(gradient);
	piece_cxt->arc(half, half, size / 2.70, 0.0, 2.0 * M_PI);
	piece_cxt->fill();

	return piece;
}
