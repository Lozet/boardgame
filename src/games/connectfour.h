#ifndef BOARDGAME_CONNECTFOUR_H
#define BOARDGAME_CONNECTFOUR_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"

class ConnectFour : public Game
{
public:
	ConnectFour(const GameDifficulty difficulty, const GameNumberPlayers nb_players = GAME_1_PLAYER);

	~ConnectFour();

	void reset();

	void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	bool handle_click(GdkEventButton* release_event);

	int get_nb_iter() const;

	bool update_animation();

	void set_displayed_move(const std::string& move);

private:
	static const double RADIUS;

	double anim_speed_;

	void play_displayed_move();

	void set_anim_speed(const int column);

	double get_square_size(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
	double get_square_size(GdkWindow *window);

	Cairo::RefPtr<Cairo::Surface> create_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double start_red, const double start_green, const double start_blue, const double end_red, const double end_green, const double end_blue, const double size);

	inline Cairo::RefPtr<Cairo::Surface> create_player1_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double size) {
		return create_piece(target, 1.0, 0.0, 0.0, 0.6, 0.0, 0.0, size);
	};

	inline Cairo::RefPtr<Cairo::Surface> create_player2_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double size) {
		return create_piece(target, 1.0, 1.0, 0.0, 0.6, 0.6, 0.0, size);
	};

	inline Cairo::RefPtr<Cairo::Surface> create_player1_win_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double size) {
		return create_piece(target, 1.0, 0.0, 0.0, 0.8, 0.0, 0.0, size);
	};

	inline Cairo::RefPtr<Cairo::Surface> create_player2_win_piece(const Cairo::RefPtr<Cairo::Surface>& target, const double size) {
		return create_piece(target, 1.0, 1.0, 0.0, 0.8, 0.8, 0.0, size);
	};
};

#endif //BOARDGAME_CONNECTFOUR_H
