#ifndef BOARDGAME_OPTIONS_H
#define BOARDGAME_OPTIONS_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

typedef enum GameDifficulty
{
	DIFFICULTY_VERY_EASY,
	DIFFICULTY_EASY,
	DIFFICULTY_NORMAL,
	DIFFICULTY_HARD,
	DIFFICULTY_VERY_HARD
} GameDifficulty;

typedef enum GameNumberPlayers
{
	GAME_0_PLAYERS,
	GAME_1_PLAYER,
	GAME_2_PLAYERS
} GameNumberPlayers;

typedef enum GameId
{
	GAME_TIC_TAC_TOE,
	GAME_CONNECT_FOUR,
	GAME_BRAZILIAN_DRAUGHTS,
	GAME_CANADIAN_DRAUGHTS,
	GAME_INTERNATIONAL_DRAUGHTS,
	GAME_ENGLISH_DRAUGHTS,
	GAME_EINSTEIN_WURFELT_NICHT,
	GAME_NIM,
	GAME_REVERSI
} GameId;
#define NB_GAMES	9

struct GameOptions {
	GameId game;
	GameNumberPlayers nb_players;
	GameDifficulty difficulty;
	int nb_iterations;
	double uctk;
};

struct WindowOptions {
	bool maximized;
	int width;
	int height;
	bool toolbar_visible;
	bool statusbar_visible;
	bool history_visible;
};

#endif // BOARDGAME_OPTIONS_H
