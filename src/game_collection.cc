/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "game_collection.h"
#include "exception.h"
#include "games/connectfour.h"
#include "games/draughts.h"
#include "games/einstein_wurfelt_nicht.h"
#include "games/nim.h"
#include "games/reversi.h"
#include "games/tictactoe.h"
#include <glibmm/i18n.h>

namespace GameCollection {

/** The games descriptions. */
static std::vector<GameInfo> list_games_;

/** The games categories descriptions. */
static std::vector<GameCategory> list_categories_;

void init()
{
	// add the games
	list_games_.emplace_back(GAME_TIC_TAC_TOE, "tic-tac-toe", _("Tic-tac-toe"));
	list_games_.emplace_back(GAME_CONNECT_FOUR, "connect-four", _("Connect Four"));
	list_games_.emplace_back(GAME_BRAZILIAN_DRAUGHTS, "brazilian-draughts", _("Brazilian Draughts"));
	list_games_.emplace_back(GAME_CANADIAN_DRAUGHTS, "canadian-draughts", _("Canadian Draughts"));
	list_games_.emplace_back(GAME_INTERNATIONAL_DRAUGHTS, "international-draughts", _("International Draughts"));
	list_games_.emplace_back(GAME_ENGLISH_DRAUGHTS, "english-draughts", _("English Draughts"));
	list_games_.emplace_back(GAME_EINSTEIN_WURFELT_NICHT, "einstein-wurfelt-nicht", _("EinStein würfelt nicht!"));
	list_games_.emplace_back(GAME_NIM, "nim", _("Nim"));
	list_games_.emplace_back(GAME_REVERSI, "reversi", _("Reversi"));

	// add the game categories
	list_categories_.emplace_back("connect", _("Connection games"));
	GameCategory& connect = list_categories_.back();
	connect.games.push_back(&list_games_[GAME_TIC_TAC_TOE]);
	connect.games.push_back(&list_games_[GAME_CONNECT_FOUR]);

	list_categories_.emplace_back("draughts", _("Draughts"));
	GameCategory& draughts = list_categories_.back();
	draughts.games.push_back(&list_games_[GAME_BRAZILIAN_DRAUGHTS]);
	draughts.games.push_back(&list_games_[GAME_CANADIAN_DRAUGHTS]);
	draughts.games.push_back(&list_games_[GAME_INTERNATIONAL_DRAUGHTS]);
	draughts.games.push_back(&list_games_[GAME_ENGLISH_DRAUGHTS]);

	list_categories_.emplace_back("dice", _("Dice games"));
	GameCategory& dice = list_categories_.back();
	dice.games.push_back(&list_games_[GAME_EINSTEIN_WURFELT_NICHT]);

	list_categories_.emplace_back("octal", _("Octal games"));
	GameCategory& octal = list_categories_.back();
	octal.games.push_back(&list_games_[GAME_NIM]);

	list_categories_.emplace_back("reversi", _("Reversi"));
	GameCategory& reversi = list_categories_.back();
	reversi.games.push_back(&list_games_[GAME_REVERSI]);
}

Game* create_game(const GameOptions& options)
{
	Game* game;

	switch (options.game) {
		case GAME_TIC_TAC_TOE:
			game = new TicTacToe(options.difficulty, options.nb_players);
			break;
		case GAME_CONNECT_FOUR:
			game = new ConnectFour(options.difficulty, options.nb_players);
			break;

		case GAME_BRAZILIAN_DRAUGHTS:
			game = new Draughts(options.difficulty, options.nb_players, Draughts::BRAZILIAN_VARIANT);
			break;
		case GAME_CANADIAN_DRAUGHTS:
			game = new Draughts(options.difficulty, options.nb_players, Draughts::CANADIAN_VARIANT);
			break;
		case GAME_INTERNATIONAL_DRAUGHTS:
			game = new Draughts(options.difficulty, options.nb_players, Draughts::INTERNATIONAL_VARIANT);
			break;
		case GAME_ENGLISH_DRAUGHTS:
			game = new Draughts(options.difficulty, options.nb_players, Draughts::ENGLISH_VARIANT);
			break;

		case GAME_NIM:
			game = new Nim(options.difficulty, options.nb_players);
			break;

		case GAME_EINSTEIN_WURFELT_NICHT:
			game = new EinSteinWurfeltNicht(options.difficulty, options.nb_players);
			break;

		case GAME_REVERSI:
			game = new Reversi(options.difficulty, options.nb_players);
			break;

		default:
			throw MsgErr(_("Games not yet implemented"));
	}

	return game;
}

const std::vector<GameCategory>& get_categories()
{
	return list_categories_;
}

const GameInfo& get_info(const GameId id)
{
	return list_games_[id];
}

}
