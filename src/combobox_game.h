#ifndef BOARDGAME_COMBOBOX_GAME_H
#define BOARDGAME_COMBOBOX_GAME_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <gtkmm/combobox.h>
#include <gtkmm/liststore.h>
#include "game_info.h"

class ComboBoxGame : public Gtk::ComboBox
{
public:
	ComboBoxGame();

	virtual ~ComboBoxGame();

	void append_category(const GameCategory& category);

private:
	// model columns for the combobox
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{ add(col_id); add(col_name); add(col_sensitive); }

		Gtk::TreeModelColumn<Glib::ustring> col_id; // game id
		Gtk::TreeModelColumn<Glib::ustring> col_name; // game name
		Gtk::TreeModelColumn<bool> col_sensitive; // header or entry
	};

	ModelColumns columns_;

	Glib::RefPtr<Gtk::ListStore> refTreeModel_;

	void append(const Glib::ustring& id, const Glib::ustring& label, const bool sensitive);

	inline void append_game(const Glib::ustring& id, const Glib::ustring& label) {
		append(id, label, true);
	}

	inline void append_header(const Glib::ustring& id, const Glib::ustring& label) {
		append(id, "<span style=\"italic\" weight=\"bold\">--- " + label + " ---</span>", false);
	}
};

#endif // BOARDGAME_COMBOBOX_GAME_H
