/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "application.h"
#include "exception.h"
#include "game_collection.h"
#include "random.h"
#include <gtkmm/aboutdialog.h>
#include <gtkmm/settings.h>
#include <glibmm/i18n.h>
#include <glibmm/miscutils.h>
#include <iostream>

BoardgameApplication::BoardgameApplication()
: Gtk::Application("org.lozet.boardgame", Gio::APPLICATION_HANDLES_COMMAND_LINE)
{
	Glib::set_application_name(_("Boardgame"));
	Glib::set_prgname("boardgame");

	// set up the command line options
	// Glib ignore int and double options with a value of 0.
	// So all int and double options are set as OPTION_TYPE_STRING...
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_BOOL,
		"list-games", 'l',
		_("List all available games.")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_STRING,
		"game", 'g',
		_("The id of the game to play.")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_STRING,
		"players", 'p',
		_("Number of human players from 0 to 2.")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_STRING,
		"difficulty", 'd',
		_("The AI difficulty level from 0 (very easy) to 4 (very hard).")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_STRING,
		"iter", 'i',
		_("The number of iterations. Override the difficulty option.")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_STRING,
		"uctk", 'u',
		_("UCTK value, used to balance between exploitation and exploration.")
	);
	add_main_option_entry(
		Gio::Application::OPTION_TYPE_BOOL,
		"version", 'v',
		_("Show the application version")
	);

	GameCollection::init();

	// for -l and -v
	signal_handle_local_options().connect(
		sigc::mem_fun(*this, &BoardgameApplication::on_handle_local_options)
	);
}

Glib::RefPtr<BoardgameApplication> BoardgameApplication::create()
{
	return Glib::RefPtr<BoardgameApplication>(new BoardgameApplication());
}

int BoardgameApplication::on_command_line(const Glib::RefPtr< Gio::ApplicationCommandLine > &command_line)
{
	int status = EXIT_SUCCESS;

	try
	{
		// get the game options either from the command line options either
		// from gsettings.
		const Glib::RefPtr<Glib::VariantDict>& options = command_line->get_options_dict();
		GameOptions game_options;
		Glib::ustring value;

		if (options->contains("game")) {
			options->lookup_value("game", value);
			game_options.game = parse_game(value);
		} else {
			game_options.game = settings_->get_last_game();
		}

		if (options->contains("players")) {
			options->lookup_value("players", value);
			game_options.nb_players = parse_nb_players(value);
		} else {
			game_options.nb_players = settings_->get_nb_players();
		}

		if (options->contains("difficulty")) {
			options->lookup_value("difficulty", value);
			game_options.difficulty = parse_difficulty(value);
		} else {
			game_options.difficulty = settings_->get_difficulty();
		}

		if (options->contains("iter")) {
			options->lookup_value("iter", value);
			game_options.nb_iterations = parse_nb_iterations(value);
		} else {
			game_options.nb_iterations = -1;
		}

		if (options->contains("uctk")) {
			options->lookup_value("uctk", value);
			game_options.uctk = parse_uctk(value);
		} else {
			game_options.uctk = -1.0;
		}

		// create a new window
		create_window(game_options);
	}
	catch (const BoardgameErr& ex)
	{
		command_line->printerr(Glib::ustring::compose(_("Command Line Error: %1\n"), ex.get_message()));
		status = EXIT_FAILURE;
	}

	return status;
}

void BoardgameApplication::on_startup()
{
	//Call the base class's implementation:
	Gtk::Application::on_startup();

	//Create actions for menus and toolbars.

	add_action("newboard", sigc::mem_fun(*this, &BoardgameApplication::on_menu_new));

	add_action("help", sigc::mem_fun(*this, &BoardgameApplication::on_menu_help));
	add_action("gamehelp", sigc::mem_fun(*this, &BoardgameApplication::on_menu_game_help));
	add_action("about", sigc::mem_fun(*this, &BoardgameApplication::on_menu_about));

	add_action("quit", sigc::mem_fun(*this, &BoardgameApplication::on_menu_quit));

	refBuilder_ = Gtk::Builder::create();

	//Layout the actions in a menubar and an application menu:
	try
	{
		refBuilder_->add_from_resource("/org/lozet/boardgame/menu.ui");
	}
	catch (const Glib::Error& ex)
	{
		std::cerr << "Building menus failed: " << ex.what();
	}


	// Get the menubar and add it to the application:
	auto object = refBuilder_->get_object("menu-boardgame");
	auto gmenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
	if (!gmenu) {
		g_warning("GMenu not found");
	}
	else {
		set_menubar(gmenu);
	}

	// if gtk displays app menu, add it (the fallback is useless)
	Glib::RefPtr<Gtk::Settings> gtk_settings = Gtk::Settings::get_default();
	Glib::PropertyProxy<bool> property = gtk_settings->property_gtk_shell_shows_app_menu();
	const bool gtk_show_app_menu = property.get_value();
	if (gtk_show_app_menu) {
		object = refBuilder_->get_object("appmenu");
		auto appMenu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
		if (!appMenu) {
			g_warning("AppMenu not found");
		}
		else {
			set_app_menu(appMenu);
		}
	}


	// create the object handling the application settings
	settings_ = std::unique_ptr<Settings>(new Settings);

	init_random();

	std::vector< Glib::RefPtr<Gdk::Pixbuf> > icons;
	icons.push_back(Gdk::Pixbuf::create_from_resource("/org/lozet/boardgame/icon16.png"));
	icons.push_back(Gdk::Pixbuf::create_from_resource("/org/lozet/boardgame/icon32.png"));
	icons.push_back(Gdk::Pixbuf::create_from_resource("/org/lozet/boardgame/icon48.png"));
	Gtk::Window::set_default_icon_list(icons);
}

int BoardgameApplication::on_handle_local_options(const Glib::RefPtr<Glib::VariantDict>& options)
{
	if (options->contains("version"))
	{
		std::cout << Glib::ustring::compose(_("%1 version %2"), PACKAGE_NAME, PACKAGE_VERSION) << std::endl;
		return EXIT_SUCCESS;
	}
	else if (options->contains("list-games"))
	{
		const std::vector<GameCategory>& categories = GameCollection::get_categories();
		std::cout << _("List of games:") << std::endl;
		for (auto ic = categories.cbegin(); ic != categories.cend(); ++ic) {
			const GameCategory& cat = *ic;
			//std::cout << "  " << cat.name << std::endl;
			for (auto ig = cat.games.cbegin(); ig != cat.games.cend(); ++ig) {
				std::cout  << "  " << (*ig)->id << " : " << (*ig)->name << std::endl;
			}
		}
		return EXIT_SUCCESS;
	}

	return -1;
}

void BoardgameApplication::on_menu_new()
{
	GameOptions game_options;
	game_options.game = settings_->get_last_game();
	game_options.nb_players = settings_->get_nb_players();
	game_options.difficulty = settings_->get_difficulty();
	game_options.nb_iterations = -1;
	game_options.uctk = -1.0;
	create_window(game_options);
}

void BoardgameApplication::create_window(const GameOptions& game_options)
{
	WindowOptions win_options = settings_->get_ui_options();

	MainWindow* win = new MainWindow(game_options, win_options);

	//Make sure that the application runs for as long this window is still open:
	add_window(*win);

	//Delete the window when it is hidden.
	//That's enough for this simple example.
	win->signal_hide().connect(sigc::bind<MainWindow*>(
		sigc::mem_fun(*this, &BoardgameApplication::on_window_hide), win));

	win->show();
}

void BoardgameApplication::save_config(MainWindow* window)
{
	WindowOptions ui_options = window->get_ui_options();
	const GameOptions& game_options = window->get_game_options();

	settings_->set_ui_options(ui_options);
	settings_->set_game_options(game_options);
}

void BoardgameApplication::on_window_hide(MainWindow* window)
{
	save_config(window);
	delete window;
}

void BoardgameApplication::on_menu_quit()
{
	quit(); // Not really necessary, when Gtk::Widget::hide() is called.

	MainWindow* active_window = static_cast<MainWindow*>(get_active_window());
	save_config(active_window);

	// Remove and delete all windows from the application
	std::vector<Gtk::Window*> windows = get_windows();
	for (int i = 0; i < windows.size(); ++i) {
		remove_window(*(windows[i]));
		delete windows[i];
	}
}

void BoardgameApplication::on_menu_help()
{
	display_help("index");
}

void BoardgameApplication::on_menu_game_help()
{
	MainWindow* active_window = static_cast<MainWindow*>(get_active_window());
	if (!active_window) {
		return;
	}

	const GameOptions& options = active_window->get_game_options();
	Glib::ustring page = GameCollection::get_info(options.game).str_id;

	display_help(page);
}

void BoardgameApplication::display_help(const Glib::ustring page)
{
	GtkWindow* window = get_active_window()->gobj();
	GError *error = NULL;

	Glib::ustring path = "help:boardgame/" + page;

#ifdef _WIN32
	gchar prog[5];
	gchar arg[path.size()];
	gchar *argv[] = { prog, arg, NULL };
	strncpy(prog, "yelp", 5);
	strncpy(arg, path.c_str(), path.size());

	g_spawn_async_with_pipes (NULL, argv, NULL,
	                          G_SPAWN_SEARCH_PATH, NULL, NULL,
	                          NULL, NULL, NULL, NULL, &error);
#else
	gtk_show_uri (gtk_widget_get_screen (GTK_WIDGET (window)),
				  path.c_str(), GDK_CURRENT_TIME, &error);
#endif

	if (error != NULL)
	{
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new (window,
						GTK_DIALOG_DESTROY_WITH_PARENT,
						GTK_MESSAGE_ERROR,
						GTK_BUTTONS_CLOSE,
						_("There was an error displaying the help."));

		gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
				"%s", error->message);

		g_signal_connect (G_OBJECT (dialog),
				"response",
				G_CALLBACK (gtk_widget_destroy),
				NULL);

		gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

		gtk_widget_show (dialog);

		g_error_free (error);
	}
}

void BoardgameApplication::on_menu_about()
{
	Gtk::AboutDialog about;

	try {
		Glib::RefPtr<Gdk::Pixbuf> logo = Gdk::Pixbuf::create_from_resource("/org/lozet/boardgame/logo.png");
		about.set_logo(logo);
	} catch (const Glib::Error& ex) {
		std::cerr << "Error: Unable to load the Boardgame logo." << std::endl << ex.what() << std::endl;
	}

	about.set_program_name(_("Boardgame"));
	about.set_version( VERSION );
	about.set_comments( _("A program to play several board games.") );
	about.set_website(PACKAGE_URL);
	about.set_copyright( _("Copyright \xc2\xa9 2016,2017 Gregory Lozet") );
	about.set_license_type(Gtk::LICENSE_GPL_3_0);

	std::vector<Glib::ustring> authors ;
	authors.push_back("Gregory Lozet <gregorylozet@gmail.com>");
	about.set_authors(authors);

	about.set_translator_credits( _("translator-credits") );

	about.set_transient_for(*get_active_window());
	about.run();
}

GameId BoardgameApplication::parse_game(Glib::ustring arg)
{
	int value;

	try
	{
		value = std::stoi(arg);
	}
	catch (const std::invalid_argument& ex)
	{
		throw MsgErr(_("The game id must be an integer."));
	}
	catch (const std::out_of_range& ex)
	{
		throw MsgErr(_("The game id specified is invalid."));
	}

	if (value < 0 || value >= NB_GAMES) {
		throw MsgErr(Glib::ustring::compose(_("There isn't a game with id %1"), value));
	}

	return static_cast<GameId>(value);
}

GameNumberPlayers BoardgameApplication::parse_nb_players(Glib::ustring arg)
{
	int value;

	try
	{
		value = std::stoi(arg);
	}
	catch (const std::invalid_argument& ex)
	{
		throw MsgErr(_("The number of players must be an integer."));
	}
	catch (const std::out_of_range& ex)
	{
		throw MsgErr(_("The number of players must be between 0 and 2."));
	}

	if (value < 0 || value > 2) {
		throw MsgErr(_("The number of players must be between 0 and 2."));
	}

	return static_cast<GameNumberPlayers>(value);
}

GameDifficulty BoardgameApplication::parse_difficulty(Glib::ustring arg)
{
	int value;

	try
	{
		value = std::stoi(arg);
	}
	catch (const std::invalid_argument& ex)
	{
		throw MsgErr(_("The difficulty must be an integer."));
	}
	catch (const std::out_of_range& ex)
	{
		throw MsgErr(_("The difficulty specified is invalid."));
	}

	if (value < DIFFICULTY_VERY_EASY || value > DIFFICULTY_VERY_HARD) {
		throw MsgErr(Glib::ustring::compose(
			_("Unknow difficulty %1\nThe AI difficulty level must be between %2 (very easy) and %3 (very hard)."),
			value, DIFFICULTY_VERY_EASY, DIFFICULTY_VERY_HARD
		));
	}

	return static_cast<GameDifficulty>(value);
}

int BoardgameApplication::parse_nb_iterations(Glib::ustring arg)
{
	int value;

	try
	{
		value = std::stoi(arg);
	}
	catch (const std::invalid_argument& ex)
	{
		throw MsgErr(_("The number of iterations must be an integer."));
	}
	catch (const std::out_of_range& ex)
	{
		throw MsgErr(_("The number of iterations specified is out of range."));
	}

	if (value <= 0) {
		throw MsgErr(_("The number of iterations must be greater than 0."));
	}

	return value;
}

double BoardgameApplication::parse_uctk(Glib::ustring arg)
{
	double value;

	try
	{
		value = std::stod(arg);
	}
	catch (const std::invalid_argument& ex)
	{
		throw MsgErr(_("The UCTK value must be a number."));
	}
	catch (const std::out_of_range& ex)
	{
		throw MsgErr(_("The UCTK value specified is out of range."));
	}

	if (value < 0.0) {
		throw MsgErr(_("The UCTK value can't be negative."));
	}

	return value;
}
