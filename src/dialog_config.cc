/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "dialog_config.h"
#include "game_info.h"
#include "game_collection.h"
#include <glibmm/i18n.h>
#include <gtkmm/stock.h>

DialogConfig::DialogConfig(GameOptions& options)
: Dialog(_("Config a new game")),
  vbox_(Gtk::ORIENTATION_VERTICAL),
  hb_game_(Gtk::ORIENTATION_HORIZONTAL),
  lbl_game_(_("Game :")),
  hb_nb_players_(Gtk::ORIENTATION_HORIZONTAL),
  lbl_nb_players_(_("Number of players :")),
  hb_difficulty_(Gtk::ORIENTATION_HORIZONTAL),
  lbl_difficulty_(_("Difficulty :"))
{
	set_border_width(10);
	fill_widgets();
	preselect_options(options);
	pack_widgets();
	show_all_children();
}

DialogConfig::~DialogConfig()
{
}

void DialogConfig::fill_widgets()
{
	const std::vector<GameCategory>& categories = GameCollection::get_categories();
	for (auto it = categories.cbegin(); it != categories.cend(); ++it) {
		cmb_game_.append_category(*it);
	}

	cmb_nb_players_.append(std::to_string(GAME_0_PLAYERS), _("0 players"));
	cmb_nb_players_.append(std::to_string(GAME_1_PLAYER), _("1 player"));
	cmb_nb_players_.append(std::to_string(GAME_2_PLAYERS), _("2 players"));

	cmb_difficulty_.append(std::to_string(DIFFICULTY_VERY_EASY), _("Very Easy"));
	cmb_difficulty_.append(std::to_string(DIFFICULTY_EASY), _("Easy"));
	cmb_difficulty_.append(std::to_string(DIFFICULTY_NORMAL), _("Normal"));
	cmb_difficulty_.append(std::to_string(DIFFICULTY_HARD), _("Hard"));
	cmb_difficulty_.append(std::to_string(DIFFICULTY_VERY_HARD), _("Very Hard"));
}

void DialogConfig::preselect_options(GameOptions& options)
{
	cmb_game_.set_active_id(std::to_string(options.game));
	cmb_nb_players_.set_active_id(std::to_string(options.nb_players));
	cmb_difficulty_.set_active_id(std::to_string(options.difficulty));
}

void DialogConfig::get_options(GameOptions& options)
{
	options.game = (GameId) std::stoi(cmb_game_.get_active_id());
	options.nb_players = (GameNumberPlayers) std::stoi(cmb_nb_players_.get_active_id());
	options.difficulty = (GameDifficulty) std::stoi(cmb_difficulty_.get_active_id());
}

void DialogConfig::pack_widgets()
{
	get_vbox()->pack_start(vbox_);
	vbox_.pack_start(hb_game_, Gtk::PACK_SHRINK, 8);
	vbox_.pack_start(hb_nb_players_, Gtk::PACK_SHRINK, 8);
	vbox_.pack_start(hb_difficulty_, Gtk::PACK_SHRINK, 8);

	// game
	hb_game_.pack_start(lbl_game_, Gtk::PACK_SHRINK, 10);
	hb_game_.pack_start(cmb_game_, Gtk::PACK_SHRINK, 10);

	// number of players
	hb_nb_players_.pack_start(lbl_nb_players_, Gtk::PACK_SHRINK, 10);
	hb_nb_players_.pack_start(cmb_nb_players_, Gtk::PACK_SHRINK, 10);

	// difficulty
	hb_difficulty_.pack_start(lbl_difficulty_, Gtk::PACK_EXPAND_PADDING, 10);
	hb_difficulty_.pack_start(cmb_difficulty_, Gtk::PACK_EXPAND_PADDING, 10);

	// add the buttons
	add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CLOSE);
	add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
}
