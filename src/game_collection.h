#ifndef BOARDGAME_GAME_COLLECTION_H
#define BOARDGAME_GAME_COLLECTION_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "game_info.h"
#include "games/game.h"
#include "options.h"

namespace GameCollection
{
	void init();

	Game* create_game(const GameOptions& options);

	const std::vector<GameCategory>& get_categories();

	const GameInfo& get_info(const GameId id);
}

#endif // BOARDGAME_GAME_COLLECTION_H
