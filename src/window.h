#ifndef BOARDGAME_WINDOW_MAIN_H
#define BOARDGAME_WINDOW_MAIN_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"
#include "history_view.h"
#include "uct_worker.h"
#include "options.h"
#include <gtkmm/applicationwindow.h>
#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include <gtkmm/drawingarea.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/statusbar.h>
#include <glibmm/dispatcher.h>
#include <thread>

class MainWindow : public Gtk::ApplicationWindow
{
private:
	GameOptions options_;

	// menu and toolbar objects
	Glib::RefPtr<Gtk::Builder> refBuilder_;
	Glib::RefPtr<Gio::SimpleAction> refShowToolbar_;
	Glib::RefPtr<Gio::SimpleAction> refShowStatusbar_;
	Glib::RefPtr<Gio::SimpleAction> refShowHistory_;

	Gtk::Box vbox_;

	Gtk::Box hbox_;
	Gtk::DrawingArea board_area_;
	Gtk::ScrolledWindow history_scroll_;
	HistoryView history_view_;

	// the status bar
	Gtk::Statusbar statusbar_;
	unsigned context_id_;

	Game* game_;

	Glib::Dispatcher dispatcher_;
	UctWorker worker_;
	std::thread* worker_thread_;

	sigc::connection timeout_conn_;


	/** Initialize the window.
	 */
	void initialize();

	/** Create the window menu and toolbar.
	 */
	void create_menu(const WindowOptions& win_options);

	/** Pack the widgets.
	 */
	void pack_widgets();

	/** Connect the signals.
	 */
	void connect_signals();

	/** Initialise the window appearance.
	 */
	void init_display(const WindowOptions& win_options);


	/** Dispatcher handler.
	 */
	void on_notification_from_worker_thread();

	void start_mcts_thread();

	void stop_mcts_thread();

	void start_next_turn();

	void start_game();

	void stop_game();

	void on_turn_finished();

	bool on_timeout();

	void start_move_animation();

	void stop_move_animation();

	Glib::ustring get_filename_from_dialog();

protected:
	/** Start a new game.
	 */
	void on_action_game_new();

	/** Choose which board game to play.
	 */
	void on_action_game_select();

	/** Export the game history.
	 */
	void on_action_game_export();

	/** Toggle the toolbar visible and hidden.
	 */
	void on_action_view_show_toolbar();

	/** Toggle the statusbar visible and hidden.
	 */
	void on_action_view_show_statusbar();

	/** Toggle the history sidebar visible and hidden.
	 */
	void on_action_view_show_history();

	/** The board game need to be redrawn.
	 */
	bool on_board_draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);

	/** The board game received a user input.
	 */
	bool on_board_button_release(GdkEventButton* release_event);

public:
	/** Instantiate the main window.
	 */
	MainWindow(const GameOptions& game_options, const WindowOptions& win_options);

	virtual ~MainWindow();

	// Called from the worker thread.
	void notify();

	/** Get the game configuration.
	 */
	const GameOptions& get_game_options() const;

	/** Get the window configuration.
	 */
	WindowOptions get_ui_options() const;
};

#endif // BOARDGAME_MAIN_WINDOW_H
