/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "history_view.h"
#include <glibmm/i18n.h>

HistoryView::HistoryView()
{
	// create the tree model
	refTreeModel_ = Gtk::ListStore::create(m_Columns);
	set_model(refTreeModel_);

	// set up the columns
	append_column(_("Moves"), m_Columns.m_col_move);

	// make them reorderable and resizable
	std::vector<Gtk::TreeViewColumn*> columns = get_columns();
	for (auto column = columns.begin(); column != columns.end(); ++column)
	{
		(*column)->set_reorderable();
		(*column)->set_resizable();

		// set the background color of the cells renderers
		std::vector<Gtk::CellRenderer*> renderers = (*column)->get_cells();
		for (auto iter = renderers.begin(); iter != renderers.end(); ++iter) {
			(*column)->add_attribute((*iter)->property_cell_background_rgba(), m_Columns.m_col_backcolor);
		}
	}

	show_all_children();
}

HistoryView::~HistoryView()
{
}

void HistoryView::connect(Game*const game)
{
	disconnect();
	refTreeModel_->clear();

	conn_new_move_ = game->signal_new_move().connect(sigc::mem_fun(*this, &HistoryView::on_new_move) );
	conn_game_reset_ = game->signal_game_reset().connect(sigc::mem_fun(*this, &HistoryView::on_game_reset) );
}

void HistoryView::disconnect()
{
	if (conn_new_move_.connected()) {
		conn_new_move_.disconnect();
	}
	if (conn_game_reset_.connected()) {
		conn_game_reset_.disconnect();
	}
}

void HistoryView::on_new_move(const int player, const std::string& move)
{
	Gtk::TreeModel::Row row = *(refTreeModel_->append());
	row[m_Columns.m_col_move] = move;
	if (player == 1) {
		row[m_Columns.m_col_backcolor] = Gdk::RGBA("#FFFFFF");
	} else {
		row[m_Columns.m_col_backcolor] = Gdk::RGBA("#CCCCCC");
	}
}

void HistoryView::on_game_reset()
{
	refTreeModel_->clear();
}
