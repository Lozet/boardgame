#ifndef BOARDGAME_NODE_H
#define BOARDGAME_NODE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"
#include <list>
#include <vector>

/** A node in the game tree. Note wins is always from the viewpoint of playerJustMoved. */
class Node
{
public:
	Node(const std::string& move, Node*const parent, const GameState*const state);

	~Node();

	Node* select_child(const double uctk);

	Node* select_child(const std::string& move);

	Node* add_child(const std::string& move, const GameState*const state);

	void update(const double result);

	double get_wins() const;

	int get_visits() const;

	Node* get_parent();

	int get_player() const;

	std::string get_move() const;

	std::string get_most_visited_move() const;

	std::string get_random_move() const;

	inline bool is_selectable() const
	{
		return (untried_moves_.empty() && !childs_.empty());
	}

	inline bool is_expandable() const
	{
		return (!untried_moves_.empty());
	}

	void display_tree(const int verbose_level) const;

private:
	/** the move that got us to this node - "None" for the root node */
	const std::string move_;

	/** NULL for the root node */
	Node* parent_;

	std::vector<Node*> childs_;

	double wins_;

	int visits_;

	/** future child nodes */
	std::list<std::string> untried_moves_;

	/** the only part of the state that the Node needs later */
	int playerJustMoved_;
};

#endif //BOARDGAME_NODE_H
