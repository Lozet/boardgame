#ifndef BOARDGAME_ENGLISH_DRAUGHTS_STATE_H
#define BOARDGAME_ENGLISH_DRAUGHTS_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/draughts_state.h"
#include <vector>

/** English Draughts State
*/
class EnglishDraughtsState : public DraughtsState
{
public:
	enum Direction
	{
		DIR_LEFT_DOWN = 4,
		DIR_LEFT_UP = -4,
		DIR_RIGHT_DOWN = 5,
		DIR_RIGHT_UP = -3
	};

	EnglishDraughtsState();

	~EnglishDraughtsState();

	// Game functions

	GameState* clone() const;

	void do_move(const std::string& move);

	void print() const;

	// Draughts functions

	const DraughtsState::Square* get_board() const;

	int get_board_size() const;

	int get_jump_target(const int source, const int destination) const;

	void get_player1_color(Color& color) const;

	void get_player2_color(Color& color) const;

	void get_player1_highlight_color(Color& color) const;

	void get_player2_highlight_color(Color& color) const;

	void get_dark_square_color(Color& color) const;

	void get_light_square_color(Color& color) const;

	// English Draughts functions

	static const int NB_SQUARES_BY_SIDE = 8;
	static const int NB_SQUARES = NB_SQUARES_BY_SIDE * NB_SQUARES_BY_SIDE / 2;

	static const int FIRST_ROW = 0;
	static const int LAST_ROW = NB_SQUARES_BY_SIDE - 1;

	// id of the first column and number of used squares in a row
	static const int FIRST_COLUMN = NB_SQUARES_BY_SIDE / 2;
	static const int LAST_COLUMN = FIRST_COLUMN - 1;

	static const int MAX_KING_MOVES = 80;

private:
	struct BoardLayout
	{
		char board[NB_SQUARES];
		int player[2];
	};

	Square board_[NB_SQUARES];

	// if a player is playing this board layout for the 3rd times, the game is a draw
	std::vector<BoardLayout> know_boards_;


	inline void set_square(const int i, const SquareType type, const int player) {
		board_[i].type = type;
		board_[i].player = player;
	}

	inline void free_square(const int i) {
		board_[i].type = SQUARE_EMPTY;
		board_[i].player = 0;
	}

	inline int get_row(const int square) const {
		return square / FIRST_COLUMN;
	}

	inline int get_column(const int square) const {
		return square % NB_SQUARES_BY_SIDE;
	}

	inline void move_position(int& square, const Direction direction) const {
		square += direction - (get_row(square) % 2);
	}

	void check_for_promotion(const int square);

	void find_moves();

	void get_piece_moves(const int square);

	void try_piece_move(const int square, const Direction direction);

	void get_piece_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const Square& square, const int position) const;

	void try_piece_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const Square& square, const int position, const Direction direction) const;

	void fill_layout(BoardLayout& layout);

	static Direction get_direction(const int src, const int dst);
};

#endif //BOARDGAME_ENGLISH_DRAUGHTS_STATE_H
