// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/reversi_state.h"
#include "random.h"
#include <cstring>
#include <cstdio>
#include <cassert>

ReversiState::ReversiState()
: previous_player_blocked_(false),
  winner_(0)
{
	memset(board_, 0, sizeof(board_));

	// The dark player moves first. (dark = 1)
	board_[27] = 2;
	board_[28] = 1;
	board_[35] = 1;
	board_[36] = 2;

	find_moves();
}

ReversiState::~ReversiState()
{
}

GameState* ReversiState::clone() const
{
	ReversiState*const state = new ReversiState(*this);
	return state;
}

const int* ReversiState::get_board() const
{
	return (const int*) board_;
}

bool ReversiState::try_move(const int move)
{
	if (move < 0 || move >= NB_SQUARES) {
		return false;
	}

	return is_playable(move);
}

void ReversiState::do_move(const std::string& move)
{
	do_move(std::stoi(move));
}

void ReversiState::do_move(const int move)
{
	assert(move >= 0 && move < NB_SQUARES);
	assert(board_[move] == 0);
	const int current_player = get_current_player();

	board_[move] = current_player;
	take_opponent_pieces(move);

	// free the moves from previous player
	moves_.clear();

	// change the current player
	playerJustMoved_ = current_player;

	// find the move for the current player
	find_moves();
}

void ReversiState::take_opponent_pieces(const int square)
{
	// left
	try_capture(square, -1, -1);
	try_capture(square, -1, 0);
	try_capture(square, -1, 1);

	// right
	try_capture(square, 1, -1);
	try_capture(square, 1, 0);
	try_capture(square, 1, 1);

	// bottom
	try_capture(square, 0, -1);
	// top
	try_capture(square, 0, 1);
}

void ReversiState::get_pieces_taken(const std::string& move, std::vector<int>& captured_pieces) const
{
	const int square = std::stoi(move);
	get_capture(captured_pieces, square, -1, -1);
	get_capture(captured_pieces, square, -1, 0);
	get_capture(captured_pieces, square, -1, 1);
	get_capture(captured_pieces, square, 1, -1);
	get_capture(captured_pieces, square, 1, 0);
	get_capture(captured_pieces, square, 1, 1);
	get_capture(captured_pieces, square, 0, -1);
	get_capture(captured_pieces, square, 0, 1);
}

void ReversiState::get_capture(std::vector<int>& captured_pieces, const int square, const int dx, const int dy) const
{
	int x = square % SIDE_SIZE + dx;
	int y = square / SIDE_SIZE + dy;

	if (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_)
	{
		do {
			x += dx;
			y += dy;
		} while (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_);

		if (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == 3 - playerJustMoved_)
		{
			x -= dx;
			y -= dy;
			while (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_) {
				captured_pieces.push_back(x + y * SIDE_SIZE);
				x -= dx;
				y -= dy;
			}
		}
	}
}

void ReversiState::try_capture(const int square, const int dx, const int dy)
{
	int x = square % SIDE_SIZE + dx;
	int y = square / SIDE_SIZE + dy;

	if (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_)
	{
		do {
			x += dx;
			y += dy;
		} while (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_);

		if (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == 3 - playerJustMoved_)
		{
			x -= dx;
			y -= dy;
			while (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_) {
				board_[x + y * SIDE_SIZE] = 3 - playerJustMoved_;
				x -= dx;
				y -= dy;
			}
		}
	}
}

bool ReversiState::can_capture(const int square, const int dx, const int dy) const
{
	int x = square % SIDE_SIZE + dx;
	int y = square / SIDE_SIZE + dy;

	if (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_)
	{
		do {
			x += dx;
			y += dy;
		} while (is_on_board(x, y) && board_[x + y * SIDE_SIZE] == playerJustMoved_);

		return is_on_board(x, y) && board_[x + y * SIDE_SIZE] == 3 - playerJustMoved_;
	}
	else
	{
		return false;
	}
}

bool ReversiState::is_playable(const int square) const
{
	return (board_[square] == 0) && (
		can_capture(square, -1, 0) ||
		can_capture(square, -1, -1) ||
		can_capture(square, -1, 1) ||
		can_capture(square, 1, 0) ||
		can_capture(square, 1, -1) ||
		can_capture(square, 1, 1) ||
		can_capture(square, 0, -1) ||
		can_capture(square, 0, 1)
	);
}

void ReversiState::set_result()
{
	int total[3] = { 0, 0, 0 };
	for (int i = 0; i < NB_SQUARES; ++i) {
		++total[board_[i]];
	}

	if (total[1] > total[2]) {
		winner_ = 1; // score p1 = total[0] + total[1], score p2 = total[2]
	} else if (total[1] < total[2]) {
		winner_ = 2; // score p1 = total[1], score p2 = total[0] + total[2]
	}
}

void ReversiState::get_scores(int scores[2]) const
{
	int total[3] = { 0, 0, 0 };
	for (int i = 0; i < NB_SQUARES; ++i) {
		++total[board_[i]];
	}

	scores[0] = total[1];
	scores[1] = total[2];

	if (scores[0] > scores[1]) {
		scores[0] += total[0];
	} else if (scores[0] < scores[1]) {
		scores[1] += total[0];
	}
}

void ReversiState::find_moves()
{
	// search for moves
	for (int i = 0; i < NB_SQUARES; ++i) {
		if (is_playable(i)) {
			moves_.push_back(std::to_string(i));
		}
	}

	// if there is no moves available
	if (moves_.empty())
	{
		if (previous_player_blocked_) {
			// both players can't play => the game ends
			set_result();
		} else {
			// record that the current player can't play
			previous_player_blocked_ = true;
			// pass to the next player
			playerJustMoved_ = 3 - playerJustMoved_;
			find_moves();
		}
	}
	else if (previous_player_blocked_)
	{
		previous_player_blocked_ = false;
	}
}

void ReversiState::print() const
{
	printf("result = %d\n", winner_);
	for (int j = 0; j < SIDE_SIZE; ++j) {
		for (int i = 0; i < SIDE_SIZE; ++i) {
			printf("%d ", board_[j * SIDE_SIZE + i]);
		}
		putchar('\n');
	}
	putchar('\n');
}

bool ReversiState::has_move_left() const
{
	return (moves_.size() > 0 && winner_ == 0);
}

bool ReversiState::has_one_move_left() const
{
	return (moves_.size() == 1 && winner_ == 0);
}

void ReversiState::get_moves(std::list<std::string>& moves) const
{
	for (auto it = moves_.cbegin(); it != moves_.cend(); ++it) {
		moves.push_back(*it);
	}
}

std::string ReversiState::get_random_move() const
{
	const int r = generate_number(moves_.size());
	std::list<std::string>::const_iterator it = moves_.cbegin();
	std::advance(it, r);
	return (*it);
}

std::string ReversiState::get_only_move_left() const
{
	assert(moves_.size() == 1);
	return moves_.front();
}

double ReversiState::get_result(const int playerjm) const
{
	if (winner_ == 0) { // draw
		assert(moves_.size() == 0);
		assert(previous_player_blocked_);
		return 0.5;
	} else if (winner_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}
