// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/nim_state.h"
#include "random.h"
#include <cassert>

NimState::NimState()
: nb_tokens_(0)
{
	for (int i = 0; i < NB_HEAPS; ++i) {
		board_[i] = generate_number(1, MAX_TOKENS_PER_HEAP);
		nb_tokens_ += board_[i];
	}
}

NimState::~NimState()
{
}

GameState* NimState::clone() const
{
	NimState*const state = new NimState(*this);
	return state;
}

const int* NimState::get_board() const
{
	return (const int*) board_;
}

void NimState::do_move(const std::string& move)
{
	assert(nb_tokens_ > 0);

	// "0:4" => "take 4 tokens from heap 0"
	std::string str = move.c_str();
	std::string::size_type sz;

	const int heap = std::stoi(str, &sz);
	assert(heap >= 0 && heap < NB_HEAPS);
	str.erase(0, sz + 1);
	const int number = std::stoi(str);
	assert(number > 0 && number <= 3);

	nb_tokens_ -= number;
	board_[heap] -= number;

	playerJustMoved_ = 3 - playerJustMoved_;

	assert(nb_tokens_ >= 0);
}

void NimState::get_moves(std::list<std::string>& moves) const
{
	for (int h = 0; h < NB_HEAPS; ++h) {
		const int max = std::min(board_[h], 3);
		for (int n = 1; n <= max; ++n) {
			moves.push_back(std::to_string(h) + ":" + std::to_string(n));
		}
	}
}

std::string NimState::get_random_move() const
{
	assert(nb_tokens_ > 0);

	int h = generate_number(NB_HEAPS);
	while (board_[h] == 0) {
		if (++h >= NB_HEAPS) {
			h = 0;
		}
	}

	const int number = generate_number(1, std::min(board_[h], 3));

	return std::to_string(h) + ":" + std::to_string(number);
}

bool NimState::has_move_left() const
{
	return (nb_tokens_ > 0);
}

bool NimState::has_one_move_left() const
{
	return (nb_tokens_ == 1);
}

std::string NimState::get_only_move_left() const
{
	for (int i = 0; i < NB_HEAPS; ++i) {
		if (board_[i] != 0) {
			return std::to_string(i) + ":1";
		}
	}

	assert("Only move not found" == 0);
	return "";
}

double NimState::get_result(const int playerjm) const
{
	assert(nb_tokens_ == 0);

	if (playerJustMoved_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}
