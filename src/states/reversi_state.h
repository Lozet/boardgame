#ifndef BOARDGAME_REVERSI_STATE_H
#define BOARDGAME_REVERSI_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"
#include <vector>

class ReversiState : public GameState
{
public:
	ReversiState();

	~ReversiState();

	GameState* clone() const;

	bool try_move(const int move);

	void do_move(const std::string& move);

	void do_move(const int move);

	std::string get_random_move() const;

	void get_moves(std::list<std::string>& moves) const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	std::string get_only_move_left() const;

	double get_result(const int playerjm) const;

	const int* get_board() const;

	void print() const;

	void get_scores(int[2]) const;

	void get_pieces_taken(const std::string& move, std::vector<int>& moving_pieces) const;

	static const int SIDE_SIZE = 8;
	static const int NB_SQUARES = SIDE_SIZE * SIDE_SIZE;

	inline int get_column(const int square) const {
		return (square % SIDE_SIZE);
	}

	inline int get_row(const int square) const {
		return (square / SIDE_SIZE);
	}

	inline bool is_on_board(const int square) const {
		return square >= 0 && square < NB_SQUARES;
	}

	inline bool is_on_board(const int x, const int y) const {
		return x >= 0 && x < SIDE_SIZE && y >= 0 && y < SIDE_SIZE;
	}

private:
	// 0 = "empty", 1 = "player 1", 2 = "player 2"
	int board_[NB_SQUARES];

	bool previous_player_blocked_;

	int winner_;

	std::list<std::string> moves_;


	void get_capture(std::vector<int>& captured_pieces, const int square, const int dx, const int dy) const;

	void take_opponent_pieces(const int square);

	void try_capture(const int square, const int dx, const int dy);

	/** find all moves of the current player. */
	void find_moves();

	/** check if the current player can play this square. */
	bool is_playable(const int square) const;

	bool can_capture(const int square, const int dx, const int dy) const;

	void set_result();

	int get_next_piece(const int square, const int dx, const int dy) const;
};

#endif //BOARDGAME_REVERSI_STATE_H
