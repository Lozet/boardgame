#ifndef BOARDGAME_EINSTEINWURFELTNICHT_STATE_H
#define BOARDGAME_EINSTEINWURFELTNICHT_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"

/** EinStein Würfelt Nicht! State
*/
class EinSteinWurfeltNichtState : public GameState
{
public:

	struct Square
	{
		int player;
		int number;
	};

	EinSteinWurfeltNichtState();

	~EinSteinWurfeltNichtState();

	GameState* clone() const;

	int get_current_player() const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	bool is_random_move() const;

	void get_moves(std::list<std::string>& moves) const;

	std::string get_random_move() const;

	std::string get_only_move_left() const;

	bool try_move(const int src_x, const int src_y, const int dst_x, const int dst_y);

	void do_move(const std::string& move);

	double get_result(const int playerjm) const;


	const EinSteinWurfeltNichtState::Square* get_board() const;

	int get_dice() const;

	bool is_playable(const int number) const;

	void roll_dice();

	void print() const;

	static const int NB_ROWS = 5;
	static const int NB_COLUMNS = 5;

	static inline std::string get_coord(const int x, const int y) {
		return (char) (4 - x + 'a') + std::to_string(y + 1);
	}

private:
	// 0 = empty, 1 = player 1, 2 = player 2
	Square board_[NB_COLUMNS][NB_ROWS];

	bool dice_rolled_;

	int dice_;

	int winner_;


	void do_move(const int src_x, const int src_y, const int dst_x, const int dst_y);

	void get_moves_from_dice(std::list<std::string>& moves) const;

	void get_piece_moves(std::list<std::string>& moves, const int x, const int y) const;

	std::string get_random_move_from_dice() const;

	std::string get_piece_random_move(const int x, const int y) const;

	void get_dice_outcomes(std::list<std::string>& moves) const;

	std::string get_random_dice() const;

	bool is_reachable(const int src, const int dst) const;
};

#endif //BOARDGAME_EINSTEINWURFELTNICHT_STATE_H
