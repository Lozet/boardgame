// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/english_draughts_state.h"
#include <cstring>
#include <cstdio>
#include <cassert>

EnglishDraughtsState::EnglishDraughtsState()
{
	playerJustMoved_ = 1;

	memset(board_, 0, sizeof(board_));
	for (int i = 0; i < 12; ++i) {
		set_square(i, SQUARE_PION, 2);
		set_square(NB_SQUARES - 1 - i, SQUARE_PION, 1);
	}

	find_moves();
}

EnglishDraughtsState::~EnglishDraughtsState()
{
}

GameState* EnglishDraughtsState::clone() const
{
	EnglishDraughtsState*const state = new EnglishDraughtsState(*this);
	return state;
}

void EnglishDraughtsState::print() const
{
	printf("Board :\n");
	for (int y = 0; y < 8; ++y) {
		for (int x = 0; x < 8; ++x) {
			// draw the square
			if ((x+y) % 2 == 1) {
				const Square& s = board_[y * 4 + (x/2)];

				if (s.type == SQUARE_EMPTY) {
					putchar('#');
				} else {
					if (s.player == 1) {
						if (s.type == SQUARE_PION) {
							putchar('o');
						} else { // SQUARE_DAME
							putchar('O');
						}
					} else if (s.player == 2) {
						if (s.type == SQUARE_PION) {
							putchar('x');
						} else { // SQUARE_DAME
							putchar('X');
						}
					}
				}
			} else {
				putchar(' ');
			}
		}
		putchar('\n');
	}
}

const DraughtsState::Square* EnglishDraughtsState::get_board() const
{
	return (const DraughtsState::Square*) board_;
}

void EnglishDraughtsState::fill_layout(BoardLayout& layout)
{
	for (int i = 0; i < NB_SQUARES; ++i) {
		layout.board[i] = (board_[i].type << 4) + board_[i].player;
	}
}

void EnglishDraughtsState::do_move(const std::string& move)
{
	playerJustMoved_ = 3 - playerJustMoved_; // playerJustMoved is now the current player

	// eg. "33-29", "21x32", "18x29x38", ...
	std::string str = move.c_str();
	std::string::size_type sz;

	int square = std::stoi(str, &sz) - 1;
	const char action = str[sz];
	str.erase(0, sz + 1);

	int destination = std::stoi(str, &sz) - 1;
	str.erase(0, sz);

	bool not_draw = true;
	const bool is_king_move = (action == '-' && board_[square].type == SQUARE_DAME);

	// if it's a king move
	if (is_king_move) {
		// 25 king moves in a row
		if (++king_moves_ >= MAX_KING_MOVES) {
			not_draw = false;
		} else if (know_boards_.empty()) {
			// store the layout before the move for the current player
			BoardLayout layout;
			fill_layout(layout);
			layout.player[playerJustMoved_ - 1] = 1;
			layout.player[2 - playerJustMoved_] = 0;

			know_boards_.push_back(layout);
		}
	} else {
		king_moves_ = 0;
		know_boards_.clear();
	}

	if (action == '-')
	{
		board_[destination].type = board_[square].type;
		board_[destination].player = board_[square].player;
		board_[square].type = SQUARE_EMPTY;
		board_[square].player = 0;

		check_for_promotion(destination);
	}
	else if (action == 'x')
	{
		// move the current player piece
		board_[destination].type = board_[square].type;
		board_[destination].player = board_[square].player;
		board_[square].type = SQUARE_EMPTY;
		board_[square].player = 0;

		// remove the opponent piece
		int target = get_jump_target(square, destination);
		board_[target].type = SQUARE_EMPTY;
		board_[target].player = 0;

		// check rafle
		while (!str.empty()) {
			assert(str.at(0) == 'x');
			str.erase(0, 1);

			square = destination;
			destination = std::stoi(str, &sz) - 1;
			str.erase(0, sz);

			board_[destination].type = board_[square].type;
			board_[destination].player = board_[square].player;
			board_[square].type = SQUARE_EMPTY;
			board_[square].player = 0;

			target = get_jump_target(square, destination);
			board_[target].type = SQUARE_EMPTY;
			board_[target].player = 0;
		}

		check_for_promotion(destination);
	}

	// free the moves from previous player
	moves_.clear();

	if (is_king_move) {
		// store the new layout
		BoardLayout layout;
		fill_layout(layout);

		// search for the same layout
		int i;
		for (i = 0; i < know_boards_.size(); ++i) {
			if (!memcmp(layout.board, know_boards_[i].board, sizeof(layout.board))) {
				break;
			}
		}

		// if this is a new layout
		if (i == know_boards_.size()) {
			layout.player[playerJustMoved_ - 1] = 0;
			layout.player[2 - playerJustMoved_] = 1;
			know_boards_.push_back(layout);
		} else {
			if (++know_boards_[i].player[2 - playerJustMoved_] >= 3) {
				not_draw = false;
				know_boards_.clear();
			}
		}
	}

	// if the game isn't in a draw state
	if (not_draw) {
		// find the move for the current player
		find_moves();
	}
}

void EnglishDraughtsState::check_for_promotion(const int square)
{
	if (board_[square].type == SQUARE_PION) {
		const int player = board_[square].player;
		const int row = get_row(square);
		if ((player == 1 && row == FIRST_ROW) || (player == 2 && row == LAST_ROW)) {
			board_[square].type = SQUARE_DAME;
		}
	}
}

EnglishDraughtsState::Direction EnglishDraughtsState::get_direction(const int src, const int dst)
{
	const int mod = (dst % 4) - (src % 4);

	if (dst - src > 0) // backward move from white POV
	{
		if (mod > 0) {
			return DIR_RIGHT_DOWN;
		} else if (mod < 0) {
			return DIR_LEFT_DOWN;
		} else if ((src / 4) % 2 == 0) {
			return DIR_LEFT_DOWN;
		} else {
			return DIR_RIGHT_DOWN;
		}
	}
	else // forward move from white POV
	{
		if (mod > 0) {
			return DIR_RIGHT_UP;
		} else if (mod < 0) {
			return DIR_LEFT_UP;
		} else if ((src / 4) % 2 == 0) {
			return DIR_LEFT_UP;
		} else {
			return DIR_RIGHT_UP;
		}
	}

	fprintf(stderr, "Error: Direction from %d to %d does not exist.\n", src, dst);
	assert("Direction not found" == 0);
	return DIR_LEFT_DOWN;
}

void EnglishDraughtsState::try_piece_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const Square& square, const int position, const Direction direction) const
{
	const int target = position + direction - ((position / 4) % 2);
	if (board_[target].type != SQUARE_EMPTY && board_[target].player != square.player)
	{
		const int destination = target + direction - ((target / 4) % 2);
		if (board_[destination].type == SQUARE_EMPTY) // can jump
		{
			bool recursive = false;

			if (parent == captures.cend())
			{
				std::string move = std::to_string(position + 1) + "x" + std::to_string(destination + 1);
				captures.emplace_back(move, target);
				recursive = true;
			}
			else if ((*parent).can_take(target)) // can't jump over the same piece twice
			{
				std::string move = (*parent).move + "x" + std::to_string(destination + 1);
				captures.emplace_back(move, (*parent).pieces, target);
				recursive = true;
			}

			if (recursive && square.type == SQUARE_PION) {
				// stop capturing when a piece becomes king
				const int row = get_row(destination);
				if ((square.player == 1 && row == FIRST_ROW) || (square.player == 2 && row == LAST_ROW)) {
					recursive = false;
				}
			}

			if (recursive) {
				// recursively find jump available
				get_piece_captures(captures, --captures.cend(), square, destination);
			}
		}
	}
}

void EnglishDraughtsState::get_piece_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const Square& square, const int position) const
{
	const int column = position % NB_SQUARES_BY_SIDE;
	const int row = position / (NB_SQUARES_BY_SIDE / 2);

	// check for forward jumps from black POV
	if ((square.type == SQUARE_DAME || square.player == 2) && row != LAST_ROW && row != LAST_ROW - 1)
	{
		// check for left jump
		if (column != FIRST_COLUMN && column != 0) {
			try_piece_capture(captures, parent, square, position, DIR_LEFT_DOWN);
		}
		// check for right jump
		if (column != LAST_COLUMN && column != NB_SQUARES_BY_SIDE - 1) {
			try_piece_capture(captures, parent, square, position, DIR_RIGHT_DOWN);
		}
	}

	// check for backward jumps from black POV
	if ((square.type == SQUARE_DAME || square.player == 1) && row != FIRST_ROW && row != FIRST_ROW + 1)
	{
		// check for left jump
		if (column != FIRST_COLUMN && column != 0) {
			try_piece_capture(captures, parent, square, position, DIR_LEFT_UP);
		}
		// check for right jump
		if (column != LAST_COLUMN && column != NB_SQUARES_BY_SIDE - 1) {
			try_piece_capture(captures, parent, square, position, DIR_RIGHT_UP);
		}
	}
}

void EnglishDraughtsState::try_piece_move(const int square, const Direction direction)
{
	const int destination = square + direction - ((square / 4) % 2);
	if (board_[destination].type == SQUARE_EMPTY)
	{
		moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(destination + 1));
	}
}

void EnglishDraughtsState::get_piece_moves(const int square)
{
	const int column = square % 8;
	const int row = square / 4;
	const int player = board_[square].player;
	const SquareType type = board_[square].type;

	// check for forward moves from black POV
	if ((type == SQUARE_DAME || player == 2) && row != LAST_ROW)
	{
		// check for left move
		if (column != FIRST_COLUMN) {
			try_piece_move(square, DIR_LEFT_DOWN);
		}
		// check for right move
		if (column != LAST_COLUMN) {
			try_piece_move(square, DIR_RIGHT_DOWN);
		}
	}

	// check for forward moves from white POV
	if ((type == SQUARE_DAME || player == 1) && row != FIRST_ROW)
	{
		// check for left move
		if (column != FIRST_COLUMN) {
			try_piece_move(square, DIR_LEFT_UP);
		}
		// check for right move
		if (column != LAST_COLUMN) {
			try_piece_move(square, DIR_RIGHT_UP);
		}
	}
}

void EnglishDraughtsState::find_moves()
{
	const int current_player = 3 - playerJustMoved_;

	// check players pieces
	int pieces[2][2];
	memset(pieces, 0, sizeof(pieces));

	// look for mandatory moves
	std::list<Capture> captures;

	for (int i = 0; i < NB_SQUARES; ++i) {
		const Square& s = board_[i];

		if (s.player != 0 && s.type != 0) {
			++pieces[s.player - 1][s.type - 1];
		}

		// if the square contains a piece owned by the current player
		if (s.player == current_player) {
			get_piece_captures(captures, captures.cend(), s, i);
		}
	}

	if (captures.size() > 0)
	{
		// filter moves with distinct path
		bool removable;
		std::list<Capture>::const_iterator it = captures.cbegin();

		while (it != captures.cend()) {
			const std::string& m = (*it).move;
			removable = false;

			std::list<Capture>::const_iterator it2 = it;
			++it2;
			while (!removable && it2 != captures.cend()) {
				if ((*it2).move.size() > m.size() && !(*it2).move.compare(0, m.size(), m)) {
					removable = true;
				} else {
					++it2;
				}
			}

			if (removable) {
				captures.erase(it++);
			} else {
				++it;
			}
		}

		for (auto it = captures.cbegin(); it != captures.cend(); ++it) {
			moves_.push_back((*it).move);
		}
	}
	else // look for moves
	{
		for (int i = 0; i < NB_SQUARES; ++i) {
			if (board_[i].player == current_player) {
				get_piece_moves(i);
			}
		}

		// if there is no moves available
		if (moves_.empty()) {
			// the current player lose the game
			winner_ = playerJustMoved_;
		}
	}
}

int EnglishDraughtsState::get_jump_target(const int source, const int destination) const
{
	Direction dir = get_direction(source, destination);
	int target = source;
	move_position(target, dir);
	return target;
}

int EnglishDraughtsState::get_board_size() const
{
	return NB_SQUARES_BY_SIDE;
}

void EnglishDraughtsState::get_player1_color(Color& color) const
{
	color.r = 0.85;
	color.g = 0.85;
	color.b = 0.8;
}

void EnglishDraughtsState::get_player2_color(Color& color) const
{
	color.r = 0.8;
	color.g = 0.1;
	color.b = 0.1;
}

void EnglishDraughtsState::get_player1_highlight_color(Color& color) const
{
	color.r = 1.0;
	color.g = 1.0;
	color.b = 0.9;
}

void EnglishDraughtsState::get_player2_highlight_color(Color& color) const
{
	color.r = 0.95;
	color.g = 0.1;
	color.b = 0.1;
}

void EnglishDraughtsState::get_dark_square_color(Color& color) const
{
	color.r = 0.33;
	color.g = 0.53;
	color.b = 0.13;
}

void EnglishDraughtsState::get_light_square_color(Color& color) const
{
	color.r = 1.0;
	color.g = 0.93;
	color.b = 0.73;
}
