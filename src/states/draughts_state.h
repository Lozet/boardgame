#ifndef BOARDGAME_DRAUGHTS_STATE_H
#define BOARDGAME_DRAUGHTS_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"
#include "color.h"

/** Draughts State
*/
class DraughtsState : public GameState
{
public:
	enum SquareType
	{
		SQUARE_EMPTY,
		SQUARE_PION,
		SQUARE_DAME
	};

	struct Square
	{
		SquareType type;
		int player;
	};

	typedef Square* Board;


	DraughtsState();

	virtual ~DraughtsState();

	virtual const Square* get_board() const = 0;

	virtual int get_board_size() const = 0;

	virtual int get_jump_target(const int source, const int destination) const = 0;

	virtual void get_player1_color(Color& color) const = 0;

	virtual void get_player2_color(Color& color) const = 0;

	virtual void get_player1_highlight_color(Color& color) const = 0;

	virtual void get_player2_highlight_color(Color& color) const = 0;

	virtual void get_dark_square_color(Color& color) const = 0;

	virtual void get_light_square_color(Color& color) const = 0;

	int get_draw_counter() const;

	int get_king_moves() const;

	bool try_move(const std::string& move);

	void get_moves(std::list<std::string>& moves) const;

	std::string get_random_move() const;

	void get_moves_from(std::list<std::string>& moves, const int square) const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	std::string get_only_move_left() const;

	double get_result(const int playerjm) const;

	int get_winner() const;

protected:
	struct Capture
	{
		std::string move;
		std::list<int> pieces;

		Capture(const std::string m, const int t)
		: move(m)
		{
			pieces.push_back(t);
		}

		Capture(const std::string m, const std::list<int>& p, const int t)
		: move(m),
		  pieces(p)
		{
			pieces.push_back(t);
		}

		// A man can only be jumped once during a multiple jumping sequence.
		bool can_take(const int target) const
		{
			for (std::list<int>::const_reverse_iterator it = pieces.rbegin(); it != pieces.rend(); ++it) {
				if ((*it) == target) {
					return false;
				}
			}
			return true;
		}
	};

	int winner_;

	int draw_counter_;

	int king_moves_;

	std::list<std::string> moves_;

};

#endif //BOARDGAME_DRAUGHTS_STATE_H
