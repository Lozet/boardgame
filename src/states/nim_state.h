#ifndef BOARDGAME_NIM_STATE_H
#define BOARDGAME_NIM_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"

class NimState : public GameState
{
public:
	NimState();

	~NimState();

	GameState* clone() const;

	void do_move(const std::string& move);

	std::string get_random_move() const;

	void get_moves(std::list<std::string>& moves) const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	std::string get_only_move_left() const;

	double get_result(const int playerjm) const;

	const int* get_board() const;

	static const int NB_HEAPS = 3;
	static const int MAX_TOKENS_PER_HEAP = 7;

private:
	// number of token in the heaps
	int board_[NB_HEAPS];

	int nb_tokens_;
};

#endif //BOARDGAME_NIM_STATE_H
