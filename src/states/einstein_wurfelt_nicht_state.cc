// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/einstein_wurfelt_nicht_state.h"
#include "random.h"
#include <cstring>
#include <cstdio>
#include <cassert>

EinSteinWurfeltNichtState::EinSteinWurfeltNichtState()
: dice_rolled_(false),
  winner_(0)
{
	memset(board_, 0, sizeof(board_));

	int number = 1;
	for (int j = 0; j < 3; ++j) {
		for (int i = 0; i < 3 - j; ++i) {
			board_[j][i].player = 1;
			board_[j][i].number = number;
			board_[4 - j][4 - i].player = 2;
			board_[4 - j][4 - i].number = number;
			++number;
		}
	}
}

EinSteinWurfeltNichtState::~EinSteinWurfeltNichtState()
{
}

GameState* EinSteinWurfeltNichtState::clone() const
{
	EinSteinWurfeltNichtState*const state = new EinSteinWurfeltNichtState(*this);
	return state;
}

int EinSteinWurfeltNichtState::get_current_player() const
{
	if (!dice_rolled_) {
		return 3 - playerJustMoved_;
	} else {
		return playerJustMoved_;
	}
}

const EinSteinWurfeltNichtState::Square* EinSteinWurfeltNichtState::get_board() const
{
	return (const EinSteinWurfeltNichtState::Square*) board_;
}

void EinSteinWurfeltNichtState::roll_dice()
{
	assert(!dice_rolled_);
	dice_ = generate_number(1, 6);
	dice_rolled_ = true;

	playerJustMoved_ = 3 - playerJustMoved_;
}

bool EinSteinWurfeltNichtState::is_random_move() const
{
	return !dice_rolled_;
}

void EinSteinWurfeltNichtState::do_move(const std::string& move)
{
	if (dice_rolled_) {
		assert(move.size() == 5);
		const int src_x = 4 - (move.at(0) - 'a');
		assert(src_x >= 0 && src_x <= 5);
		const int src_y = (move.at(1) - '0') - 1;
		assert(src_y >= 0 && src_y <= 5);
		const int dst_x = 4 - (move.at(3) - 'a');
		assert(dst_x >= 0 && dst_x <= 5);
		const int dst_y = (move.at(4) - '0') - 1;
		assert(dst_y >= 0 && dst_y <= 5);
		do_move(src_x, src_y, dst_x, dst_y);
		dice_rolled_ = false;
	} else {
		dice_ = std::stoi(move);
		dice_rolled_ = true;
		playerJustMoved_ = 3 - playerJustMoved_;
	}
}

void EinSteinWurfeltNichtState::print() const
{
	printf("board:\n  ");
	for (int i = 0; i < 5; ++i) {
		printf("  %c   ", 'E' - i);
	}
	putchar('\n');

	for (int j = 0; j < 5; ++j) {
		printf("%d ", j + 1);
		for (int i = 0; i < 5; ++i) {
			printf("[%d.%d] ", board_[j][i].player, board_[j][i].number);
		}
		putchar('\n');
	}

	if (dice_rolled_) {
		printf("dice = %d\n", dice_);
	} else {
		printf("dice not rolled\n");
	}

	putchar('\n');
}

bool EinSteinWurfeltNichtState::try_move(const int src_x, const int src_y, const int dst_x, const int dst_y)
{
	// src coord are valid
	if (src_x < 0 || src_x > 5 || src_y < 0 || src_y > 5 ||
		dst_x < 0 || dst_x > 5 || dst_y < 0 || dst_y > 5) {
		return false;
	}

	// src coord belong to the current player, that is
	// the player that just rolled the dice.
	if (board_[src_y][src_x].player != get_current_player()) {
		return false;
	}

	//TODO src piece have a playable number

	// dst coord is reachable from src coord
	if (!is_reachable(src_y * 5 + src_x, dst_y * 5 + dst_x)) {
		return false;
	}

	// all good
	return true;
}

void EinSteinWurfeltNichtState::do_move(const int src_x, const int src_y, const int dst_x, const int dst_y)
{
	// update current player
	const int player = get_current_player();
	const int opponent = 3 - player;

	assert(board_[src_y][src_x].player == player);

	// move up the piece
	Square temp;
	temp.player = board_[src_y][src_x].player;
	temp.number = board_[src_y][src_x].number;
	board_[src_y][src_x].player = 0;
	board_[src_y][src_x].number = 0;

	const bool opponent_piece_taken = (board_[dst_y][dst_x].player == opponent);

	// put down the piece
	board_[dst_y][dst_x].player = temp.player;
	board_[dst_y][dst_x].number = temp.number;

	// check result

	if (player == 1) {
		if (dst_x == 4 && dst_y == 4) {
			winner_ = player;
		}
	} else {
		if (dst_x == 0 && dst_y == 0) {
			winner_ = player;
		}
	}

	if (opponent_piece_taken && winner_ == 0) {
		// check if the last enemy pieces was taken
		bool has_won = true;
		for (int j = 0; has_won && j < 5; ++j) {
			for (int i = 0; has_won && i < 5; ++i) {
				if (board_[j][i].player == opponent) {
					has_won = false;
				}
			}
		}
		if (has_won) {
			winner_ = player;
		}
	}
}

int EinSteinWurfeltNichtState::get_dice() const
{
	return dice_;
}

void EinSteinWurfeltNichtState::get_moves(std::list<std::string>& moves) const
{
	if (winner_ == 0) {
		if (dice_rolled_) {
			get_moves_from_dice(moves);
		} else {
			get_dice_outcomes(moves); // merge similar dices outputs
		}
	}
}

void EinSteinWurfeltNichtState::get_dice_outcomes(std::list<std::string>& moves) const
{
	// the player rolling the dice is currently the "previous player"
	const int player = 3 - playerJustMoved_;

	int pieces[6];
	int nb_pieces = 0;
	memset(pieces, 0, sizeof(pieces));

	for (int i = 0; i < NB_COLUMNS; ++i) {
		for (int j = 0; j < NB_ROWS; ++j) {
			if (board_[j][i].player == player) {
				pieces[board_[j][i].number - 1] = 1;
				++nb_pieces;
			}
		}
	}

	if (nb_pieces == 1) { // don't roll dice
		moves.push_back("3");
		return;
	}

	int lowest = 0, highest = 5;

	// lowest pieces
	while (pieces[lowest] == 0) {
		++lowest;
	}
	assert(lowest < 6); // no piece!
	moves.push_back(std::to_string(lowest + 1));

	// highest pieces
	while (pieces[highest] == 0) {
		--highest;
	}
	moves.push_back(std::to_string(highest + 1));

	for (int i = lowest + 1; i < highest; ++i) {
		if (pieces[i] != 0) {
			moves.push_back(std::to_string(i + 1));
		} else {
			while (pieces[i + 1] == 0) {
				++i;
			}
			moves.push_back(std::to_string(i + 1));
		}
	}
}

void EinSteinWurfeltNichtState::get_moves_from_dice(std::list<std::string>& moves) const
{
	bool searching = true;

	for (int i = 0; i < NB_COLUMNS && searching; ++i) {
		for (int j = 0; j < NB_ROWS && searching; ++j) {
			if (board_[j][i].player == playerJustMoved_ && board_[j][i].number == dice_) {
				get_piece_moves(moves, i, j);
				searching = false;
			}
		}
	}

	// if the exact piece wasn't found
	if (searching)
	{
		int next_highest = 7;
		int next_lowest = 0;
		int highest_x = 0, highest_y = 0, lowest_x = 0, lowest_y = 0;
		for (int i = 0; i < NB_COLUMNS; ++i) {
			for (int j = 0; j < NB_ROWS; ++j) {
				if (board_[j][i].player == playerJustMoved_) {
					const int number = board_[j][i].number;
					if (number > dice_) {
						if (number < next_highest) {
							next_highest = number;
							highest_x = i;
							highest_y = j;
						}
					} else {
						if (number > next_lowest) {
							next_lowest = number;
							lowest_x = i;
							lowest_y = j;
						}
					}
				}
			}
		}

		if (next_highest != 7) {
			assert(next_highest > dice_ && next_highest <= 6);
			get_piece_moves(moves, highest_x, highest_y);
		}

		if (next_lowest != 0) {
			assert(next_lowest >= 1 && next_lowest < dice_);
			get_piece_moves(moves, lowest_x, lowest_y);
		}
	}
}

void EinSteinWurfeltNichtState::get_piece_moves(std::list<std::string>& moves, const int x, const int y) const
{
	const std::string src = get_coord(x, y);
	//char sep;

	if (playerJustMoved_ == 1) { // player 1 moves
		if (x < 4) {
			/*if (board_[y][x + 1].player != 0) {
				sep = 'x';
			} else {
				sep = '-';
			}*/
			moves.push_back(src + '-' + get_coord(x + 1, y));
		}

		if (y < 4) {
			moves.push_back(src + "-" + get_coord(x, y + 1));
			if (x < 4) {
				moves.push_back(src + "-" + get_coord(x + 1, y + 1));
			}
		}
	} else { // player 2 moves
		if (y > 0) {
			moves.push_back(src + "-" + get_coord(x, y - 1));
			if (x > 0) {
				moves.push_back(src + "-" + get_coord(x - 1, y - 1));
			}
		}

		if (x > 0) {
			moves.push_back(src + "-" + get_coord(x - 1, y));
		}
	}
}

bool EinSteinWurfeltNichtState::is_playable(const int number) const
{
	if (!dice_rolled_) {
		return false;
	} else if (number == dice_) {
		return true;
	} else {
		// check the exact piece is no more available
		for (int i = 0; i < NB_COLUMNS; ++i) {
			for (int j = 0; j < NB_ROWS; ++j) {
				if (board_[j][i].player == playerJustMoved_ && board_[j][i].number == dice_) {
					return false;
				}
			}
		}

		// search for the movable pieces
		int next_highest = 7;
		int next_lowest = 0;
		for (int i = 0; i < NB_COLUMNS; ++i) {
			for (int j = 0; j < NB_ROWS; ++j) {
				if (board_[j][i].player == playerJustMoved_) {
					const int number = board_[j][i].number;
					if (number > dice_) {
						if (number < next_highest) {
							next_highest = number;
						}
					} else {
						if (number > next_lowest) {
							next_lowest = number;
						}
					}
				}
			}
		}

		if (next_highest != 7 && number == next_highest) {
			return true;
		}
		if (next_lowest != 0 && number == next_lowest) {
			return true;
		}

		return false;
	}
}

bool EinSteinWurfeltNichtState::is_reachable(const int src, const int dst) const
{
	if (playerJustMoved_ == 1) { // player 1 moves
		if (dst == src + 1) {
			return (src % NB_ROWS) < 4;
		}
		else if (dst == src + NB_ROWS) {
			return (src / NB_ROWS) < 4;
		}
		else if (dst == src + NB_ROWS + 1) {
			return ((src / NB_ROWS) < 4) && ((src % NB_ROWS) < 4);
		}
	} else { // player 2 moves
		if (dst == src - 1) {
			return (src % NB_ROWS) > 0;
		}
		else if (dst == src - NB_ROWS) {
			return (src / NB_ROWS) > 0;
		}
		else if (dst == src - NB_ROWS - 1) {
			return ((src / NB_ROWS) > 0) && ((src % NB_ROWS) > 0);
		}
	}

	return false;
}

/* light rollout - fully random */
std::string EinSteinWurfeltNichtState::get_random_move() const
{
	if (dice_rolled_) {
		return get_random_move_from_dice();
	} else {
		return get_random_dice();
	}
}

std::string EinSteinWurfeltNichtState::get_random_dice() const
{
	const int player = 3 - playerJustMoved_;

	int pieces[6];
	int nb_pieces = 0;
	int valid_pieces[6];
	double pieces_prob[6];
	memset(pieces, 0, sizeof(pieces));

	for (int i = 0; i < NB_COLUMNS; ++i) {
		for (int j = 0; j < NB_ROWS; ++j) {
			if (board_[j][i].player == player) {
				pieces[board_[j][i].number - 1] = 1;
				++nb_pieces;
			}
		}
	}

	if (nb_pieces == 1) { // don't roll dice
		return "3";
	}

	int lowest = 0, highest = 5;
	int p = 0;
	int v = 1;

	// lowest pieces
	while (pieces[lowest] == 0) {
		++lowest;
		++v;
	}
	assert(lowest < 6); // no piece!
	valid_pieces[p] = lowest + 1;
	pieces_prob[p] = v;
	++p;

	// highest pieces
	v = 1;
	while (pieces[highest] == 0) {
		++v;
		--highest;
	}
	valid_pieces[p] = highest + 1;
	pieces_prob[p] = v;
	++p;

	for (int i = lowest + 1; i < highest; ++i) {
		if (pieces[i] != 0) {
			valid_pieces[p] = i + 1;
			pieces_prob[p] = 1;
			++p;
		} else {
			int v = 1;
			while (pieces[i + 1] == 0) {
				++v;
				++i;
			}
			valid_pieces[p] = i + 1;
			pieces_prob[p] = v;
			++p;
		}
	}

	const int r = generate_number(6);
	int d = 0;
	p = 0;

	do {
		d += pieces_prob[p];
		++p;
	} while (d <= r);
	assert(p <= 6);

	return std::to_string(valid_pieces[p - 1]);
}

std::string EinSteinWurfeltNichtState::get_random_move_from_dice() const
{
	for (int i = 0; i < NB_COLUMNS; ++i) {
		for (int j = 0; j < NB_ROWS; ++j) {
			if (board_[j][i].player == playerJustMoved_ && board_[j][i].number == dice_) {
				return get_piece_random_move(i, j);
			}
		}
	}

	int next_highest = 7;
	int next_lowest = 0;
	int highest_x = 0, highest_y = 0, lowest_x = 0, lowest_y = 0;
	for (int i = 0; i < NB_COLUMNS; ++i) {
		for (int j = 0; j < NB_ROWS; ++j) {
			if (board_[j][i].player == playerJustMoved_) {
				const int number = board_[j][i].number;
				if (number > dice_) {
					if (number < next_highest) {
						next_highest = number;
						highest_x = i;
						highest_y = j;
					}
				} else {
					if (number > next_lowest) {
						next_lowest = number;
						lowest_x = i;
						lowest_y = j;
					}
				}
			}
		}
	}

	// if two choice, choose one piece
	if (next_highest != 7 && next_lowest != 0) {
		if (generate_number(0, 1) == 0) {
			next_highest = 7;
		} else {
			next_lowest = 0;
		}
	}

	if (next_highest != 7) {
		assert(next_highest > dice_ && next_highest <= 6);
		return get_piece_random_move(highest_x, highest_y);
	}
	else {
	//if (next_lowest != 0) {
		assert(next_lowest >= 1 && next_lowest < dice_);
		return get_piece_random_move(lowest_x, lowest_y);
	}
}

std::string EinSteinWurfeltNichtState::get_piece_random_move(const int x, const int y) const
{
	std::string move = (char) (4 - x + 'a') + std::to_string(y + 1);
	char dst_x[3];
	int dst_y[3];
	int nb_moves = 0;

	if (playerJustMoved_ == 1) { // player 1 moves
		if (x < 4) {
			dst_x[nb_moves] = 3 - x + 'a';
			dst_y[nb_moves] = y + 1;
			++nb_moves;
		}

		if (y < 4) {
			dst_x[nb_moves] = 4 - x + 'a';
			dst_y[nb_moves] = y + 2;
			++nb_moves;
			if (x < 4) {
				dst_x[nb_moves] = 3 - x + 'a';
				dst_y[nb_moves] = y + 2;
				++nb_moves;
			}
		}
	} else {
		if (y > 0) {
			dst_x[nb_moves] = 4 - x + 'a';
			dst_y[nb_moves] = y;
			++nb_moves;
			if (x > 0) {
				dst_x[nb_moves] = 5 - x + 'a';
				dst_y[nb_moves] = y;
				++nb_moves;
			}
		}

		if (x > 0) {
			dst_x[nb_moves] = 5 - x + 'a';
			dst_y[nb_moves] = y + 1;
			++nb_moves;
		}
	}

	assert(nb_moves > 0);
	const int r = generate_number(nb_moves);

	return move + "-" + dst_x[r] + std::to_string(dst_y[r]);
}

bool EinSteinWurfeltNichtState::has_move_left() const
{
	return (winner_ == 0);
}

bool EinSteinWurfeltNichtState::has_one_move_left() const
{
	return false;
}

std::string EinSteinWurfeltNichtState::get_only_move_left() const
{
	assert("Error: This game doesn't use the \"only one move\" optimization." == 0);
	return "";
}

double EinSteinWurfeltNichtState::get_result(const int playerjm) const
{
	assert(winner_ != 0); // no draws in this game

	if (winner_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}
