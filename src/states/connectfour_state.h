#ifndef BOARDGAME_CONNECTFOUR_STATE_H
#define BOARDGAME_CONNECTFOUR_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"

/** Connect Four State
	move are in the range 0-6 for a 7 columns game
*/
class ConnectFourState : public GameState
{
public:
	ConnectFourState();

	~ConnectFourState();

	GameState* clone() const;

	bool try_move(const int move);

	void do_move(const std::string& move);

	std::string get_random_move() const;

	void get_moves(std::list<std::string>& moves) const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	std::string get_only_move_left() const;

	double get_result(const int playerjm) const;

	const int* get_board() const;

	int get_free_row(const int column);

	void hightlight_winning_pieces(const int column);

	void print() const;

	static const int NB_ROWS = 6;
	static const int NB_COLUMNS = 7;
	static const int HIGHTLIGHT_PIECE = 10;

private:
	// 0 = empty, 1 = player 1, 2 = player 2
	int board_[NB_COLUMNS][NB_ROWS];

	int moves_left_;

	int winner_;

	int get_winning_move() const;

	/** Try which result outcome if a player play a position. */
	bool try_result(const int player, const int column, const int row) const;
};

#endif //BOARDGAME_CONNECTFOUR_STATE_H
