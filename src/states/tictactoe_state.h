#ifndef BOARDGAME_TICTACTOE_STATE_H
#define BOARDGAME_TICTACTOE_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/game_state.h"

class TicTacToeState : public GameState
{
public:
	TicTacToeState();

	~TicTacToeState();

	GameState* clone() const;

	bool try_move(const int move);

	void do_move(const std::string& move);

	std::string get_random_move() const;

	void get_moves(std::list<std::string>& moves) const;

	bool has_move_left() const;

	bool has_one_move_left() const;

	std::string get_only_move_left() const;

	double get_result(const int playerjm) const;

	const int* get_board() const;

	static const int NB_TOTAL_MOVE = 9;

private:
	// 0 = empty, 1 = player 1, 2 = player 2
	int board_[NB_TOTAL_MOVE];

	int moves_left_;

	int winner_;

	void check_result();
};

#endif //BOARDGAME_TICTACTOE_STATE_H
