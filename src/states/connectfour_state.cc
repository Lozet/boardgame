// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/connectfour_state.h"
#include "random.h"
#include <cstring>
#include <cstdio>
#include <cassert>

ConnectFourState::ConnectFourState()
: moves_left_(NB_COLUMNS * NB_ROWS),
  winner_(0)
{
	memset(board_, 0, sizeof(board_));
}

ConnectFourState::~ConnectFourState()
{
}

GameState* ConnectFourState::clone() const
{
	ConnectFourState*const state = new ConnectFourState(*this);
	return state;
}

const int* ConnectFourState::get_board() const
{
	return (const int*) board_;
}

bool ConnectFourState::try_move(const int move)
{
	if (move < 0 || move >= NB_COLUMNS) {
		return false;
	}

	// if the columns is not full
	if (board_[move][0] == 0) {
		return true;
	} else {
		return false;
	}
}

int ConnectFourState::get_free_row(const int column)
{
	int i = 1;
	while (i < NB_ROWS && board_[column][i] == 0) {
		++i;
	}
	return i - 1;
}

void ConnectFourState::do_move(const std::string& move)
{
	const int column = std::stoi(move);
	assert(column >= 0 && column < NB_COLUMNS);
	assert(board_[column][0] == 0);
	const int current_player = get_current_player();

	int row = get_free_row(column);
	board_[column][row] = current_player;

	// update result
	--moves_left_;
	if (try_result(current_player, column, row)) {
		winner_ = current_player;
	}

	playerJustMoved_ = current_player;
}

void ConnectFourState::print() const
{
	printf("move left : %d, result = %d\n", moves_left_, winner_);
	for (int j = 0; j < 6; ++j) {
		for (int i = 0; i < 7; ++i) {
			printf("%d ", board_[i][j]);
		}
		putchar('\n');
	}
	putchar('\n');
}

int ConnectFourState::get_winning_move() const
{
	int dangerous = -1;
	int row;
	for (int col = 0; col < NB_COLUMNS; ++col) {
		if (board_[col][0] == 0) {
			// get the row the piece will fall into
			row = 1;
			while (row < NB_ROWS && board_[col][row] == 0) {
				++row;
			}

			// if the AI can make a four in a row, do it
			if (try_result(get_current_player(), col, row - 1)) {
				return col;
			}

			// if the opponent could make a four in a row, do it before
			if (try_result(playerJustMoved_, col, row - 1)) {
				dangerous = col;
			}
		}
	}

	if (dangerous != -1) {
		return dangerous;
	} else {
		return -1;
	}
}

void ConnectFourState::get_moves(std::list<std::string>& moves) const
{
	// if the game is finished, there is no available move
	if (has_move_left()) {
		// heavy play - force to play only winning move if available
		const int winning_move = get_winning_move();
		if (winning_move != -1) {
			moves.push_back(std::to_string(winning_move));
			return;
		}

		for (int i = 0; i < NB_COLUMNS; ++i) {
			if (board_[i][0] == 0) {
				moves.push_back(std::to_string(i));
			}
		}
	}
}

/* light rollout - fully random
std::string ConnectFourState::get_random_move() const
{
	int r = generate_number(NB_COLUMNS);

	while (board_[r][0] != 0) {
		if (++r >= NB_COLUMNS) {
			r = 0;
		}
	}

	return std::to_string(r);
}*/

/* heavy rollout - avoid bad moves */
std::string ConnectFourState::get_random_move() const
{
	assert(moves_left_ > 0);

	const int winning_move = get_winning_move();
	if (winning_move != -1) {
		return std::to_string(winning_move);
	}

	int r = generate_number(NB_COLUMNS);
	while (board_[r][0] != 0) {
		if (++r >= NB_COLUMNS) {
			r = 0;
		}
	}

	return std::to_string(r);
}

bool ConnectFourState::has_move_left() const
{
	return (moves_left_ > 0 && winner_ == 0);
}

bool ConnectFourState::has_one_move_left() const
{
	return (get_winning_move() != -1 || moves_left_ == 1) && (winner_ == 0);
}

std::string ConnectFourState::get_only_move_left() const
{
	const int winning_move = get_winning_move();
	if (winning_move != -1) {
		return std::to_string(winning_move);
	}

	for (int i = 0; i < NB_COLUMNS; ++i) {
		if (board_[i][0] == 0) {
			return std::to_string(i);
		}
	}

	assert("Only move not found" == 0);
	return "";
}

double ConnectFourState::get_result(const int playerjm) const
{
	if (winner_ == 0) { // draw
		assert(moves_left_ == 0);
		return 0.5;
	} else if (winner_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}

bool ConnectFourState::try_result(const int player, const int column, const int row) const
{
	// check if the new move created a connection

	// check horizontal connection
	int nb = 1;
	int c = column - 1;
	while (c >= 0 && board_[c][row] == player) {
		--c;
		++nb;
	}

	c = column + 1;
	while ((c < NB_COLUMNS) && (board_[c][row] == player)) {
		++c;
		++nb;
	}

	if (nb >= 4) {
		return true;
	}

	// check vertical connection
	nb = 1;
	int r = row + 1;
	while (r < NB_ROWS && board_[column][r] == player) {
		++r;
		++nb;
	}

	if (nb >= 4) {
		return true;
	}

	// check up/left down/right connection
	nb = 1;
	c = column - 1;
	r = row - 1;
	while (c >= 0 && r >= 0 && board_[c][r] == player) {
		--c;
		--r;
		++nb;
	}

	c = column + 1;
	r = row + 1;
	while (c < NB_COLUMNS && r < NB_ROWS && board_[c][r] == player) {
		++c;
		++r;
		++nb;
	}

	if (nb >= 4) {
		return true;
	}

	// check up/right down/left connection
	nb = 1;
	c = column + 1;
	r = row - 1;
	while (c < NB_COLUMNS && r >= 0 && board_[c][r] == player) {
		++c;
		--r;
		++nb;
	}

	c = column - 1;
	r = row + 1;
	while (c >= 0 && r < NB_ROWS && board_[c][r] == player) {
		--c;
		++r;
		++nb;
	}

	if (nb >= 4) {
		return true;
	}

	// game not finished
	return false;
}

void ConnectFourState::hightlight_winning_pieces(const int column)
{
	// get the row
	int row = 0;
	while (row < NB_ROWS && board_[column][row] == 0) {
		++row;
	}

	// check horizontal connection
	int nb = 1;
	int c = column - 1;
	while (c >= 0 && board_[c][row] == winner_) {
		--c;
		++nb;
	}

	c = column + 1;
	while ((c < NB_COLUMNS) && (board_[c][row] == winner_)) {
		++c;
		++nb;
	}

	if (nb >= 4) {
		for (int i = c - nb; i < c; ++i) {
			board_[i][row] += HIGHTLIGHT_PIECE;
		}
	}

	// check vertical connection
	nb = 1;
	int r = row + 1;
	while (r < NB_ROWS && board_[column][r] == winner_) {
		++r;
		++nb;
	}

	if (nb >= 4) {
		for (int i = r - nb; i < r; ++i) {
			if (board_[column][i] < HIGHTLIGHT_PIECE) {
				board_[column][i] += HIGHTLIGHT_PIECE;
			}
		}
	}

	// check up/right down/left connection
	nb = 1;
	c = column + 1;
	r = row - 1;
	while (c < NB_COLUMNS && r >= 0 && board_[c][r] == winner_) {
		++c;
		--r;
		++nb;
	}

	c = column - 1;
	r = row + 1;
	while (c >= 0 && r < NB_ROWS && board_[c][r] == winner_) {
		--c;
		++r;
		++nb;
	}

	if (nb >= 4) {
		const int row_start = r - 1;
		const int column_start = c + 1;
		for (int i = row_start, j = column_start; i > row_start - nb && j < column_start + nb; --i, ++j) {
			if (board_[j][i] < HIGHTLIGHT_PIECE) {
				board_[j][i] += HIGHTLIGHT_PIECE;
			}
		}
	}

	// check up/left down/right connection
	nb = 1;
	c = column - 1;
	r = row - 1;
	while (c >= 0 && r >= 0 && board_[c][r] == winner_) {
		--c;
		--r;
		++nb;
	}

	c = column + 1;
	r = row + 1;
	while (c < NB_COLUMNS && r < NB_ROWS && board_[c][r] == winner_) {
		++c;
		++r;
		++nb;
	}

	if (nb >= 4) {
		const int row_start = r - 1;
		const int column_start = c - 1;
		for (int i = row_start, j = column_start; i > row_start - nb && j > column_start - nb; --i, --j) {
			if (board_[j][i] < HIGHTLIGHT_PIECE) {
				board_[j][i] += HIGHTLIGHT_PIECE;
			}
		}
	}
}
