// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/international_draughts_state.h"
#include <cstring>
#include <cstdio>
#include <cassert>

template <size_t S>
InternationalDraughtsState<S>::InternationalDraughtsState()
: NB_SQUARES(S * S / 2),
  FIRST_COLUMN(S / 2)
{
	memset(board_, 0, sizeof(board_));
	const int nb_pieces = ((S - 2) * S) / 4;
	for (int i = 0; i < nb_pieces; ++i) {
		set_square(i, SQUARE_PION, 2);
		set_square(NB_SQUARES - 1 - i, SQUARE_PION, 1);
	}

	find_moves();
}

template <size_t S>
InternationalDraughtsState<S>::~InternationalDraughtsState()
{
}

template <size_t S>
GameState* InternationalDraughtsState<S>::clone() const
{
	// the <S> isn't needed?
	return new InternationalDraughtsState<S>(*this);
}

template <size_t S>
void InternationalDraughtsState<S>::print() const
{
	printf("Board :\n");
	for (int y = 0; y < S; ++y) {
		for (int x = 0; x < S; ++x) {
			// draw the square
			if ((x+y) % 2 == 1) {
				const Square& s = board_[y * FIRST_COLUMN + (x/2)];

				if (s.type == SQUARE_EMPTY) {
					putchar('#');
				} else {
					if (s.player == 1) {
						if (s.type == SQUARE_PION) {
							putchar('o');
						} else { // SQUARE_DAME
							putchar('O');
						}
					} else if (s.player == 2) {
						if (s.type == SQUARE_PION) {
							putchar('x');
						} else { // SQUARE_DAME
							putchar('X');
						}
					}
				}
			} else {
				putchar(' ');
			}
		}
		putchar('\n');
	}
}

template <size_t S>
const DraughtsState::Square* InternationalDraughtsState<S>::get_board() const
{
	return (const DraughtsState::Square*) board_;
}

template <size_t S>
void InternationalDraughtsState<S>::fill_layout(BoardLayout& layout)
{
	for (int i = 0; i < NB_SQUARES; ++i) {
		layout.board[i] = (board_[i].type << 4) + board_[i].player;
	}
}

template <size_t S>
void InternationalDraughtsState<S>::do_move(const std::string& move)
{
	playerJustMoved_ = 3 - playerJustMoved_; // playerJustMoved is now the current player

	// eg. "33-29", "21x32", "18x29x38", ...
	std::string str = move.c_str();
	std::string::size_type sz;

	int square = std::stoi(str, &sz) - 1;
	const char action = str[sz];
	str.erase(0, sz + 1);

	int destination = std::stoi(str, &sz) - 1;
	str.erase(0, sz);

	bool not_draw = true;
	const bool is_king_move = (action == '-' && board_[square].type == SQUARE_DAME);

	// if there is a active draw counter
	if (draw_counter_ > 0) {
		if (--draw_counter_ == 0) {
			not_draw = false;
		}
	}
	// if it's a king move
	else if (is_king_move) {
		// 25 king moves in a row
		if (++king_moves_ >= MAX_KING_MOVES) {
			not_draw = false;
		} else if (know_boards_.empty()) {
			// store the layout before the move for the current player
			BoardLayout layout;
			fill_layout(layout);
			layout.player[playerJustMoved_ - 1] = 1;
			layout.player[2 - playerJustMoved_] = 0;

			know_boards_.push_back(layout);
		}
	} else {
		king_moves_ = 0;
		know_boards_.clear();
	}

	if (action == '-')
	{
		board_[destination].type = board_[square].type;
		board_[destination].player = board_[square].player;
		board_[square].type = SQUARE_EMPTY;
		board_[square].player = 0;

		check_for_promotion(destination);
	}
	else if (action == 'x')
	{
		// move the current player piece
		board_[destination].type = board_[square].type;
		board_[destination].player = board_[square].player;
		board_[square].type = SQUARE_EMPTY;
		board_[square].player = 0;

		// remove the opponent piece
		int target = get_jump_target(square, destination);
		board_[target].type = SQUARE_EMPTY;
		board_[target].player = 0;

		// check rafle
		while (!str.empty()) {
			assert(str.at(0) == 'x');
			str.erase(0, 1);

			square = destination;
			destination = std::stoi(str, &sz) - 1;
			str.erase(0, sz);

			board_[destination].type = board_[square].type;
			board_[destination].player = board_[square].player;
			board_[square].type = SQUARE_EMPTY;
			board_[square].player = 0;

			target = get_jump_target(square, destination);
			board_[target].type = SQUARE_EMPTY;
			board_[target].player = 0;
		}

		check_for_promotion(destination);
	}

	// free the moves from previous player
	moves_.clear();

	if (is_king_move && draw_counter_ <= 0) {
		// store the new layout
		BoardLayout layout;
		fill_layout(layout);

		// search for the same layout
		int i;
		for (i = 0; i < know_boards_.size(); ++i) {
			if (!memcmp(layout.board, know_boards_[i].board, sizeof(char) * NB_SQUARES)) {
				break;
			}
		}

		// if this is a new layout
		if (i == know_boards_.size()) {
			layout.player[playerJustMoved_ - 1] = 0;
			layout.player[2 - playerJustMoved_] = 1;
			know_boards_.push_back(layout);
		} else {
			if (++know_boards_[i].player[2 - playerJustMoved_] >= 3) {
				not_draw = false;
				know_boards_.clear();
			}
		}
	}

	// if the game isn't in a draw state
	if (not_draw) {
		// find the move for the current player
		find_moves();
	}
}

template <size_t S>
void InternationalDraughtsState<S>::check_for_promotion(const int square)
{
	if (board_[square].type == SQUARE_PION) {
		const int player = board_[square].player;
		const int row = get_row(square);
		if ((player == 1 && row == 0) || (player == 2 && row == S - 1)) {
			board_[square].type = SQUARE_DAME;
		}
	}
}

template <size_t S>
int InternationalDraughtsState<S>::get_movement(const int src, const int dst) const
{
	const int mod = (dst % FIRST_COLUMN) - (src % FIRST_COLUMN);

	if (dst - src > 0) // backward move from white POV
	{
		if (mod > 0) {
			return FIRST_COLUMN + 1; // DIR_RIGHT_DOWN;
		} else if (mod < 0) {
			return FIRST_COLUMN; // DIR_LEFT_DOWN;
		} else if ((src / FIRST_COLUMN) % 2 == 0) {
			return FIRST_COLUMN; // DIR_LEFT_DOWN
		} else {
			return FIRST_COLUMN + 1; // DIR_RIGHT_DOWN;
		}
	}
	else // forward move from white POV
	{
		if (mod > 0) {
			return -FIRST_COLUMN + 1; // DIR_RIGHT_UP;
		} else if (mod < 0) {
			return -FIRST_COLUMN; // DIR_LEFT_UP;
		} else if ((src / FIRST_COLUMN) % 2 == 0) {
			return -FIRST_COLUMN + 1; // DIR_RIGHT_UP;
		} else {
			return -FIRST_COLUMN; // DIR_LEFT_UP;
		}
	}

	fprintf(stderr, "Error: Direction from %d to %d does not exists\n", src, dst);
	assert("Direction not found" == 0);
	return 0;
}

template <size_t S>
int InternationalDraughtsState<S>::get_movement(const Direction direction) const
{
	switch (direction) {
		case DIR_RIGHT_DOWN:
			return FIRST_COLUMN + 1;
		case DIR_LEFT_DOWN:
			return FIRST_COLUMN;
		case DIR_RIGHT_UP:
			return -FIRST_COLUMN + 1;
		case DIR_LEFT_UP:
			return -FIRST_COLUMN;
	}
	return 0;
}

template <size_t S>
void InternationalDraughtsState<S>::try_pion_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square, const Direction direction) const
{
	const int move = get_movement(direction);

	const int target = square + move - ((square / FIRST_COLUMN) % 2);
	if (board_[target].type != SQUARE_EMPTY && board_[target].player != player)
	{
		const int destination = target + move - ((target / FIRST_COLUMN) % 2);
		if (board_[destination].type == SQUARE_EMPTY) // can jump
		{
			if (parent == captures.cend())
			{
				std::string move = std::to_string(square + 1) + "x" + std::to_string(destination + 1);
				captures.emplace_back(move, target);
				get_pion_captures(captures, --captures.cend(), player, destination); // recursively find jump available
			}
			else if ((*parent).can_take(target)) // can't jump over the same piece twice
			{
				std::string move = (*parent).move + "x" + std::to_string(destination + 1);
				captures.emplace_back(move, (*parent).pieces, target);
				get_pion_captures(captures, --captures.cend(), player, destination);
			}
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::get_pion_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square) const
{
	const int column = get_column(square);
	const int row = get_row(square);

	// check for forward jumps from black POV
	if (row < S - 2)
	{
		// check for left jump
		if (column != FIRST_COLUMN && column != 0) {
			try_pion_capture(captures, parent, player, square, DIR_LEFT_DOWN);
		}
		// check for right jump
		if (column != S - 1 && column != FIRST_COLUMN - 1) {
			try_pion_capture(captures, parent, player, square, DIR_RIGHT_DOWN);
		}
	}

	// check for backward jumps from black POV
	if (row > 1)
	{
		// check for left jump
		if (column != FIRST_COLUMN && column != 0) {
			try_pion_capture(captures, parent, player, square, DIR_LEFT_UP);
		}
		// check for right jump
		if (column != S - 1 && column != FIRST_COLUMN - 1) {
			try_pion_capture(captures, parent, player, square, DIR_RIGHT_UP);
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::get_dame_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square) const
{
	try_dame_capture(captures, parent, player, square, DIR_LEFT_DOWN, S - 1, FIRST_COLUMN);
	try_dame_capture(captures, parent, player, square, DIR_RIGHT_DOWN, S - 1, FIRST_COLUMN - 1);
	try_dame_capture(captures, parent, player, square, DIR_LEFT_UP, 0, FIRST_COLUMN);
	try_dame_capture(captures, parent, player, square, DIR_RIGHT_UP, 0, FIRST_COLUMN - 1);
}

template <size_t S>
void InternationalDraughtsState<S>::try_dame_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square, const Direction direction, const int max_row, const int max_column) const
{
	int i = square;
	bool valid = true;

	while (valid && get_row(i) != max_row && get_column(i) != max_column)
	{
		move_position(i, direction);

		if (board_[i].type != SQUARE_EMPTY)
		{
			if (board_[i].player == player) {
				// can't take our own piece
				valid = false;
			} else {
				// find a potential target
				const int target = i;

				while (valid && get_row(i) != max_row && get_column(i) != max_column)
				{
					move_position(i, direction);

					if (board_[i].type == SQUARE_EMPTY) {
						if (parent == captures.cend())
						{
							std::string move = std::to_string(square + 1) + "x" + std::to_string(i + 1);
							captures.emplace_back(move, target);
							// recursively find jump available
							get_dame_captures(captures, --captures.cend(), player, i);
						}
						else if ((*parent).can_take(target)) // can't jump over the same piece twice
						{
							std::string move = (*parent).move + "x" + std::to_string(i + 1);
							captures.emplace_back(move, (*parent).pieces, target);

							get_dame_captures(captures, --captures.cend(), player, i);
						}
					} else {
						valid = false;
					}
				}
			}
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::try_pion_move(const int square, const Direction direction)
{
	const int move = get_movement(direction);
	const int destination = square + move - ((square / FIRST_COLUMN) % 2);
	if (board_[destination].type == SQUARE_EMPTY)
	{
		moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(destination + 1));
	}
}

template <size_t S>
void InternationalDraughtsState<S>::get_pion_moves(const int square)
{
	const int column = get_column(square);
	const int row = get_row(square);

	// check for forward moves from black POV
	if (row != S - 1 && board_[square].player == 2)
	{
		// check for left move
		if (column != FIRST_COLUMN) {
			try_pion_move(square, DIR_LEFT_DOWN);
		}
		// check for right move
		if (column != FIRST_COLUMN - 1) {
			try_pion_move(square, DIR_RIGHT_DOWN);
		}
	}

	// check for forward moves from white POV
	if (row != 0 && board_[square].player == 1)
	{
		// check for left move
		if (column != FIRST_COLUMN) {
			try_pion_move(square, DIR_LEFT_UP);
		}
		// check for right move
		if (column != FIRST_COLUMN - 1) {
			try_pion_move(square, DIR_RIGHT_UP);
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::get_dame_moves(const int square)
{
	// check for left forward move from black POV
	int i = square;
	while (get_row(i) != S - 1 && get_column(i) != FIRST_COLUMN)
	{
		move_position(i, DIR_LEFT_DOWN);

		if (board_[i].type == SQUARE_EMPTY) {
			moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(i + 1));
		} else {
			break;
		}
	}

	// check for right forward move from black POV
	i = square;
	while (get_row(i) != S - 1 && get_column(i) != FIRST_COLUMN - 1)
	{
		move_position(i, DIR_RIGHT_DOWN);

		if (board_[i].type == SQUARE_EMPTY) {
			moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(i + 1));
		} else {
			break;
		}
	}

	// check for left upward move from black POV
	i = square;
	while (get_row(i) != 0 && get_column(i) != FIRST_COLUMN)
	{
		move_position(i, DIR_LEFT_UP);

		if (board_[i].type == SQUARE_EMPTY) {
			moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(i + 1));
		} else {
			break;
		}
	}

	// check for right upward move from black POV
	i = square;
	while (get_row(i) != 0 && get_column(i) != FIRST_COLUMN - 1)
	{
		move_position(i, DIR_RIGHT_UP);

		if (board_[i].type == SQUARE_EMPTY) {
			moves_.push_back(std::to_string(square + 1) + "-" + std::to_string(i + 1));
		} else {
			break;
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::check_for_draw(const int pieces[2][2])
{
	// if white has only one king left
	if (pieces[0][0] == 0 && pieces[0][1] == 1)
	{
		if ((draw_counter_ == 0) && (
			(pieces[1][0] == 0 && pieces[1][1] == 3) ||
			(pieces[1][0] == 1 && pieces[1][1] == 2) ||
			(pieces[1][0] == 2 && pieces[1][1] == 1)))
		{
			draw_counter_ = MAX_COUNTER_3VS1;
		}
		else if ((draw_counter_ == 0 || draw_counter_ > MAX_COUNTER_2VS1) && (
				 (pieces[1][0] == 0 && pieces[1][1] == 2) ||
				 (pieces[1][0] == 1 && pieces[1][1] == 1) ||
				 (pieces[1][0] == 0 && pieces[1][1] == 1)))
		{
			draw_counter_ = MAX_COUNTER_2VS1;
		}
	} // if black has only one king left
	else if (pieces[1][0] == 0 && pieces[1][1] == 1)
	{
		if ((draw_counter_ == 0) && (
			(pieces[0][0] == 0 && pieces[0][1] == 3) ||
			(pieces[0][0] == 1 && pieces[0][1] == 2) ||
			(pieces[0][0] == 2 && pieces[0][1] == 1)))
		{
			draw_counter_ = MAX_COUNTER_3VS1;
		}
		else if ((draw_counter_ == 0 || draw_counter_ > MAX_COUNTER_2VS1) && (
				 (pieces[0][0] == 0 && pieces[0][1] == 2) ||
				 (pieces[0][0] == 1 && pieces[0][1] == 1) ||
				 (pieces[0][0] == 0 && pieces[0][1] == 1)))
		{
			draw_counter_ = MAX_COUNTER_2VS1;
		}
	}
}

template <size_t S>
void InternationalDraughtsState<S>::find_moves()
{
	const int current_player = 3 - playerJustMoved_;

	// check players pieces
	int pieces[2][2];
	memset(pieces, 0, sizeof(pieces));

	// look for mandatory moves
	std::list<Capture> captures;

	for (int i = 0; i < NB_SQUARES; ++i) {
		const Square& s = board_[i];

		if (s.player != 0 && s.type != 0) {
			++pieces[s.player - 1][s.type - 1];
		}

		// if the square contains a piece owned by the current player
		if (s.player == current_player)
		{
			if (s.type == SQUARE_PION) {
				get_pion_captures(captures, captures.cend(), current_player, i);
			} else if (s.type == SQUARE_DAME) {
				get_dame_captures(captures, captures.cend(), current_player, i);
			}
		}
	}

	// check if a draw counter must be activated
	check_for_draw(pieces);

	int nb_captures = 0;
	for (auto it = captures.cbegin(); it != captures.cend(); ++it)
	{
		const int nb = (*it).pieces.size();
		if (nb > nb_captures) {
			nb_captures = nb;
		}
	}

	if (nb_captures > 0)
	{
		for (auto it = captures.cbegin(); it != captures.cend(); ++it)
		{
			if ((*it).pieces.size() == (unsigned int) nb_captures) {
				moves_.push_back((*it).move);
			}
		}
	}
	else // look for moves
	{
		for (int i = 0; i < NB_SQUARES; ++i) {
			const Square& s = board_[i];
			if (s.player == current_player)
			{
				if (s.type == SQUARE_PION) {
					get_pion_moves(i);
				} else if (s.type == SQUARE_DAME) {
					get_dame_moves(i);
				}
			}
		}

		// if there is no moves available
		if (moves_.empty()) {
			// the current player lose the game
			winner_ = playerJustMoved_;
		}
	}
}

template <size_t S>
int InternationalDraughtsState<S>::get_jump_target(const int source, const int destination) const
{
	const int move = get_movement(source, destination);
	int target = source;
	do {
		target += move - (get_row(target) % 2);
	} while (board_[target].type == DraughtsState::SQUARE_EMPTY);
	return target;
}

template <size_t S>
int InternationalDraughtsState<S>::get_board_size() const
{
	return S;
}

template <size_t S>
void InternationalDraughtsState<S>::get_player1_color(Color& color) const
{
	color.r = 0.99;
	color.g = 0.8;
	color.b = 0.28;
}

template <size_t S>
void InternationalDraughtsState<S>::get_player2_color(Color& color) const
{
	color.r = 0.38;
	color.g = 0.27;
	color.b = 0.23;
}

template <size_t S>
void InternationalDraughtsState<S>::get_player1_highlight_color(Color& color) const
{
	color.r = 0.99;
	color.g = 0.6;
	color.b = 0.28;
}

template <size_t S>
void InternationalDraughtsState<S>::get_player2_highlight_color(Color& color) const
{
	color.r = 0.53;
	color.g = 0.27;
	color.b = 0.23;
}

template <size_t S>
void InternationalDraughtsState<S>::get_dark_square_color(Color& color) const
{
	color.r = 0.65;
	color.g = 0.49;
	color.b = 0.36;
}

template <size_t S>
void InternationalDraughtsState<S>::get_light_square_color(Color& color) const
{
	color.r = 0.91;
	color.g = 0.82;
	color.b = 0.67;
}
