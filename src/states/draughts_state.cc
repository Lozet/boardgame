// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/draughts_state.h"
#include "random.h"
#include <cassert>

DraughtsState::DraughtsState()
: winner_(0),
  draw_counter_(0),
  king_moves_(0)
{
}

DraughtsState::~DraughtsState()
{
}

int DraughtsState::get_draw_counter() const
{
	return draw_counter_;
}

int DraughtsState::get_king_moves() const
{
	return king_moves_;
}

void DraughtsState::get_moves(std::list<std::string>& moves) const
{
	for (auto it = moves_.cbegin(); it != moves_.cend(); ++it) {
		moves.push_back(*it);
	}
}

bool DraughtsState::try_move(const std::string& move)
{
	for (auto it = moves_.cbegin(); it != moves_.cend(); ++it) {
		if (*it == move) {
			return true;
		}
	}
	return false;
}

void DraughtsState::get_moves_from(std::list<std::string>& moves, const int square) const
{
	for (auto it = moves_.cbegin(); it != moves_.cend(); ++it) {
		const int src = std::stoi(*it) - 1;
		if (src == square) {
			moves.push_back(*it);
		}
	}
}

std::string DraughtsState::get_random_move() const
{
	const int r = generate_number(moves_.size());
	std::list<std::string>::const_iterator it = moves_.cbegin();
	std::advance(it, r);
	return (*it);
}

bool DraughtsState::has_move_left() const
{
	return (moves_.size() > 0 && winner_ == 0);
}

bool DraughtsState::has_one_move_left() const
{
	return (moves_.size() == 1 && winner_ == 0);
}

std::string DraughtsState::get_only_move_left() const
{
	assert(moves_.size() > 0);
	return moves_.front();
}

double DraughtsState::get_result(const int playerjm) const
{
	if (winner_ == 0) { // draw
		assert(moves_.size() == 0);
		return 0.5;
	} else if (winner_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}

int DraughtsState::get_winner() const
{
	return winner_;
}
