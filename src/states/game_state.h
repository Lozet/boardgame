#ifndef BOARDGAME_GAME_STATE_H
#define BOARDGAME_GAME_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <list>
#include <string>

class GameState
{
public:
	GameState();

	virtual ~GameState();

	virtual GameState* clone() const = 0;

	virtual bool has_move_left() const = 0;

	virtual bool has_one_move_left() const = 0;

	virtual bool is_random_move() const { return false; };

	virtual void get_moves(std::list<std::string>& moves) const = 0;

	virtual std::string get_random_move() const = 0;

	virtual std::string get_only_move_left() const = 0;

	virtual void do_move(const std::string& move) = 0;

	virtual double get_result(const int playerjm) const = 0;

	virtual void print() const;

	virtual int get_current_player() const;

	int get_player_just_moved() const;

protected:
	int playerJustMoved_;
};

#endif //BOARDGAME_GAME_STATE_H
