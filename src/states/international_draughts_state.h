#ifndef BOARDGAME_INTERNATIONAL_DRAUGHTS_STATE_H
#define BOARDGAME_INTERNATIONAL_DRAUGHTS_STATE_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/draughts_state.h"
#include <vector>

/** International Draughts State
*/
template <size_t S>
class InternationalDraughtsState : public DraughtsState
{
public:
	enum Direction
	{
		DIR_LEFT_DOWN,
		DIR_LEFT_UP,
		DIR_RIGHT_DOWN,
		DIR_RIGHT_UP
	};

	InternationalDraughtsState();

	~InternationalDraughtsState();

	// Game functions

	GameState* clone() const;

	void do_move(const std::string& move);

	void print() const;

	// Draughts functions

	const DraughtsState::Square* get_board() const;

	int get_board_size() const;

	int get_jump_target(const int source, const int destination) const;

	void get_player1_color(Color& color) const;

	void get_player2_color(Color& color) const;

	void get_player1_highlight_color(Color& color) const;

	void get_player2_highlight_color(Color& color) const;

	void get_dark_square_color(Color& color) const;

	void get_light_square_color(Color& color) const;

	// International Draughts functions

	static const int MAX_KING_MOVES = 25;

	static const int MAX_COUNTER_3VS1 = 32;

	static const int MAX_COUNTER_2VS1 = 10;

private:
	struct BoardLayout
	{
		char board[S * S / 2];
		int player[2];
	};

	const int NB_SQUARES;
	const int FIRST_COLUMN;

	Square board_[S * S / 2];

	// if a player is playing this board layout for the 3rd times, the game is a draw
	std::vector<BoardLayout> know_boards_;


	inline void set_square(const int i, const SquareType type, const int player) {
		board_[i].type = type;
		board_[i].player = player;
	}

	inline void free_square(const int i) {
		board_[i].type = SQUARE_EMPTY;
		board_[i].player = 0;
	}

	inline int get_row(const int square) const {
		return square / FIRST_COLUMN;
	}

	inline int get_column(const int square) const {
		return square % S;
	}

	inline void move_position(int& square, const Direction direction) const {
		square += get_movement(direction) - (get_row(square) % 2);
	}

	void find_moves();

	void get_pion_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square) const;

	void try_pion_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square, const Direction direction) const;

	void get_dame_captures(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square) const;

	void try_dame_capture(std::list<Capture>& captures, std::list<Capture>::const_iterator parent, const int player, const int square, const Direction direction, const int max_row, const int max_column) const;

	void get_pion_moves(const int square);

	void try_pion_move(const int square, const Direction direction);

	void get_dame_moves(const int square);

	void check_for_promotion(const int square);

	void check_for_draw(const int pieces[2][2]);

	int get_movement(const int src, const int dst) const;

	int get_movement(const Direction direction) const;

	void fill_layout(BoardLayout& layout);
};

typedef InternationalDraughtsState<8> BrazilianDraughtsState;
typedef InternationalDraughtsState<10> FrenchDraughtsState;
typedef InternationalDraughtsState<12> CanadianDraughtsState;

#include "states/international_draughts_state.tcc"

#endif //BOARDGAME_INTERNATIONAL_DRAUGHTS_STATE_H
