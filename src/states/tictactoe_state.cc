// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "states/tictactoe_state.h"
#include "random.h"
#include <cstring>
#include <cassert>

TicTacToeState::TicTacToeState()
: moves_left_(NB_TOTAL_MOVE),
  winner_(0)
{
	memset(board_, 0, sizeof(board_));
}

TicTacToeState::~TicTacToeState()
{
}

GameState* TicTacToeState::clone() const
{
	TicTacToeState*const state = new TicTacToeState(*this);
	return state;
}

const int* TicTacToeState::get_board() const
{
	return (const int*) board_;
}

bool TicTacToeState::try_move(const int move)
{
	if (move < 0 || move >= NB_TOTAL_MOVE) {
		return false;
	}

	if (board_[move] == 0) {
		return true;
	} else {
		return false;
	}
}

void TicTacToeState::do_move(const std::string& move)
{
	const int i = std::stoi(move);

	assert(i >= 0 && i < NB_TOTAL_MOVE);
	assert(board_[i] == 0);

	playerJustMoved_ = 3 - playerJustMoved_;
	board_[i] = playerJustMoved_;

	// update result
	--moves_left_;
	check_result();
}

void TicTacToeState::get_moves(std::list<std::string>& moves) const
{
	// if the game is finished, there is no available move
	if (has_move_left()) {
		for (int i = 0; i < NB_TOTAL_MOVE; ++i) {
			if (board_[i] == 0) {
				moves.push_back(std::to_string(i));
			}
		}
	}
}

std::string TicTacToeState::get_random_move() const
{
	int r = generate_number(NB_TOTAL_MOVE);

	while (board_[r] != 0) {
		if (++r >= NB_TOTAL_MOVE) {
			r = 0;
		}
	}

	return std::to_string(r);
}

bool TicTacToeState::has_move_left() const
{
	return (moves_left_ > 0 && winner_ == 0);
}

bool TicTacToeState::has_one_move_left() const
{
	return (moves_left_ == 1 && winner_ == 0);
}

std::string TicTacToeState::get_only_move_left() const
{
	for (int i = 0; i < NB_TOTAL_MOVE; ++i) {
		if (board_[i] == 0) {
			return std::to_string(i);
		}
	}

	assert("Only move not found" == 0);
	return "";
}

double TicTacToeState::get_result(const int playerjm) const
{
	if (winner_ == 0) { // draw
		assert(moves_left_ == 0);
		return 0.5;
	} else if (winner_ == playerjm) {
		return 1.0;
	} else {
		return 0.0;
	}
}

void TicTacToeState::check_result()
{
	int p;

	if (board_[0]) {
		p = board_[0];
		if ((p == board_[1] && p == board_[2]) ||
			(p == board_[3] && p == board_[6]) ||
			(p == board_[4] && p == board_[8]))
		{
			winner_ = p;
			return;
		}
	}

	if (board_[4]) {
		p = board_[4];
		if ((p == board_[3] && p == board_[5]) ||
			(p == board_[1] && p == board_[7]) ||
			(p == board_[2] && p == board_[6]))
		{
			winner_ = p;
			return;
		}
	}

	if (board_[8]) {
		p = board_[8];
		if ((p == board_[7] && p == board_[6]) ||
			(p == board_[2] && p == board_[5]))
		{
			winner_ = p;
		}
	}
}
