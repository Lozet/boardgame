// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "settings.h"
#include "exception.h"
#include <glibmm/variant.h>
//XXX range_check() : Deprecated:2.40:Use g_settings_schema_key_range_check() instead.

Settings::Settings()
{
	settings_game_ = Gio::Settings::create("org.lozet.boardgame.game");
	settings_ui_ = Gio::Settings::create("org.lozet.boardgame.ui");
}

Settings::~Settings()
{
}

WindowOptions Settings::get_ui_options() const
{
	WindowOptions win_options;
	win_options.maximized = get_maximized();
	win_options.width = get_width();
	win_options.height = get_height();
	win_options.toolbar_visible = is_toolbar_visible();
	win_options.statusbar_visible = is_statusbar_visible();
	win_options.history_visible = is_history_visible();
	return win_options;
}

void Settings::set_ui_options(const WindowOptions& options) const
{
	if (options.maximized) {
		set_maximized(true);
	} else {
		set_maximized(false);
		set_width(options.width);
		set_height(options.height);
	}

	set_toolbar_visible(options.toolbar_visible);
	set_statusbar_visible(options.statusbar_visible);
	set_history_visible(options.history_visible);
}

void Settings::set_game_options(const GameOptions& options) const
{
	set_last_game(options.game);
	set_nb_players(options.nb_players);
	set_difficulty(options.difficulty);
}

bool Settings::get_maximized() const
{
	return settings_ui_->get_boolean("maximized");
}

void Settings::set_maximized(const bool maximized) const
{
	settings_ui_->set_boolean("maximized", maximized);
}

int Settings::get_width() const
{
	return settings_ui_->get_int("width");
}

int Settings::get_height() const
{
	return settings_ui_->get_int("height");
}

void Settings::set_width(const int width) const
{
	settings_ui_->set_int("width", width);
}

void Settings::set_height(const int height) const
{
	settings_ui_->set_int("height", height);
}

bool Settings::is_toolbar_visible() const
{
	return settings_ui_->get_boolean("toolbar-visible");
}

void Settings::set_toolbar_visible(const bool visible) const
{
	settings_ui_->set_boolean("toolbar-visible", visible);
}

bool Settings::is_statusbar_visible() const
{
	return settings_ui_->get_boolean("statusbar-visible");
}

void Settings::set_statusbar_visible(const bool visible) const
{
	settings_ui_->set_boolean("statusbar-visible", visible);
}

bool Settings::is_history_visible() const
{
	return settings_ui_->get_boolean("history-visible");
}

void Settings::set_history_visible(const bool visible) const
{
	settings_ui_->set_boolean("history-visible", visible);
}


GameId Settings::get_last_game() const
{
	return (GameId) settings_game_->get_enum("last-game");
}

void Settings::set_last_game(const GameId last_game) const
{
	settings_game_->get_enum("last-game", last_game);
}

GameNumberPlayers Settings::get_nb_players() const
{
	return (GameNumberPlayers) settings_game_->get_enum("nb-players");
}

void Settings::set_nb_players(const GameNumberPlayers nb_players) const
{
	settings_game_->get_enum("nb-players", nb_players);
}

GameDifficulty Settings::get_difficulty() const
{
	return (GameDifficulty) settings_game_->get_enum("difficulty");
}

void Settings::set_difficulty(const GameDifficulty difficulty) const
{
	settings_game_->get_enum("difficulty", difficulty);
}
