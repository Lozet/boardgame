#ifndef BOARDGAME_EXCEPTION_H
#define BOARDGAME_EXCEPTION_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <glibmm/ustring.h>
#include <glibmm/i18n.h>

class BoardgameErr {
public:
	virtual Glib::ustring get_message() const {return _("Error in boardgame program");}
};

class MsgErr : public BoardgameErr {
public:
	MsgErr(const Glib::ustring& m) { message = m; }
	Glib::ustring get_message() const { return message; }
	void prepend_message(const Glib::ustring& m) { message = m + message; }
	void append_message(const Glib::ustring& m) { message += m; }
private:
	Glib::ustring message;
};

#endif // BOARDGAME_EXCEPTION_H
