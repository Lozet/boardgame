#ifndef BOARDGAME_SETTINGS_H
#define BOARDGAME_SETTINGS_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <giomm/settings.h>
#include "options.h"

class Settings
{
public:
	Settings();
	~Settings();

	WindowOptions get_ui_options() const;
	void set_ui_options(const WindowOptions& options) const;

	void set_game_options(const GameOptions& options) const;

	bool is_toolbar_visible() const;
	void set_toolbar_visible(const bool visible) const;

	bool is_statusbar_visible() const;
	void set_statusbar_visible(const bool visible) const;

	bool is_history_visible() const;
	void set_history_visible(const bool visible) const;

	bool get_maximized() const;
	void set_maximized(const bool maximized) const;

	int get_width() const;
	void set_width(const int width) const;

	int get_height() const;
	void set_height(const int height) const;

	GameId get_last_game() const;
	void set_last_game(const GameId last_game) const;

	GameNumberPlayers get_nb_players() const;
	void set_nb_players(const GameNumberPlayers nb_players) const;

	GameDifficulty get_difficulty() const;
	void set_difficulty(const GameDifficulty difficulty) const;

private:
	Glib::RefPtr<Gio::Settings> settings_game_;
	Glib::RefPtr<Gio::Settings> settings_ui_;
};

#endif //BOARDGAME_SETTINGS_H
