#ifndef BOARDGAME_RANDOM_H
#define BOARDGAME_RANDOM_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

/** init random functions */
void init_random();

/** generate a number in the range [min,max] */
int generate_number(const int min, const int max);

/** generate a number in the range [0,limit[ */
int generate_number(const int limit);

#endif // BOARDGAME_RANDOM_H
