#ifndef BOARDGAME_MESSAGEDIALOG_H
#define BOARDGAME_MESSAGEDIALOG_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <glibmm/ustring.h>
#include <gtkmm/messagedialog.h>

// message box functions
inline
void message_info(Gtk::Window& parent, const Glib::ustring &title, const Glib::ustring &message)
{
	Gtk::MessageDialog dialog(parent, title, false /* use_markup */, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true /* modal */);
	dialog.set_secondary_text(message);
	dialog.run();
}

inline
void message_info(const Glib::ustring &title, const Glib::ustring &message)
{
	Gtk::MessageDialog dialog(title, false /* use_markup */, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true /* modal */);
	dialog.set_secondary_text(message);
	dialog.run();
}

inline
void message_error(Gtk::Window& parent, const Glib::ustring &title, const Glib::ustring &message)
{
	Gtk::MessageDialog dialog(parent, title, false /* use_markup */, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true /* modal */);
	dialog.set_secondary_text(message);
	dialog.run();
}

inline
void message_error(const Glib::ustring &title, const Glib::ustring &message)
{
	Gtk::MessageDialog dialog(title, false /* use_markup */, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true /* modal */);
	dialog.set_secondary_text(message);
	dialog.run();
}

#endif // BOARDGAME_MESSAGEDIALOG_H
