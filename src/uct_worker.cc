// -*- c++ -*-
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "uct_worker.h"
#include "node.h"
#include "window.h"
#include <cstdio>

UctWorker::UctWorker() :
  mutex_(),
  shall_stop_(false),
  has_stopped_(false),
  move_("")
{
}

// Accesses to these data are synchronized by a mutex.
// Some microseconds can be saved by getting all data at once, instead of having
// separate get_foo() and get_bar() methods.
void UctWorker::get_move(std::string* move) const
{
	std::lock_guard<std::mutex> lock(mutex_);
	if (move) {
		*move = move_.c_str();
	}
}

void UctWorker::stop_work()
{
	std::lock_guard<std::mutex> lock(mutex_);
	shall_stop_ = true;
}

bool UctWorker::has_stopped() const
{
	std::lock_guard<std::mutex> lock(mutex_);
	return has_stopped_;
}

void UctWorker::do_work(MainWindow* caller, const GameState*const rootstate, const int itermax, const double uctk, const int verbosity)
{
	{
		std::lock_guard<std::mutex> lock(mutex_);
		has_stopped_ = false;
		move_ = "";
	} // The mutex is unlocked here by lock's destructor.

	std::string move = uct_search(rootstate, itermax, uctk, verbosity);
	bool should_notify;

	{ // set the result
		std::lock_guard<std::mutex> lock(mutex_);
		should_notify = !shall_stop_;
		shall_stop_ = false;
		has_stopped_ = true;
		move_ = move;
	}

	if (should_notify) {
		// notify the GUI that the move is found
		caller->notify();
	}
}

std::string UctWorker::uct_search(const GameState*const rootstate, const int itermax, const double uctk, const int verbosity)
{
	//printf("UctWorker::uct_search(%p, %d, %lf)\n", rootstate, itermax, uctk);
	Node rootnode("", 0, rootstate);
	Node* node;

	// UCT search
	for (int i = 0; i < itermax; ++i)
	{
		GameState*const state = rootstate->clone();
		node = &rootnode;

		// Select

		// while node is fully expanded and non-terminal
		while (node->is_selectable()) {
			if (state->is_random_move()) {
				const std::string m = state->get_random_move();
				node = node->select_child(m);
				state->do_move(m);
			} else {
				node = node->select_child(uctk);
				state->do_move(node->get_move());
			}
		}

		// Expand

		// if we can expand (i.e. state/node is non-terminal)
		if (node->is_expandable()) {
			const std::string m = node->get_random_move();
			state->do_move(m);
			node = node->add_child(m, state); // add child and descend tree
		}

		// Rollout - this can often be made orders of magnitude quicker using
		// a state.GetRandomMove() function

		// while state is non-terminal
		while (state->has_move_left()) {
			state->do_move(state->get_random_move()); // => do_random_move()
		}

		// Backpropagate

		// backpropagate from the expanded node and work back to the root node
		while (node) {
			// state is terminal. Update node with result from POV of node.playerJustMoved
			node->update(state->get_result(node->get_player()));
			node = node->get_parent();
		}

		delete state;

		if ((i % 1000) == 0)
		{
			std::lock_guard<std::mutex> lock(mutex_);

			// quit the loop if the GUI asked so
			if (shall_stop_) {
				break;
			}
		}

		// could notify thinking progression
		//caller->notify();
	}

	rootnode.display_tree(verbosity);

	// return the most visited move
	return rootnode.get_most_visited_move();
}
