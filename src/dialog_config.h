#ifndef BOARDGAME_DIALOG_CONFIG_H
#define BOARDGAME_DIALOG_CONFIG_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "combobox_game.h"
#include "options.h"
#include <gtkmm/comboboxtext.h>
#include <gtkmm/dialog.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>

class DialogConfig : public Gtk::Dialog
{
private:
	void pack_widgets();
	void fill_widgets();
	void preselect_options(GameOptions& options);

protected:
	Gtk::Box vbox_;

	Gtk::Box hb_game_;
	Gtk::Label lbl_game_;
	ComboBoxGame cmb_game_;

	Gtk::Box hb_nb_players_;
	Gtk::Label lbl_nb_players_;
	Gtk::ComboBoxText cmb_nb_players_;

	Gtk::Box hb_difficulty_;
	Gtk::Label lbl_difficulty_;
	Gtk::ComboBoxText cmb_difficulty_;

public:
	DialogConfig(GameOptions& options);
	virtual ~DialogConfig();
	void get_options(GameOptions& options);
};

#endif // BOARDGAME_DIALOG_CONFIG_H
