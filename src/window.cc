/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "window.h"
#include "dialog_config.h"
#include "exception.h"
#include "game_collection.h"
#include "messagedialog.h"
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/toolbar.h>
#include <glibmm/i18n.h>
#include <vector>
#include <iostream>
#include <cassert>

MainWindow::MainWindow(const GameOptions& game_options, const WindowOptions& win_options)
: Gtk::ApplicationWindow(),
  options_(game_options),
  vbox_(Gtk::ORIENTATION_VERTICAL),
  hbox_(Gtk::ORIENTATION_HORIZONTAL),
  game_(nullptr),
  worker_thread_(nullptr)
{
	initialize();
	create_menu(win_options);
	pack_widgets();
	connect_signals();
	init_display(win_options);

	// start a new game
	start_game();
}

MainWindow::~MainWindow()
{
	stop_game();
}

void MainWindow::initialize()
{
	// window properties
	set_title(_("Boardgame"));

	set_has_resize_grip();

	// these two events are needed to receive the button release event
	board_area_.set_events(Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK );

	history_scroll_.set_size_request(100, -1);
	history_scroll_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

	// the status bar
	context_id_ = statusbar_.get_context_id("game status");
}

void MainWindow::create_menu(const WindowOptions& win_options)
{
	// Create actions for menus and toolbars.

	// Game menu:
	add_action("new", sigc::mem_fun(*this, &MainWindow::on_action_game_new));
	add_action("select", sigc::mem_fun(*this, &MainWindow::on_action_game_select));
	add_action("export", sigc::mem_fun(*this, &MainWindow::on_action_game_export));

	// View menu:
	refShowToolbar_ = add_action_bool(
		"showtoolbar",
		sigc::mem_fun(*this, &MainWindow::on_action_view_show_toolbar),
		win_options.toolbar_visible
	);
	refShowStatusbar_ = add_action_bool(
		"showstatusbar",
		sigc::mem_fun(*this, &MainWindow::on_action_view_show_statusbar),
		win_options.statusbar_visible
	);
	refShowHistory_ = add_action_bool(
		"showhistory",
		sigc::mem_fun(*this, &MainWindow::on_action_view_show_history),
		win_options.history_visible
	);

	// Create the toolbar

	refBuilder_ = Gtk::Builder::create();
	try
	{
		refBuilder_->add_from_resource("/org/lozet/boardgame/toolbar.ui");
	}
	catch (const Glib::Error& ex)
	{
		std::cerr << "Building toolbar failed: " <<  ex.what();
	}
}

void MainWindow::pack_widgets()
{
	// the window contains vbox_ as widget
	add(vbox_);

	Gtk::Toolbar* toolbar = nullptr;
	refBuilder_->get_widget("toolbar", toolbar);
	if (!toolbar) {
		g_warning("GtkToolbar not found");
	} else {
		vbox_.pack_start(*toolbar, Gtk::PACK_SHRINK);
	}

	// the painting area for drawing the board
	hbox_.pack_start(board_area_);
	history_scroll_.add(history_view_);
	hbox_.pack_start(history_scroll_, Gtk::PACK_SHRINK);

	vbox_.pack_start(hbox_);

	// the status bar
	vbox_.pack_end(statusbar_, Gtk::PACK_SHRINK);
}

void MainWindow::connect_signals()
{
	board_area_.signal_draw().connect(sigc::mem_fun(*this, &MainWindow::on_board_draw));

	board_area_.signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::on_board_button_release));

	// An handler to receive notification from the thread.
	dispatcher_.connect(sigc::mem_fun(*this, &MainWindow::on_notification_from_worker_thread));
}

void MainWindow::init_display(const WindowOptions& win_options)
{
	set_default_size(win_options.width, win_options.height);
	if (win_options.maximized) {
		maximize();
	}

	show_all_children();

	if (!win_options.toolbar_visible) {
		Gtk::Toolbar* toolbar = nullptr;
		refBuilder_->get_widget("toolbar", toolbar);
		if (toolbar) {
			toolbar->hide();
		}
	}

	if (!win_options.statusbar_visible) {
		statusbar_.hide();
	}

	if (!win_options.history_visible) {
		history_scroll_.hide();
	}
}

void MainWindow::start_game()
{
	assert(game_ == nullptr);

	try
	{
		game_ = GameCollection::create_game(options_);

		history_view_.connect(game_);

		start_next_turn();
	}
	catch (const BoardgameErr& ex)
	{
		message_error(_("Error Boardgame"), Glib::ustring::compose("%1\n%2",
			_("Error while starting the new game."), ex.get_message()));
	}
}

void MainWindow::start_mcts_thread()
{
	assert(worker_thread_ == nullptr);

	worker_thread_ = new std::thread(
		[this]
		{
			int nb_iter;
			double uctk;

			if (options_.nb_iterations != -1) {
				nb_iter = options_.nb_iterations;
			} else {
				nb_iter = game_->get_nb_iter();
			}

			if (options_.uctk != -1.0) {
				uctk = options_.uctk;
			} else {
				uctk = game_->get_uctk();
			}

			worker_.do_work(this, game_->get_state(), nb_iter, uctk, 0);
		});
}

void MainWindow::stop_mcts_thread()
{
	if (worker_thread_) {
		// Order the worker thread to stop and wait for it to stop.
		worker_.stop_work();
		if (worker_thread_->joinable()) {
			worker_thread_->join();
			delete worker_thread_;
			worker_thread_ = nullptr;
		}
	}
}

// notify() is called from ExampleWorker::do_work(). It is executed in the worker
// thread. It triggers a call to on_notification_from_worker_thread(), which is
// executed in the GUI thread.
void MainWindow::notify()
{
	dispatcher_.emit();
}

void MainWindow::on_notification_from_worker_thread()
{
	if (worker_thread_ && worker_.has_stopped())
	{
		// work is done.
		if (worker_thread_->joinable()) {
			worker_thread_->join();
		}
		delete worker_thread_;
		worker_thread_ = nullptr;

		// get the computed move
		std::string move;
		worker_.get_move(&move);

		// play the move
		game_->set_displayed_move(move);
		start_move_animation();
	}

	//update_widgets();
}

void MainWindow::start_next_turn()
{
	statusbar_.pop(context_id_);
	statusbar_.push(game_->get_status(), context_id_);
	//game_->get_state()->print();

	// play turns that player can't control, like rolling the dice
	if (game_->get_state()->is_random_move()) {
		const std::string move = game_->get_state()->get_random_move();
		game_->set_displayed_move(move);
		start_move_animation();
	// if a computer player must play
	} else if (game_->is_computer_turn()) {
		// if only one move, available, play it
		if (game_->get_state()->has_one_move_left()) {
			const std::string move = game_->get_state()->get_only_move_left();
			game_->set_displayed_move(move);
			start_move_animation();
		} else {
			start_mcts_thread();
		}
	}
}

void MainWindow::on_turn_finished()
{
	board_area_.get_window()->invalidate(false);

	// if the game is going on
	if (game_->get_state()->has_move_left()) {
		start_next_turn();
	} else {
		statusbar_.pop(context_id_);
		statusbar_.push(game_->get_status(), context_id_);
	}
}

void MainWindow::start_move_animation()
{
	timeout_conn_ = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::on_timeout), 33);
}

void MainWindow::stop_move_animation()
{
	if (timeout_conn_.connected()) {
		timeout_conn_.disconnect();
	}
}

bool MainWindow::on_timeout()
{
	const bool to_be_continued = game_->update_animation();

	if (to_be_continued) {
		// force our program to redraw the entire board.
		board_area_.get_window()->invalidate(false);
	} else {
		on_turn_finished();
	}

	return to_be_continued;
}

// This signal is emitted when a widget is supposed to render itself.
// The widget's top left corner must be painted at the origin of the passed in
// context and be sized to the values returned by Gtk::Widget::get_allocated_width()
// and Gtk::Widget::get_allocated_height().
// Signal handlers connected to this signal can modify the cairo context passed
// as cr in any way they like and don't need to restore it. The signal emission
// takes care of calling cairo_save() before and cairo_restore() after invoking
// the handler.
// The signal handler will get a cr with a clip region already set to the
// widget's dirty region, i.e. to the area that needs repainting. Complicated
// widgets that want to avoid redrawing themselves completely can get the full
// extents of the clip region with gdk_cairo_get_clip_rectangle(), or they can
// get a finer-grained representation of the dirty region with cairo_copy_clip_rectangle_list().
bool MainWindow::on_board_draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
	if (game_) {
		game_->draw(cr);
	}

	// true to stop other handlers from being invoked for the event.
	// false to propagate the event further.
	return false;
}

bool MainWindow::on_board_button_release(GdkEventButton* release_event)
{
	// if click provoked a move
	if (game_->handle_click(release_event)) {
		// refresh display
		start_move_animation();
	}
	return false;
}

// GAME MENU

void MainWindow::on_action_game_new()
{
	if (game_)
	{
		stop_mcts_thread();
		stop_move_animation();

		game_->reset();
		//history_view_.reset();

		start_next_turn();
		board_area_.get_window()->invalidate(false);
	}
}

void MainWindow::on_action_game_select()
{
	try
	{
		DialogConfig dialog(options_);
		dialog.set_transient_for(*this);

		const int retour = dialog.run();

		// if returned with OK button then
		if (retour == Gtk::RESPONSE_OK) {
			dialog.get_options(options_);

			stop_game();

			start_game();
			board_area_.get_window()->invalidate(false);
		}
	}
	catch (const Glib::Error& ex)
	{
		std::cerr << __FILE__ << ": " << ex.what() << std::endl;
		message_error("Glib Error when configuring a new game", ex.what());
	}
}

Glib::ustring MainWindow::get_filename_from_dialog()
{
	Gtk::FileChooserDialog dialog(*this, _("Export history to file"), Gtk::FILE_CHOOSER_ACTION_SAVE);
	dialog.add_button(_("_Save"), Gtk::RESPONSE_APPLY);
	dialog.add_button(_("_Cancel"), Gtk::RESPONSE_CANCEL);
	dialog.set_default_response(Gtk::RESPONSE_APPLY);
	dialog.set_do_overwrite_confirmation(true);
	const int ret = dialog.run();

	Glib::ustring filename;

	if (ret == Gtk::RESPONSE_APPLY) {
		filename = dialog.get_filename();
	}

	return filename;
}

void MainWindow::on_action_game_export()
{
	Glib::ustring filename = get_filename_from_dialog();

	if (filename != "")
	{
		FILE* fp = fopen(filename.c_str(), "w");

		if (fp) {
			fprintf(fp, _("Game: %s\n"), GameCollection::get_info(options_.game).name.c_str());
			game_->export_history(fp);
			fclose(fp);
		} else {
			Glib::ustring err = Glib::ustring::compose("Error opening the file '%1' for writing :\n%2", filename, strerror(errno));
			message_error(*this, "Error while exporting the history", err);
		}
	}
}

void MainWindow::stop_game()
{
	if (game_) {
		stop_mcts_thread();
		stop_move_animation();
		history_view_.disconnect();
		delete game_;
		game_ = nullptr;
	}
}

WindowOptions MainWindow::get_ui_options() const
{
	WindowOptions options;

	// get the maximized state
	options.maximized = get_window()->get_state() & Gdk::WINDOW_STATE_MAXIMIZED;

	// get the window dimensions
	get_size(options.width, options.height);

	// get the toggles states
	refShowToolbar_->get_state(options.toolbar_visible);
	refShowStatusbar_->get_state(options.statusbar_visible);
	refShowHistory_->get_state(options.history_visible);

	return options;
}

const GameOptions& MainWindow::get_game_options() const
{
	return options_;
}

// MENU VIEW

void MainWindow::on_action_view_show_toolbar()
{
	bool active = false;
	refShowToolbar_->get_state(active);

	//The toggle action's state does not change automatically:
	active = !active;
	refShowToolbar_->change_state(active);

	Gtk::Toolbar* toolbar = nullptr;
	refBuilder_->get_widget("toolbar", toolbar);
	if (toolbar) {
		toolbar->set_visible(active);
	}
}

void MainWindow::on_action_view_show_statusbar()
{
	bool active = false;
	refShowStatusbar_->get_state(active);

	//The toggle action's state does not change automatically:
	active = !active;
	refShowStatusbar_->change_state(active);

	statusbar_.set_visible(active);
}

void MainWindow::on_action_view_show_history()
{
	bool active = false;
	refShowHistory_->get_state(active);

	//The toggle action's state does not change automatically:
	active = !active;
	refShowHistory_->change_state(active);

	history_scroll_.set_visible(active);

	int width, height;
	get_size(width, height);
	if (active) {
		width += 100;
	} else {
		width -= 100;
	}
	resize(width, height);
}
