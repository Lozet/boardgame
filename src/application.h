#ifndef BOARDGAME_APPLICATION_H
#define BOARDGAME_APPLICATION_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "settings.h"
#include "window.h"
#include <gtkmm/application.h>
#include <gtkmm/builder.h>
#include <memory>

class BoardgameApplication : public Gtk::Application
{
protected:
	BoardgameApplication();

public:
	static Glib::RefPtr<BoardgameApplication> create();

private:
	void init_games();

	void create_window(const GameOptions& game_options);

	void save_config(MainWindow* window);

	// Overrides of default signal handlers:
	void on_startup() override;
	int on_command_line(const Glib::RefPtr< Gio::ApplicationCommandLine > &command_line) override;
	int on_handle_local_options(const Glib::RefPtr<Glib::VariantDict>& options);

	/** When a window is hidden (closed).
	 */
	void on_window_hide(MainWindow* window);

	/** Create a new window.
	 */
	void on_menu_new();

	/** Show the help.
	 */
	void on_menu_help();

	/** Show the current game help.
	 */
	void on_menu_game_help();

	/** Show the about dialogbox.
	 */
	void on_menu_about();

	/** Quit the application.
	 */
	void on_menu_quit();

	void display_help(const Glib::ustring page);

	static GameId parse_game(Glib::ustring arg);
	static GameNumberPlayers parse_nb_players(Glib::ustring arg);
	static GameDifficulty parse_difficulty(Glib::ustring arg);
	static int parse_nb_iterations(Glib::ustring arg);
	static double parse_uctk(Glib::ustring arg);

	/** Used to build the menu. Could be a local variable of on_startup(). */
	Glib::RefPtr<Gtk::Builder> refBuilder_;

	/** Store the application settings. */
	std::unique_ptr<Settings> settings_;

};

#endif // BOARDGAME_APPLICATION_H
