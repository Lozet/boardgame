#ifndef BOARDGAME_GAME_INFO_H
#define BOARDGAME_GAME_INFO_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <glibmm/ustring.h>
#include <vector>

struct GameInfo {
	Glib::ustring id;
	Glib::ustring str_id;
	Glib::ustring name;

	GameInfo(const int i, const Glib::ustring& si, const Glib::ustring& n)
	: id(std::to_string(i)),
      str_id(si),
	  name(n)
	{
	}
};

struct GameCategory {
	Glib::ustring id;
	Glib::ustring name;
	std::vector<GameInfo*> games;

	GameCategory(const Glib::ustring& i, const Glib::ustring& n)
	: id(i),
	  name(n)
	{
	}
};

#endif //BOARDGAME_GAME_INFO_H
