#ifndef BOARDGAME_HISTORY_VIEW_H
#define BOARDGAME_HISTORY_VIEW_H
/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "games/game.h"
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <sigc++/connection.h>

class HistoryView: public Gtk::TreeView
{
public:
	HistoryView();
	virtual ~HistoryView();

	void connect(Game*const game);
	void disconnect();

	void on_new_move(const int player, const std::string& move);
	void on_game_reset();


protected:
	//Tree model columns:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:

		ModelColumns()
		{
			add(m_col_move);
			add(m_col_backcolor);
		}

		Gtk::TreeModelColumn<Glib::ustring> m_col_move;
		// the row background color represents the player
		Gtk::TreeModelColumn<Gdk::RGBA> m_col_backcolor;
	};

	ModelColumns m_Columns;


private:
	Glib::RefPtr<Gtk::ListStore> refTreeModel_;
	sigc::connection conn_new_move_;
	sigc::connection conn_game_reset_;
};

#endif // BOARDGAME_HISTORY_VIEW_H
