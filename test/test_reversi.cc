/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/reversi_state.h"
#include "uct.h"
#include <algorithm>
#include <cstring>

SUITE(ReversiStateTest)
{
	class GameFixture
	{
	public:
		ReversiState g;
	};

	TEST_FIXTURE(GameFixture, ExampleGame)
	{
		std::list<std::string> moves;
		std::vector<int> disks;

		CHECK_EQUAL(1, g.get_current_player());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(false, g.is_random_move());

		CHECK_EQUAL(false, g.try_move(0));
		CHECK_EQUAL(false, g.try_move(17));
		CHECK_EQUAL(false, g.try_move(18));
		CHECK_EQUAL(true, g.try_move(19));
		CHECK_EQUAL(false, g.try_move(20));
		CHECK_EQUAL(false, g.try_move(21));

		g.get_moves(moves);
		CHECK_EQUAL(4, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "19") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "26") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "37") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "44") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.get_pieces_taken("19", disks);
		CHECK_EQUAL(1, disks.size());
		CHECK(std::find(disks.begin(), disks.end(), 27) != disks.end());
		disks.clear();

		g.get_pieces_taken("37", disks);
		CHECK_EQUAL(1, disks.size());
		CHECK(std::find(disks.begin(), disks.end(), 36) != disks.end());
		disks.clear();

		g.do_move(37);
		CHECK_EQUAL(2, g.get_current_player());
		g.get_moves(moves);
		CHECK_EQUAL(3, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "29") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "43") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "45") != moves.end());
		moves.clear();

		g.do_move(45);
		CHECK_EQUAL(1, g.get_current_player());
		g.get_moves(moves);
		CHECK_EQUAL(4, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "19") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "26") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "44") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "53") != moves.end());
		moves.clear();

		g.do_move(53);
		CHECK_EQUAL(2, g.get_current_player());
		g.get_moves(moves);
		CHECK_EQUAL(6, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "20") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "29") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "34") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "38") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "43") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "54") != moves.end());
		moves.clear();

		g.do_move(38);
		g.do_move(18);
		g.do_move(19);
		g.do_move(31);
		g.do_move(17);
		g.do_move(11);
		g.do_move(61);
		g.do_move(52);
		g.do_move(34);
		g.do_move(25);
		g.do_move(20);
		g.do_move(33);
		g.do_move(46);
		g.do_move(47);
		g.do_move(39);
		g.do_move(21);
		g.do_move(2);
		g.do_move(10);
		g.do_move(44);
		g.do_move(43);
		g.do_move(32);
		g.do_move(60);
		g.do_move(54);
		g.do_move(16);
		g.do_move(26);
		g.do_move(40);
		g.do_move(24);
		g.do_move(9);
		g.do_move(0);
		g.do_move(62);
		g.do_move(8);
		g.do_move(12);
		g.do_move(48);
		g.do_move(63);
		g.do_move(22);
		g.do_move(55);
		g.do_move(29);
		g.do_move(13);
		g.do_move(30);
		g.do_move(4);
		g.do_move(41);
		g.do_move(42);
		g.do_move(3);
		g.do_move(23);
		g.do_move(49);
		g.do_move(50);
		g.do_move(58);
		g.do_move(1);
		g.do_move(5);
		g.do_move(14);
		g.do_move(7);
		g.do_move(57);
		g.do_move(56);
		g.do_move(15);
		g.do_move(51);
		g.do_move(59);
		CHECK_EQUAL(true, g.has_one_move_left());
		g.do_move(6);

		int scores[2];
		g.get_scores(scores);
		CHECK_EQUAL(26, scores[0]);
		CHECK_EQUAL(38, scores[1]);

		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(1.0, g.get_result(2));
		CHECK_EQUAL(0.0, g.get_result(1));
	}

	TEST_FIXTURE(GameFixture, InitialPosition)
	{
		int good[64];
		memset(good, 0, sizeof(good));
		good[27] = 2;
		good[28] = 1;
		good[35] = 1;
		good[36] = 2;

		const int* tested = g.get_board();

		CHECK_EQUAL(0, memcmp(tested, good, sizeof(good)));
	}
}
