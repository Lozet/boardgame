/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/international_draughts_state.h"
#include "uct.h"
#include <algorithm>

SUITE(InternationalDraughtsStateTest)
{
	class GameFixture
	{
	public:
		FrenchDraughtsState g;
		GameFixture() {};
	};

	TEST_FIXTURE(GameFixture, ExampleGame)
	{
		std::list<std::string> moves;

		CHECK_EQUAL(1, g.get_current_player());
		CHECK_EQUAL(0, g.get_draw_counter());
		CHECK_EQUAL(0, g.get_king_moves());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(false, g.is_random_move());
		CHECK_EQUAL(10, g.get_board_size());

		CHECK_EQUAL(false, g.try_move("2-7"));
		CHECK_EQUAL(false, g.try_move("48-42"));
		CHECK_EQUAL(false, g.try_move("31-25"));
		CHECK_EQUAL(true, g.try_move("32-28"));
		CHECK_EQUAL(false, g.try_move("32-23"));
		CHECK_EQUAL(false, g.try_move("32-38"));
	}

	TEST_FIXTURE(GameFixture, Rules)
	{
		std::list<std::string> moves;

		g.do_move("32-27");
		g.do_move("20-25");
		g.do_move("31-26");
		g.do_move("15-20");

		CHECK_EQUAL(1, g.get_current_player());

		// men move
		g.get_moves_from(moves, 26);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "27-21") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "27-22") != moves.end());
		moves.clear();

		g.do_move("27-22");

		// mandatory capture
		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "17x28") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18x27") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("17x28");
		g.do_move("33x22");
		g.do_move("18x27");
		g.do_move("34-29");
		g.do_move("12-18");
		g.do_move("29-24");

		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "19x30") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "20x29") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("19x30");

		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(true, g.has_one_move_left());

		g.get_moves(moves);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "35x24x15") != moves.end());
		CHECK_EQUAL("35x24x15", g.get_random_move());
		moves.clear();

		g.do_move("35x24x15");
		g.do_move("11-17");
		g.do_move("40-35");
		g.do_move("13-19");
		g.do_move("45-40");
		g.do_move("14-20");

		// backward captures of a man in a rafle with a fork
		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "15x24x13x22x11") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "15x24x13x22x31") != moves.end());
		moves.clear();

		g.do_move("15x24x13x22x11");
		g.do_move("6x17");
		g.do_move("37-31");
		g.do_move("27-32");
		g.do_move("38x27");
		g.do_move("17-21");
		g.do_move("26x17");
		g.do_move("7-11");
		g.do_move("17x6");
		g.do_move("1-7");
		g.do_move("27-21");
		g.do_move("16x27");
		g.do_move("31x22");
		g.do_move("8-13");
		g.do_move("22-17");
		g.do_move("13-19");
		g.do_move("36-31");
		g.do_move("2-8");
		g.do_move("31-26");
		g.do_move("10-15");
		g.do_move("26-21");
		g.do_move("15-20");
		g.do_move("21-16");
		g.do_move("5-10");
		g.do_move("17-11");
		g.do_move("10-15");

		// man rafling and jumping on the crowning row but keeping jumping and
		// not being crowned
		g.get_moves(moves);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "11x2x13x24") != moves.end());
		moves.clear();

		g.do_move("11x2x13x24");

		CHECK_EQUAL(DraughtsState::SQUARE_PION, g.get_board()[23].type);

		g.do_move("20x29");

		// crowing of a man
		CHECK_EQUAL(DraughtsState::SQUARE_PION, g.get_board()[5].type);
		g.do_move("6-1");
		CHECK_EQUAL(DraughtsState::SQUARE_DAME, g.get_board()[0].type);

		g.do_move("3-8");

		// mandatory king capture
		g.get_moves(moves);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "1x34") != moves.end());
		moves.clear();

		g.do_move("1x34");

		g.do_move("9-14");
		g.do_move("34-18");
		CHECK_EQUAL(1, g.get_king_moves());

		g.do_move("14-19");
		CHECK_EQUAL(0, g.get_king_moves());

		// king move
		g.get_moves_from(moves, 17);
		CHECK_EQUAL(12, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "18-13") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-9") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-12") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-7") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-1") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-22") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-27") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-31") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-36") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-23") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-29") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18-34") != moves.end());
		moves.clear();
	}
}
