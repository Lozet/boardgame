/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/connectfour_state.h"
#include "uct.h"

SUITE(ConnectFourStateTest)
{
	class GameFixture
	{
	public:
		ConnectFourState g;
	};

	TEST_FIXTURE(GameFixture, P1VictoryGame)
	{
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(5, g.get_free_row(3));
		CHECK_EQUAL(5, g.get_free_row(4));

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(4, g.get_free_row(3));

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(4, g.get_free_row(4));

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(3, g.get_free_row(3));

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(3, g.get_free_row(4));

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		// heavy rollout limits to 1 move
		CHECK_EQUAL(true, g.has_one_move_left());
		CHECK_EQUAL(2, g.get_free_row(3));

		// prevent player 1 victory
		CHECK_EQUAL("3", uct_search(&g, 1000, 1.0));

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		// heavy rollout limits to 1 move
		CHECK_EQUAL(true, g.has_one_move_left());
		CHECK_EQUAL(2, g.get_free_row(4));

		CHECK_EQUAL("3", uct_search(&g, 1000, 1.0));
		g.do_move("3");
		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(1, g.get_free_row(3));

		CHECK_EQUAL(1.0, g.get_result(1));
		CHECK_EQUAL(0.0, g.get_result(2));
	}
}
