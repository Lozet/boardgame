/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/einstein_wurfelt_nicht_state.h"
#include "uct.h"
#include <algorithm>

SUITE(EinSteinWurfeltNichtStateTest)
{
	class GameFixture
	{
	public:
		EinSteinWurfeltNichtState g;
	};

	TEST_FIXTURE(GameFixture, ExampleGame)
	{
		std::list<std::string> moves;

		for (int i = 1; i <= 6; ++i) {
			CHECK_EQUAL(false, g.is_playable(i));
		}
		CHECK_EQUAL(1, g.get_current_player());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(true, g.is_random_move());

		g.get_moves(moves);
		CHECK_EQUAL(6, moves.size());
		moves.clear();

		g.do_move("3");

		CHECK_EQUAL(3, g.get_dice());
		CHECK_EQUAL(1, g.get_current_player());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(false, g.is_random_move());

		for (int i = 1; i <= 6; ++i) {
			if (i == 3) {
				CHECK_EQUAL(true, g.is_playable(i));
			} else {
				CHECK_EQUAL(false, g.is_playable(i));
			}
		}

		g.get_moves(moves);
		CHECK_EQUAL(3, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "c1-c2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "c1-b1") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "c1-b2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("c1-b2");
		CHECK_EQUAL(true, g.is_random_move());
		CHECK_EQUAL(2, g.get_current_player());

		g.do_move("4");
		CHECK_EQUAL(false, g.is_random_move());
		CHECK_EQUAL(2, g.get_current_player());

		g.do_move("a4-a3");
		CHECK_EQUAL(true, g.is_random_move());
		CHECK_EQUAL(1, g.get_current_player());

		g.do_move("6");

		g.get_moves(moves);
		CHECK_EQUAL(3, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "e3-e4") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "e3-d3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "e3-d4") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("e3-d4");

		g.do_move("4");
		g.do_move("a3-b2");

		g.do_move("1");
		g.do_move("e1-d1");

		g.do_move("3");
		g.do_move("c5-c4");

		g.do_move("1");

		g.get_moves(moves);
		CHECK_EQUAL(3, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "d1-d2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d1-c1") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d1-c2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("d1-c1");

		g.do_move("3");
		g.do_move("c4-d3");

		g.do_move("1");
		g.do_move("c1-b2");

		g.do_move("3");
		// should give about 15% of more probability to win than the others
		CHECK_EQUAL("d3-e2", uct_search(&g, 1000, 1.0));
		g.do_move("d3-e2");

		g.do_move("4");

		CHECK_EQUAL(4, g.get_dice());
		CHECK_EQUAL(1, g.get_current_player());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(false, g.is_random_move());

		// 4 should be the moving piece, but only 1 and 5 are still there
		for (int i = 1; i <= 6; ++i) {
			if (i == 5 || i == 1 || i == 4) {
				CHECK_EQUAL(true, g.is_playable(i));
			} else {
				CHECK_EQUAL(false, g.is_playable(i));
			}
		}

		g.get_moves(moves);
		CHECK_EQUAL(6, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "b2-b3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "b2-a2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "b2-a3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-d3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-c2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-c3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("b2-a3");

		g.do_move("5");
		g.do_move("b4-c4");

		g.do_move("4");

		g.get_moves(moves);
		CHECK_EQUAL(4, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "a3-a4") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-d3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-c2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "d2-c3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		CHECK_EQUAL("a3-a4", uct_search(&g, 1000, 1.0)); // ~15% more
		g.do_move("a3-a4");

		g.do_move("4");

		g.get_moves(moves);
		CHECK_EQUAL(4, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "e2-e1") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "c4-d4") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "c4-d3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "c4-c3") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		CHECK_EQUAL("e2-e1", uct_search(&g, 1000, 1.0)); // ~65% more
		g.do_move("e2-e1");

		CHECK_EQUAL(1.0, g.get_result(2));
		CHECK_EQUAL(0.0, g.get_result(1));
		CHECK_EQUAL(false, g.has_move_left());
	}
}
