/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/english_draughts_state.h"
#include "uct.h"
#include <algorithm>

SUITE(EnglishDraughtsStateTest)
{
	class GameFixture
	{
	public:
		EnglishDraughtsState g;
	};

	TEST_FIXTURE(GameFixture, ExampleGame)
	{
		std::list<std::string> moves;

		CHECK_EQUAL(2, g.get_current_player());
		CHECK_EQUAL(0, g.get_draw_counter());
		CHECK_EQUAL(0, g.get_king_moves());
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(false, g.is_random_move());
		CHECK_EQUAL(8, g.get_board_size());

		CHECK_EQUAL(false, g.try_move("2-7"));
		CHECK_EQUAL(false, g.try_move("7-11"));
		CHECK_EQUAL(false, g.try_move("4-11"));
		CHECK_EQUAL(true, g.try_move("10-15"));
		CHECK_EQUAL(false, g.try_move("10-19"));
		CHECK_EQUAL(false, g.try_move("23-18"));

		g.get_moves(moves);
		CHECK_EQUAL(7, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "9-13") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "9-14") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "10-14") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "10-15") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "11-15") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "11-16") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "12-16") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.get_moves_from(moves, 10);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "11-15") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "11-16") != moves.end());
		moves.clear();

		CHECK_EQUAL(7, g.get_jump_target(2, 11));
		CHECK_EQUAL(7, g.get_jump_target(11, 2));

		CHECK_EQUAL(17, g.get_jump_target(14, 21));
		CHECK_EQUAL(17, g.get_jump_target(21, 14));

		g.do_move("9-14");
		CHECK_EQUAL(1, g.get_current_player());
		g.do_move("22-17");
		CHECK_EQUAL(2, g.get_current_player());
		g.do_move("11-16");
		CHECK_EQUAL(1, g.get_current_player());

		g.get_moves(moves);
		CHECK_EQUAL(7, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "17-13") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "25-22") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "26-22") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "23-18") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "23-19") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "24-19") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "24-20") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		// men moves
		g.get_moves_from(moves, 22);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "23-18") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "23-19") != moves.end());
		moves.clear();

		g.get_moves_from(moves, 16);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "17-13") != moves.end());
		moves.clear();

		g.do_move("24-20");
		g.do_move("16-19");

		// mandatory capture
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(true, g.has_one_move_left());

		g.get_moves(moves);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "23x16") != moves.end());
		moves.clear();

		g.do_move("23x16");

		// another mandatory capture
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(true, g.has_one_move_left());

		g.get_moves(moves);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "12x19") != moves.end());
		moves.clear();

		g.do_move("12x19");
		g.do_move("27-24");
		g.do_move("5-9");
		g.do_move("24x15");
		g.do_move("10x19");
		g.do_move("17x10");

		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "7x14") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "6x15") != moves.end());
		moves.clear();

		g.do_move("7x14");
		g.do_move("31-27");
		g.do_move("9-13");
		g.do_move("27-24");
		g.do_move("3-7");
		g.do_move("24x15");
		g.do_move("7-10");
		g.do_move("15-11");
		g.do_move("8x15");

		g.get_moves(moves);
		CHECK_EQUAL(7, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "21-17") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "25-22") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "26-22") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "26-23") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "32-27") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "28-24") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "20-16") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("20-16");
		g.do_move("15-19");
		g.do_move("16-12");
		g.do_move("10-15");
		g.do_move("32-27");
		g.do_move("6-10");
		g.do_move("27-24");
		g.do_move("14-18");
		g.do_move("24-20");
		g.do_move("18-23");
		g.do_move("20-16");
		g.do_move("2-7");
		g.do_move("25-22");
		g.do_move("23-27");
		g.do_move("21-17");
		g.do_move("27-32"); // crowned red piece

		g.do_move("26-23");
		g.do_move("19x26");
		g.do_move("30x23");
		g.do_move("15-19");
		g.do_move("23-18");

		g.get_moves(moves);
		CHECK_EQUAL(9, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "32-27") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "19-23") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "19-24") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "10-14") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "10-15") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "7-11") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "4-8") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "1-5") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "1-6") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.do_move("32-27");
		g.do_move("18-15");
		g.do_move("27-23");
		g.do_move("15x6");
		g.do_move("1x10");
		g.do_move("29-25");
		g.do_move("23-27");
		g.do_move("25-21");

		// king moves
		g.get_moves_from(moves, 26);
		CHECK_EQUAL(4, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "27-23") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "27-24") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "27-31") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "27-32") != moves.end());
		moves.clear();

		g.do_move("27-23");

		g.do_move("22-18");
		g.do_move("23x14");
		g.do_move("16-11");
		g.do_move("7x16");
		g.do_move("28-24");
		g.do_move("19x28");
		g.do_move("12-8");
		g.do_move("4x11");

		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());
		CHECK_EQUAL(1.0, g.get_result(2));
		CHECK_EQUAL(0.0, g.get_result(1));
	}

	TEST_FIXTURE(GameFixture, MenCaptures)
	{
		// men can't captures backward
		std::list<std::string> moves;

		g.do_move("9-13");
		g.do_move("21-17");
		g.do_move("10-14");
		g.do_move("17x10");
		g.do_move("7x14");
		g.do_move("22-18");
		g.do_move("12-16");
		g.do_move("18x9");
		g.do_move("5x14");
		g.do_move("23-18");
		g.do_move("14x23");
		g.do_move("27x18");
		g.do_move("16-20");
		g.do_move("18-14");
		g.do_move("20x27");
		g.do_move("31x24");
		g.do_move("11-15");
		g.do_move("14-9");
		g.do_move("6-10");
		g.do_move("32-27");

		g.get_moves(moves);
		CHECK_EQUAL(11, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();

		g.get_moves_from(moves, 12);
		CHECK_EQUAL(1, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "13-17") != moves.end());
		moves.clear();
	}

	TEST_FIXTURE(GameFixture, Captures)
	{
		// If there are two or more ways to jump, a player may select any one
		// that they wish, not necessarily that which gains the most pieces.
		std::list<std::string> moves;

		g.do_move("9-13");
		g.do_move("24-19");
		g.do_move("13-17");
		g.do_move("22x13");
		g.do_move("6-9");
		g.do_move("13x6");
		g.do_move("2x9");
		g.do_move("21-17");
		g.do_move("10-14");
		g.do_move("17x10");
		g.do_move("7x14");
		g.do_move("25-22");
		g.do_move("11-16");
		g.do_move("28-24");
		g.do_move("8-11");
		g.do_move("32-28");
		g.do_move("14-18");

		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "22x15x8") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "23x14") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();
	}

	TEST_FIXTURE(GameFixture, Crowning)
	{
		// crowning stop any captures sequence
		std::list<std::string> moves;

		g.do_move("9-13");
		g.do_move("22-17");
		g.do_move("13x22");
		g.do_move("26x17");
		g.do_move("6-9");
		g.do_move("17-13");
		g.do_move("10-15");
		g.do_move("22x6");
		g.do_move("2x9");
		g.do_move("30-26");
		g.do_move("11-16");
		g.do_move("26-22");
		g.do_move("1-6");
		g.do_move("23-18");
		g.do_move("9-14");

		g.get_moves(moves);
		CHECK_EQUAL(2, moves.size());
		CHECK(std::find(moves.begin(), moves.end(), "18x11x2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), "18x9x2") != moves.end());
		CHECK(std::find(moves.begin(), moves.end(), g.get_random_move()) != moves.end());
		moves.clear();
	}
}
