/******************************************************************************
	boardgame - play several board games.
	Copyright (C) 2016,2017  Gregory Lozet <gregorylozet@gmail.com>

	This file is part of boardgame.

	boardgame is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	boardgame is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with boardgame.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include "UnitTest++/UnitTest++.h"
#include "states/tictactoe_state.h"
#include "uct.h"

SUITE(TicTacToeStateTest)
{
	class GameFixture
	{
	public:
		TicTacToeState g;
	};

	TEST_FIXTURE(GameFixture, Victory1Game)
	{
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("0");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("1");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("2");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("8");
		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		CHECK_EQUAL(1.0, g.get_result(1));
		CHECK_EQUAL(0.0, g.get_result(2));
	}

	TEST_FIXTURE(GameFixture, Victory2Game)
	{
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("0");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("8");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("1");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("7");
		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		CHECK_EQUAL(0.0, g.get_result(1));
		CHECK_EQUAL(1.0, g.get_result(2));
	}

	TEST_FIXTURE(GameFixture, DrawGame)
	{
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("0");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("1");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("2");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("5");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("7");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("6");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(true, g.has_one_move_left());

		g.do_move("8");
		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		CHECK_EQUAL(0.5, g.get_result(1));
		CHECK_EQUAL(0.5, g.get_result(2));
	}

	TEST_FIXTURE(GameFixture, IAGame)
	{
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("0");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("1");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("2");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		// winning move
		CHECK_EQUAL("8", uct_search(&g, 1000, 1.0));

		g.do_move("7");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		// block a winning move
		CHECK_EQUAL("8", uct_search(&g, 1000, 1.0));

		g.do_move("5");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		CHECK_EQUAL("8", uct_search(&g, 1000, 1.0));

		g.do_move("8");
		CHECK_EQUAL(false, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		CHECK_EQUAL(1.0, g.get_result(1));
		CHECK_EQUAL(0.0, g.get_result(2));
	}

	TEST_FIXTURE(GameFixture, BetterWinGame)
	{
		g.do_move("1");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("0");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("4");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		g.do_move("3");
		CHECK_EQUAL(true, g.has_move_left());
		CHECK_EQUAL(false, g.has_one_move_left());

		// better to win than to prevent the opponent to do so
		CHECK_EQUAL("7", uct_search(&g, 1000, 1.0));
	}
}
